<?php
ob_start();

session_start('MANS_');
header("Content-Type: text/html; charset=UTF-8");
date_default_timezone_set("PRC");


 $_SEP = '.';

define('CHARSET','utf-8');
define('CAT_NAME',	'C');
define('GOODS_NAME','G');
define('ART_CAT',	'AC');
define('ART',		'A');
define('SEARCH',	'S');
define('FEATURED',	'F');
define('CHECKOUT',	'CHK');
define('ADMIN_PATH',	'm_admin');



define('SESS_ID', session_id());

function isDebugHost(){
	$debug_host = array(
			'127.0.0.1',
            'man0sions',
            'localhost',
            '192.168.0.101',
			'192.168.0.109'
	);
	if (in_array($_SERVER['SERVER_NAME'], $debug_host)) {
		return true;
	}

	return false;

}

$is_debug_host = isDebugHost();

if ($is_debug_host)
 {
	ini_set('error_reporting', E_ALL);
	define("NO_CACHE", true);
	$db_user = 'root';
 }
 else
 {
	ini_set('error_reporting', E_ALL ||~E_NOTICE);
	$db_user = "123456";
 }


$req = preg_replace("/.*?\//","",$_SERVER['REQUEST_URI']);

$que = $_SERVER['QUERY_STRING'];

$cache_id = $que;//md5($que);

/* 初始化数据库类 */
require (INC_PATH . '/class/mysql.class.php');
//$db_name = 'ethnicharms_test';
$db = new Mysql("127.0.0.1:3306","root",$db_user, "xiaoqu");

require (INC_PATH . "/class/main.class.php");

include_once (INC_PATH . "/class/goods.class.php");

include_once (INC_PATH . "/class/user.class.php");

include_once (INC_PATH . "/class/category.class.php");

include_once (INC_PATH . '/class/order.class.php');
include_once (INC_PATH . '/class/other.class.php');
include_once (INC_PATH . '/class/chat.class.php');

$Main = new Main();

$_POST = $Main->safeRequest($_POST);
$_GET = $Main->safeRequest($_GET);
$_REQUEST = $Main->safeRequest($_REQUEST);
$_COOKIE = $Main->safeRequest($_COOKIE);



$_CFG =$Main->isCached('config') ? $Main->getCache('config') : $Main->setCache($Main->loadConfig(),'config');

$Main->debugMode(true);

$Goods = new Goods();
$User = new User();
$Order = new Order();
$Cate = new Cate();
$Other = new Other();
$Chat = new Chat;


/* 创建 Smarty 对象。*/
$mobile = $Main->isMobileDevice();
if($mobile)
{
	$mobile_dir = '/templates/mobile';
	$temp_dir =  $_SEP.$mobile_dir;
}
else
{
	if($template)
	{
		$_SESSION['template'] = '/templates/'.$template;
	}

	$temp_dir = !empty($_SESSION['template']) ?  $_SEP.$_SESSION['template'] : './templates/default';

}

 require (INC_PATH . '/smarty/Smarty.class.php');
 $sm = new Smarty;
 $sm->template_dir		= $temp_dir;
 $sm->compile_dir		= ROOT_PATH.'/temp/compile/';
 $sm->config_dir		= ROOT_PATH.'/includes/';
 $sm->cache_dir			= ROOT_PATH.'/temp/cache/';
 $sm->left_delimiter	= "{{";
 $sm->right_delimiter	= "}}";
 $sm->error_reporting = true;
 $sm->cache_lifetime = 1;
 $sm->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
 if(!$is_debug_host)
 {
	//$sm->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
 }

 $sm->assign('debug',$is_debug_host);
 $sm->assign('mobile',	$mobile);
 $sm->assign('user_info',	@$_SESSION['user_info']);
 $sm->assign('is_qiandao',	$User->isQiandao());


 include_once(INC_PATH.'/class/OAuth.class.inc');
 $oauser = @OAuth::currentUser(true);
 $oauser = !empty($oauser) ? $oauser : '';

 if(empty($_SESSION['oauser']))
 {
	$_SESSION['oauser'] = $oauser;
 }

 $cat_type = @isset($_GET['cat_type']) ? @intval($_GET['cat_type']) :@intval($_COOKIE['cat_type']);

$sm->assign('cat_type',	$cat_type);
$sm->assign('que',	$que);
?>