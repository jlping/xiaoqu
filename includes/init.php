<?php

if (!defined('MANS_')) {
    die('Hack!');
}

$path = preg_replace("/includes/","",dirname(__FILE__));

define("ROOT_PATH",	preg_replace("/\\\/","/",$path));
define("INC_PATH",	ROOT_PATH.'includes');
define("STA_PATH",	INC_PATH.'/static');
define("TMP_PATH",	ROOT_PATH.'/temp');
define("UPLOAD_PATH", 'uploads');
$ajax = !empty($_POST['ajax']) ? $_POST['ajax'] : '';
$act = !empty($_REQUEST['act']) ? $_REQUEST['act'] : '';
$page = !empty($_REQUEST['page']) ? intval($_REQUEST['page']) : 0;
$size = !empty($_REQUEST['size']) ? intval($_REQUEST['size']) : 20;
$order = !empty($_REQUEST['order']) ? intval($_REQUEST['order']) : '';
$onclick = !empty($_GET['onclick']) ? $_GET['onclick'] : '';
$template = !empty($_GET['template']) ? $_GET['template'] : '';
$no_style = !empty($_GET['no_style']) ? $_GET['no_style'] : 0;
$from = !empty($_GET['from']) ? $_GET['from'] : '';
$api = !empty($_GET['api']) ? $_GET['api'] : 0;
$height = !empty($_GET['height']) ? intval($_GET['height']) : '';
$width = !empty($_GET['width']) ? intval($_GET['width']) : '';
$ipt = !empty($_GET['ipt']) ? true : false;
$ref = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

require(INC_PATH.'/static.php');
require (INC_PATH.'/config.php');


$host = 'http://' . $_SERVER['HTTP_HOST'] . preg_replace("/\/\w+.php/", "", $_SERVER['PHP_SELF']);
$sm->assign('host', $host);
$start = $page*$size;

$smarty = $sm;

$Main->common();


$root_path = ROOT_PATH;
$sm->assign('root_path',	$root_path);
ob_end_clean();
//print_r($_SESSION);

?>
