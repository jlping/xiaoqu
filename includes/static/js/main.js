Array.prototype.remove=function(dx)
{
    if(isNaN(dx)||dx>this.length){return false;}
    for(var i=0,n=0;i<this.length;i++)
    {
        if(this[i]!=this[dx])
        {
            this[n++]=this[i]
        }
    }
    this.length-=1
} 

//共用方法
function Main() {
    this.num = 0;
    this.img_arr = new Array();
    this.user_files = '*.gif; *.jpg; *.png; *.pdf; *.doc; *.docx; *.xls; *.xlsx; *.flv; *.mp4';
    this.user_files_size = 10;

}

//初始化
Main.prototype.init = function() {

    $(function() {
		//Main.setScreenStyle();
        window.d_width = $(document).width();
        window.d_height = $(document).height();
        window.w_width = $(window).width();
        window.w_height = $(window).height();
        window.Ajax_loading = $(".ajax_loding");
		
        window.Mask = $(".ui-widget-overlay");
        window.Mask.css({
            "width": d_width,
            "height": d_height
        });
		$(".btn_jq").button();
        //评论start
		var s_start = $(".s_start").raty();
        var start = $(".start");
        for (var i = 0; i < start.length; i++) {
            var ele = start.eq(i);
            var score = ele.attr("name");
            ele.raty({
                readOnly: true,
                score: score
            });

        }
		
		Main.setNav()
		
		
    })

}

//设置屏幕分辨率
Main.prototype.setScreenStyle = function(){
	
	var data = "act=set_screen&val="+screen.width;
	Main.AJAX(sep+"/ajax.php",data);	
}
Main.prototype.cusAjaxImg = function() {

    var html = '<img src="' + s_path + '/images/ajax-loader.gif"  id="ajax_img"/>'
    return html
}
//取得Radio value
Main.prototype.getRadioValue = function(name) {
    var obj = $("input[name=" + name + "]");
	
    for (var i = 0; i < obj.length; i++) {
        var ele = obj.eq(i);
		
        if (ele.attr("checked") == "checked") {
            return ele.val()
        }
    }
	return '';
}
//取得obj value
Main.prototype.getObj = function(name) {
    var obj = $("*[name=" + name + "]");
    return obj;
}
//错误提示
Main.prototype.errorMsg = function(msg){
	var str = '<label class="errors">'+msg+'</label>';
	return str;

}
//取得页面上是否有错误
Main.prototype.isError = function(obj){
	var obj = obj ? obj : $("body")
	var label = obj.find(".ui-state-error").parent("label");
	for(var i=0;i<label.length;i++)
	{
		var ele = label.eq(i);
		if(ele.css("display")!='none')
		{
			return true;	
		}	
	}
	return false;
}
//过滤"为'
Main.prototype.escapeString = function(str) {
    //str = str.replace(/\"/g, "'");
	str = str.replace(/\{/g, "\\{");
	str = str.replace(/\}/g, "\\}");
	//str = str.replace(/\&/g, "\&");
	
    return escape(str);

}
Main.prototype.json2String = function(json) {
	var data = [];
	for(var i in json)
	{
		data[data.length] = i+'='+encodeURIComponent(json[i]);	
	}
	data[data.length] = 'post_type=json2String';
	
	return data.join("&");

}
//取得指定对象下的值
Main.prototype.dataValue = function(obj,names){
	var data = new Object;
	var obj = $(obj);
	
	
	for(var i=0;i<names.length;i++)
	{
		var name = names[i];
		var ele =  obj.find("*[name="+name+"]");
		
		if(ele.attr("type")=='checkbox' || ele.attr("type")=='radio')
		{
			data[name] = this.getRadioValue(name);
 		}
		else
		{
			data[name] = ele.val();
		}
			
	}
	return data;
}
//取得token
Main.prototype.getToken = function() {
    this.setToken()
    //alert($("meta[name=token]").attr("content"))
}
Main.prototype.setToken = function() {
    var data = "act=set_token";
    Main.AJAX("ajax.php", data, this.setTokenRes);
}
Main.prototype.setTokenRes = function(res) {
    alert(res);
}
Main.prototype.closeObj = function(obj) {
    obj.hide();
}
//cloneObj
Main.prototype.cloneObj = function(obj) {
    var val = obj.val();
    var obj_f = obj.parent(".clone");
    if (!chk.isEmpty(val)) {
        var obj_c = obj_f.clone();
        obj_c.find("input").val("");
        obj_f.after(obj_c);

    }
}

//打开弹出框
Main.prototype.popupBox = function(url, title,inline) {
	var inline = inline ? inline : false;
	if(inline)
	{
		
		$(url).removeClass('none').show();	
	}
    $.colorbox({
        href: url,
        title: title,
		inline:inline,
		onClosed:function(){
			$(url).hide();	
		}
    })
}
//基本输入框
Main.prototype.inputBox = function(url, title) {
    $.colorbox({
        href: url,
        title: title
    })
}

//标签切换							
Main.prototype.tagDiv = function(obj) {
	
    var tags = $(obj).find(".tag");
    var divs = $(obj).find(".div");
    divs.hide();
	tags.removeClass("hover");
    divs.eq(0).show();
	tags.eq(0).addClass("hover");
    tags.each(function(i) {
        tags.eq(i).click(function() {
            Main.t_time = setTimeout("Main.tagsStyle(" + i + ",'" + obj + "')", 100);
        }).mouseout(function(){
			//clearTimeout(Main.t_time);	
		})
    })
}
Main.prototype.tagsStyle = function(idx, obj) {
    var tags = $(obj).find(".tag");
    var divs = $(obj).find(".div");
    tags.removeClass("hover");
    tags.eq(idx).addClass("hover");
    divs.hide();
    divs.eq(idx).show();
	
}
//ajax
Main.prototype.AJAX = function(url, data, response, type) {
    type = type ? type: 'post';
	
    $.ajax({
        'url': url,
        'data': data+'&ajax=1',
        'type': type,
        'success': function(msg) {
			
			$(".ajax_loding").hide();
            if (response) {
                response($.trim(msg));
				
            }
        }
    })

}

//is ie 成功返回版本号
Main.prototype.isIe = function() {
    var browse = window.navigator.userAgent

    if (browse.search("IE") > 0) {
        ie_pos = browse.search("IE");
        ver = browse.substr(ie_pos + 3, 3);
        ver = new Number(ver);
        return ver;
    } else {
        return false;
    }
}

//in_array
Main.prototype.inArray= function(needle,array,bool) {
   if(typeof needle=="string"||typeof needle=="number"){  
        for(var i in array){  
			
            if(needle===array[i]){  
                if(bool){  
                    return i;  
                }  
                return true;  
            }  
        }  
        return false;  
    }  
}

//文件上传
Main.prototype.uploadInit = function(obj, url, mul, limit, types, size,sel) {

    this.upload_obj = obj
    var mul = (mul != undefined) ? mul: false //上传多张
    var limit = (limit != undefined) ? limit: 10 //上传数量
    var types = (types != undefined) ? types: this.user_files
    var size = (size != undefined) ? size: this.user_files_size
	
    $(obj).uploadify({
        'auto': true,
        //关闭自动上传
        'removeTimeout': 1,
        //文件队列上传完成1秒后删除
        'swf': HOST + '/includes/static/images/uploadify.swf',
        'uploader': url,
        'method': 'post',
        //方法，服务端可以用$_POST数组获取数据
        'buttonText': Main.upLoadButton(types, limit, size),
        //设置按钮文本
        'multi': mul,
        //允许同时上传多张图片
        'uploadLimit': limit,
        //一次最多只允许上传10张图片
        'fileTypeDesc': '文件格式:',
        //只允许上传图像
        'fileTypeExts': types,
        //限制允许上传的图片后缀
        'fileSizeLimit': size + 'MB',
        //限制上传的图片不得超过200KB
        'width': 430,
        'height': 34,
        'onUploadSuccess': function(file, data, response) { //每次成功上传后执行的回调函数，从服务端返回数据到前端
            Main.img_arr[Main.num] = data;
            if (mul) Main.num++;
        },
        'onQueueComplete': this.uploadRes

    });
}
Main.prototype.uploadRes = function(res) {
	var str = ($.toJSON(res));
	var m = str.match(/(SWFUpload_\d+)_\d+/i);
	if(m)
	{
		var obj_id = m[1];
		var parnt_id = $("#"+obj_id).parent(".uploadify").attr("id");
	}
	
	
	var obj = $('#'+parnt_id+ '_res');
	var html = ''

	var idx = Main.img_arr.length-1;
	var url = Main.img_arr[idx];
	var reg_exp = /jpg|png|gif/;
	if(url.match(reg_exp))
	{
		var src=sep+'/'+url	;
	}
	else
	{
		var ext = url.substr(url.lastIndexOf(".")+1,url.length);
		
		if(Main.inArray(ext,['xls','xlsx']))
		{
			var src = s_path+'/images/xls.png';		
		}
		else if(Main.inArray(ext , ['doc','docx','wps']))
		{
			var src = s_path+'/images/word.png';		
		}
		else if(Main.inArray(ext , ['ppt']))
		{
			var src = s_path+'/images/ppt.png';		
		}
		else if(Main.inArray(ext , ['rar','zip','gz','biz']))
		{
			var src = s_path+'/images/zip.png';		
		}
		else if(Main.inArray(ext , ['pdf']))
		{
			var src = s_path+'/images/zip.png';		
		}
		else
		{
			var src = s_path+'/images/file.png';	
		}
	}
	html += '<div class="files file_' + idx + '"><a><img height="60" src="' + src + '" name="' + url + '" ></a> <img src="' + s_path + '/images/uploadify-cancel.gif" class="del_file" title="Delete" onclick="Main.delFile(' + idx + ')"> <a class="file_name ">'+file_names+'</a> </div>';
	var or_html = obj.html();
	obj.html(or_html+html);
	var files = Main.getFiles(obj,'array');
	
	$('#'+parnt_id+'s').val(files);
	
	
}
//取得文件及名称json
Main.prototype.getFiles = function(obj,type) {
	var arr = new Array();
    var obj = obj.find(".files");
    var data = new Object;
    for (var i = 0; i < obj.length; i++) {
        var ele = obj.eq(i);
        var res = new Object;
        res.url = Code.trim(ele.find("a img").attr("name"));
        res.name = Code.trim(ele.find(".file_name").html());
        res.name = res.name ? res.name: '';
        data[i] = res
		arr[i] = res.url;
    }
    if ($.toJSON(data).length < 3) {
        data = ''
    }
	if(type=='array')
	{
		return arr;	
	}
    return data
}

//赞
Main.prototype.addZan = function(id,type,user){
	if(!user)
	{
		alert('请先登录OA');
		return false;
	}
	var data = "act=zan&id="+id+"&type="+type
	Main.AJAX("ajax.php",data,this.addZanRes)
}
Main.prototype.addZanRes = function(res){
	    var res = eval("("+res+")");
		if(res.err)
		{
			alert(res.msg);
			return false;
		}
		else
		{	
			var obj = $(".zan_"+res.id)
			var c = obj.find("strong")
			alert(res.msg);
			c.html(res.zan)
		}
		
}
//删除文件
Main.prototype.delFile = function(idx) {
	
	var obj = $(Main.upload_obj + '_res');
    var chd = obj.find(".file_" + idx);
    var url = chd.find("a img").attr("name");
    var data = "act=del_file&url=" + url;
    chd.remove();
    Main.AJAX(sep+"/ajax.php", data);
	$(Main.upload_obj + 's').val(Main.img_arr);
	
	var files = Main.getFiles(obj,'array');
	$(Main.upload_obj + 's').val(files);
}
//按钮提示
Main.prototype.upLoadButton = function(type, limit, size) {
    var str = 'File Type:' + type + ', Size < ' + size + ' M , Limit ' + limit + ' File(s)';
    return str;
}
//货币切换
Main.prototype.changeCurrency = function(currency) {
    if (currency) {
        data = "act=change_currency&cur=" + currency;
        Main.AJAX("ajax.php", data, Main.locationHref);
    }
}
//页面刷新
Main.prototype.locationHref = function() {
    var href = location.href;
    href = href.substr(0, href.lastIndexOf("#"));
    location.href = href
}
Main.prototype.result = function(res) {
    if (res == 1) {
        Main.locationHref();
    }
}

//取得输入的键
Main.prototype.getKey = function(e) {
    form = document.user_login;
    if (window.event) {
        var code = e.keyCode;
    } else if (e.which) {
        var code = e.which;
    }
    if (code == 13) {
        user.login(form.email.value, form.passwd.value);
    }
}
//cookie
Main.prototype.setCookie = function(name, value, expires, path) {

    value = escape(value);
    expires = expires ? expires: '';

    if (expires == '') {
        date = new Date();
        date.setDate(date.getDate() + 7);
        expires = date.toGMTString();

    }
    document.cookie = name + "=" + escape(value) + ";expires=" + expires + ";path=" + path;
}

Main.prototype.getCookie = function(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return document.cookie.substring(c_start, c_end);
        }
    }
    return "";
}

Main.prototype.payTest = function(){
	var code = $(".code").val();
	
	Main.AJAX(sep+"/checkouts.php?act=payres",code);
}
//列表下的全选和反选
Main.prototype.checkboxAll = function(obj){
	var obj = $(obj);
	var list_checkbox = obj.find("input[name=list_checkbox]");
	list_checkbox.attr({"checked":true})
	
}
Main.prototype.verCheckboxAll = function(obj){
	var obj = $(obj);
	var list_checkbox = obj.find("input[name=list_checkbox]");
	for(var i=0;i<list_checkbox.length;i++)
	{
		var ele = list_checkbox.eq(i);
		if(ele.attr("checked")=='checked')
		{
			ele.attr({"checked":false});	
		}
		else
		{
			ele.attr({"checked":"checked"});	
		}	
	}
	
}
//设置导航
Main.prototype.setNav = function(){
	var nav = $("#nav .hide_list");
	for(i=0; i<nav.length; i++)
	{
		var obj = nav.eq(i);
		var o_width = obj.width();
		if(i>0)
		{
			//obj.css({"width":"420px"});
		}
		if(i==3)
		{
			obj.css({"left":'-100px'})	;
		}
		if(i==4)
		{
			obj.css({"left":'-200px'});	
		}
		if(i==5)
		{
			obj.css({"left":'-300px'});	
		}
		if(i==6)
		{
			obj.css({"left":'-250px'})	;
		}	
	}
}
//ckeditor config
Main.prototype.ckeditor = function(obj,type){
	var type = type ? type :'basic';
	if(type=='basic')
	{
		CKEDITOR.replace( obj, {
		toolbarGroups: [
			{ name: 'document',	   groups: [ 'mode', 'document' ] },			
			{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'tools' },	
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
			{ name: 'links' },
			{ name: 'insert' },
			{ name: 'styles' },
			{ name: 'colors' }
			
		]
		});
	}
	else if(type=='simple')
	{
		CKEDITOR.replace( obj, {
		toolbar: [
		[ 'Image',  'Smiley' ] 
		]
		});
		
	}
	else
	{
		CKEDITOR.replace( obj);	
	}
	
	
}
Main.prototype.setCnTime = function(){
	$.datepicker.regional['zh-CN'] = {  
      clearText: '清除',  
      clearStatus: '清除已选日期',  
      closeText: '关闭',  
      closeStatus: '不改变当前选择',  
      prevText: '<上月',  
      prevStatus: '显示上月',  
      prevBigText: '<<',  
      prevBigStatus: '显示上一年',  
      nextText: '下月>',  
      nextStatus: '显示下月',  
      nextBigText: '>>',  
      nextBigStatus: '显示下一年',  
      currentText: '今天',  
      currentStatus: '显示本月',  
      monthNames: ['一月','二月','三月','四月','五月','六月', '七月','八月','九月','十月','十一月','十二月'],  
      monthNamesShort: ['一月','二月','三月','四月','五月','六月', '七月','八月','九月','十月','十一月','十二月'],  
      monthStatus: '选择月份',  
      yearStatus: '选择年份',  
      weekHeader: '周',  
      weekStatus: '年内周次',  
      dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],  
      dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],  
      dayNamesMin: ['日','一','二','三','四','五','六'],  
      dayStatus: '设置 DD 为一周起始',  
      dateStatus: '选择 m月 d日, DD',  
      dateFormat: 'yy-mm-dd',  
      firstDay: 1,  
      initStatus: '请选择日期',  
      isRTL: false  
    };  
	$.datepicker.setDefaults($.datepicker.regional['zh-CN']);	
}
//登陆
Main.prototype.login = function(obj,url){
	
	var obj = $(obj);
	var email_obj = obj.find("input[name=email]");
	var pass_obj  =  obj.find("input[name=passwd]");
	
	var data = new Object;
	data.email = email_obj.val();
	data.passwd = pass_obj.val();
	
	data.field = obj.find("input[name=field]").val();
	if(data.email && data.passwd)
	{
		var url = url || './';
		data = 'act=user_login&data='+$.toJSON(data)+'&return='+url;
		
		Main.AJAX(sep+'/ajax.php',data,this.loginRes);
	}
	else
	{
		alert("请输入 用户名/密码!");
	}
}
Main.prototype.loginRes = function(res){
	
	if(res == 0)
	{
		
		alert('账户名或密码无效!')	
		
	}
	else if(res<0)
	{
		alert("账号已被禁用！");	
	}
	else
	{
		
		location.href = res ;
	}	
}
//留言
Main.prototype.review = function(obj,type,order_id,editor){
	var obj = $(obj);
	var data = new Object;
	data.user_id = obj.find(".user_id").val() || 0;
	data.user_name = obj.find(".user_name").val() || '';
	data.content = obj.find(".content").val() || editor.getPlainTxt();
	data.id      = obj.find(".id").val() || 0;
	
	data.score   = obj.find("input[name=score]").val() || 0;
	data.question =obj.find("input[name=question]").val() ||0;
	data.type   = type;
	r_id      = obj.find(".r_id").val() || 0;
	var msg = '';
	if(data.user_id=='')
	{
		msg += "请先登陆!\n"
	}
	if(data.content=='')	
	{
		msg += "请输入内容！\n"
	}
	
	if(msg)
	{
		alert(msg)
		return false	
	}
	else
	{
		var order_id = order_id ? '&order_id='+order_id : '';
		if(order_id)
		{
			data.score = $("input[name=score]:checked").val();	
		}
		var da = "act=add_review"+order_id+"&data="+$.toJSON(data)+'&r_id='+r_id;
		Main.AJAX("ajax.php",da,this.reviewRes)	
	}
}

Main.prototype.reviewRes = function (res){
	
	var res = Code.trim(res);
	var msg = user_id*1 ? '评价是种美德，你的评价会让其他民生青年少走弯路。感谢你！' : '你还没有登录唉~就这么提交可能与奖品失之交臂哦。';
	
	if(res=='1')
	{
		alert(msg)
		window.parent.location.href = window.parent.location.href;	
	}
}
Main = new Main()


/*
	encode
*/
function Code() {

    this.REGX_HTML_ENCODE = /"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g;

    this.REGX_HTML_DECODE = /&\w+;|&#(\d+);/g;

    this.REGX_TRIM = /(^\s*)|(\s*$)/g;

    this.HTML_DECODE = {
        "&lt;": "<",
        "&gt;": ">",
        "&amp;": "&",
        "&nbsp;": " ",
        "&quot;": "\"",
        "&copy;": ""

        // Add more
    };

};
Code.prototype.encodeHtml = function(s) {
    s = (s != undefined) ? s: this.toString();
    var res =  (typeof s != "string") ? s: s.replace(this.REGX_HTML_ENCODE,
    function($0) {
        var c = $0.charCodeAt(0),
        r = ["&#"];
        c = (c == 0x20) ? 0xA0: c;
        r.push(c);
        r.push(";");
        return r.join("");
    });
	res = res.replace(/\{/,"\{");
	res = res.replace(/\}/,"\}");
	res = res.replace(/&/,"\&");
	return escape(res);
};

Code.prototype.decodeHtml = function(s) {
    var HTML_DECODE = this.HTML_DECODE;

    s = (s != undefined) ? s: this.toString();
    return (typeof s != "string") ? s: s.replace(this.REGX_HTML_DECODE,
    function($0, $1) {
        var c = HTML_DECODE[$0];
        if (c == undefined) {
            // Maybe is Entity Number
            if (!isNaN($1)) {
                c = String.fromCharCode(($1 == 160) ? 32 : $1);
            } else {
                c = $0;
            }
        }
        return c;
    });
};
Code.prototype.trim = function(s) {
    s = (s != undefined) ? s: this.toString();
    return (typeof s != "string") ? s: s.replace(this.REGX_TRIM, "");
};
Code.prototype.hashCode = function() {
    var hash = this.__hash__,
    _char;
    if (hash == undefined || hash == 0) {
        hash = 0;
        for (var i = 0, len = this.length; i < len; i++) {
            _char = this.charCodeAt(i);
            hash = 31 * hash + _char;
            hash = hash & hash; // Convert to 32bit integer
        }
        hash = hash & 0x7fffffff;
    }
    this.__hash__ = hash;

    return this.__hash__;
};
var Code = new Code();


/*
	取得input的值
*/
function InputValue() {

}
//text value
InputValue.prototype.textValue = function(obj) {
    var obj = $(obj);
    var val = obj.val();
    return val;
}
//text value
InputValue.prototype.radioValue = function(obj) {
    var obj = $(obj);
    for (var i = 0; i < obj.length; i++) {
        var sel = obj.eq(i);
        if (sel.attr("checked") == 'checked') {
            var val = sel.val();
            return val;
        }
    }
    return ''
}


var ipt = new InputValue()

/*
	购物车
*/
function Cart() {

}
//添加商品到购物车 
Cart.prototype.addToCartMany = function(p_id) {
	var p_id  = p_id ? p_id : 0;
  	var package_list = $(".package_list");
	var arr = new Object();
	for(var i=0;i<package_list.length;i++)
	{
		var g = new Object;
		var obj = package_list.eq(i);
		g.goods_id = obj.attr("name");
		g.goods_number = $(".pack_num").val() ;
		var attrs = new Object;
		if(obj.find(".attr").attr("class")=='attr')
		{
			var attr_obj = obj.find(".attr");
			
			for(var j=0;j<attr_obj.length;j++)
			{
				attrs[j] = attr_obj.eq(j).val();	
				if(attrs[j] =='')
				{
					alert("Please select the attribute(s)!");	
					return false;
				}
			}
		}
		
		g.attrs = attrs;
		arr[i] = g;
	}
	
  var data = "act=add_cart_many&goods=" + $.toJSON(arr)+'&p_id='+p_id;
  Main.AJAX('ajax.php', data,this.fastCartRes);
}


//添加商品到购物车 
Cart.prototype.addToCart = function(goods_id, res, type) {
  
    var goods = new Object();
	var res = res ? res : this.addToCartRes
    var number = '1';
    var attrs = ''
    if (type == 'goods') {
        var attrs = this.getAttr();
		if (!this.checkAttr()) {
            return false;
        }
		attrs = attrs.join(",");
        number = $(".qty").val() || '1';
    }
    goods.attrs = attrs;
    goods.goods_id = goods_id;
    goods.number = number;
    var data = "act=add_cart&data=" + $.toJSON(goods);
    Main.AJAX('ajax.php', data, res);
}
Cart.prototype.addToCartRes = function(res) {
	
	location.href = 'checkouts.php?act=checkout'
}
Cart.prototype.addToCartRes2 = function(res) {
	var yes = confirm("Add to cart successfully! view your cart ?","");
	if(yes)
	{
		location.href = 'cart'	
	}
	
	
}

//取得商品属性
Cart.prototype.getAttr = function() {
    var attr = new Array
    var obj = $(".attr")

    for (var i = 0; i < obj.length; i++) {
        var ele = obj.eq(i);
        var val = ele.val();
        if (!chk.isEmpty(val)) {
            attr[attr.length] = val
        } 
    }
    return attr

}
//检查商品属性
Cart.prototype.checkAttr = function() {
    
    var obj = $(".attr")
	for (var i = 0; i < obj.length; i++) {
        var ele = obj.eq(i);
        var val = ele.val();
        if (chk.isEmpty(val)) {
            alert("Please select the attribute(s)!");
            return false;
        } 
    }
    return true;

}
//  快速增添到购物车
Cart.prototype.fastCart = function(goods_id, attr, type) {
    var type = type ? type: 'list'
	if(type=='mobile')
	{
		 Cart.addToCart(goods_id, this.addToCartRes2, 'goods')	
		 return false;
	}
    if (attr && type == 'list') {
        location.href = 'G-' + goods_id + '-.html'
    } else {
        Cart.addToCart(goods_id, this.fastCartRes, type)
    }
	
	

}

Cart.prototype.fastCartRes = function(res) {

    var res = eval("(" + res + ")");
    var cart_list = $(".cart_list");
    var list = res.list;
	
    var str = '';
    for (i = 0; i < list.length; i++) {
        str += '<div class="b_bottom"> <span class="tdone image"><img height="48" src="' + list[i].goods_thumb + '"></span> <a class="goods_name"  href="' + list[i].url + '" > ' + list[i].goods_name + '</a> <a class="pointer" onclick="Cart.dropCart(' + list[i].rec_id + ')"></a> <span class="trig">' + list[i].goods_number + '<b>X</b><em class="yellow">' + list[i].shop_priced + ' </em><em class="red"> ' + list[i].goods_attr + '</em></span> </div>'
    }
    total = res.total;
    $(".t_total").html(total.subtotal);
    $(".t_num").html(' ' + total.num + ' ');
	
    cart_list.html(str);
    var t_cart = $(".t_cart");
    var offset = t_cart.offset();
    var t_width = t_cart.width();
    var t_height = t_cart.height();
    var left = (1000 - t_cart.width()) / 2
    var top = (Main.w_height - t_cart.height()) / 2;
    t_cart.fadeIn().css({
        "left": -left,
        "top": top
    }).delay(1000).animate({
        "left": 0,
        "top": 0
    },
    500)
	

}
//dropGoods
Cart.prototype.dropCart = function(rec_id) {
    var yes = confirm("Are you sure remove it from the cart?");
    if (rec_id && yes) {
        data = "act=drop_cart&ajax=1&rec_id=" + rec_id;
        Main.AJAX("ajax.php", data, Main.result);
    }

}

var Cart = new Cart()


/*
	search
*/

function Search(){
	
}
Search.prototype.search = function(kw){
	if(kw)
	{
		this.searchPrices(0,0,false);
		location.href = "search.php?kw="+kw;	
	}
	
}

Search.prototype.searchPrices = function(start,end,location){
		var start = new Number(start);
		var end = new Number(end);
		data = 'act=price_limit&price='+start+'-'+end;
		if(location)
		{
			Main.AJAX("ajax.php",data,Main.locationHref)
		}
		else
		{
			Main.AJAX("ajax.php",data)	
		}
		
		
}


var SH = new Search(); 



//设置倒计时
function RTime(){
	
}

RTime.prototype.leftTime = function(date_str,obj){
	this.t_time = 0	;
	var date = this.dateTime(date_str);
	this.obj = $(obj);
	this.count = 0;
	this.setTimes();
}
//设置时间
RTime.prototype.setTimes = function(){
	var self = this;
	this.t_time = setTimeout(function(){self.setTimes()},1000);
	
	var r_time = this.showTime();
	var a_link = this.obj.parent("span").next("a");
	if(r_time)
	{
		this.obj.html(r_time);
		a_link.removeClass("hover");
	}
	else
	{
		a_link.addClass("hover").attr({"onclick":""});
		this.obj.removeClass("left_time");
	}
	
}
RTime.prototype.getTimes = function(){
	var r_time = this.showTime()
}
//取得剩余时间
RTime.prototype.showTime = function() {
	
	Today = new Date();
	var NowHour = Today.getHours();
	var NowMinute = Today.getMinutes();
	var NowMonth = Today.getMonth();
	var NowDate = Today.getDate();
	var NowYear = Today.getYear();
	var NowSecond = Today.getSeconds();
	if (NowYear < 2000) NowYear = 1900 + NowYear;
	Today = null;
	Hourleft = this.hour - NowHour ;
	Minuteleft = this.min - NowMinute ;
	Secondleft = this.sec - NowSecond ;
	Yearleft = this.year - NowYear ;
	Monthleft = this.mon - NowMonth - 1 ;
	Dateleft = this.day - NowDate;
	
	if (Secondleft < 0) {
		Secondleft = 60 + Secondleft;
		Minuteleft = Minuteleft - 1;
		
	}
	if (Minuteleft < 0) {
		Minuteleft = 60 + Minuteleft;
		Hourleft = Hourleft - 1;
		
	}
	if (Hourleft < 0) {
		Hourleft = 24 + Hourleft;
		Dateleft = Dateleft - 1;
	}
	
	if (Dateleft < 0) {
		Dateleft = 31 + Dateleft;
		Monthleft = Monthleft - 1;
		//Monthleft = Monthleft ? Monthleft+' 个月 ' : ''
	}
	if (Monthleft < 0) {
		Monthleft = 12 + Monthleft;
		Yearleft = Yearleft - 1;
		//Yearleft = Yearleft ? Yearleft+' 年 ' : ''
	}
	else
	{
		Dateleft += this.getMonthDays(NowYear,NowMonth) * Monthleft;	
	}
	
	if(Yearleft<0) 
	{
		//Temp =   0+'<small>天</small>' +0 + '<small>时</small>' + 0 + '<small>分</small>' + 0 + '<small>秒</small>'
		Temp = '';
	}
	else
	{
	//Temp = Yearleft + '年, ' + Monthleft + '月, ' + Dateleft + '天, ' + Hourleft + '小时, ' + Minuteleft + '分, ' + Secondleft + '秒'
	Temp ='<small> Left </small> '+ Dateleft+'<small> Days  </small>' +Hourleft + '<small> : </small>' + Minuteleft + '<small> : </small>' + Secondleft + '<small>  </small>';
	//Temp = '<small>'+Dateleft+'</small> : <small>' +Hourleft + '</small> : <small>' + Minuteleft + '</small> : <small>' + Secondleft + '</small>';
	}
	
	return Temp
	
}

//取得某月有几天
RTime.prototype.getMonthDays = function(year,month){
	 month = parseInt(month,10)+1;  
     var temp = new Date(year+"/"+month+"/0");  
     return temp.getDate(); 
}

RTime.prototype.dateTime = function(str) {
   var data = new Object;
   var reg_exp = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
   var p = str.match(reg_exp);
  
   if(p)
   {
	 data.year = this.year = p[1];
	 data.mon = this.mon  = p[2];
	 data.day = this.day  = p[3];
	 data.hour = this.hour = p[4];
	 data.min = this.min   = p[5];
	 data.sec = this.sec   = p[6];
	}
  return data;
} 



