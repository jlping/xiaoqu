/* Demo Note:  This demo uses a FileProgress class that handles the UI for displaying the file name and percent complete.
The FileProgress class is not part of SWFUpload.
*/


/* **********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
function preLoad() {
	if (!this.support.loading) {
		alert("You need the Flash Player 9.028 or above to use SWFUpload.");
		return false;
	}
}
function loadFailed() {
	alert("Something went wrong while loading SWFUpload. If this were a real application we'd clean up and then give you an alternative");
}

function fileQueued(file) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("Pending...");
		progress.toggleCancel(true, this);

	} catch (ex) {
		this.debug(ex);
	}

}

function fileQueueError(file, errorCode, message) {
	try {
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
			alert("您已经添加/上传太多文件.\n" + (message === 0 ? "You have reached the upload limit." : "您只能选择 " + (message > 1 ? "up to " + message + " files." : "1 文件.")));
			return;
		}

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			progress.setStatus("File is too big.");
			this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			progress.setStatus("Cannot upload Zero Byte files.");
			this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			progress.setStatus("Invalid File Type.");
			this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		default:
			if (file !== null) {
				progress.setStatus("Unhandled Error");
			}
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		if (numFilesSelected > 0) {
			document.getElementById(this.customSettings.cancelButtonId).disabled = false;
		}
		
		/* I want auto start the upload and I can do that here */
		this.startUpload();
	} catch (ex)  {
        this.debug(ex);
	}
}

function uploadStart(file) {
	try {
		/* I don't want to do any file validation or anything,  I'll just update the UI and
		return true to indicate that the upload should start.
		It's important to update the UI here because in Linux no uploadProgress events are called. The best
		we can do is say we are uploading.
		 */
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("Uploading...");
		progress.toggleCancel(true, this);
	}
	catch (ex) {}
	
	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	try {
		var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setProgress(percent);
		progress.setStatus("Uploading...");
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadSuccess(file, serverData) {
	try {
		
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		if (serverData) {
			progress.setStatus("上传完成！");	
			progress.setCancelled();	
			
			addImage(serverData,this.customSettings.parm);
			
		} else {
			addImage("img/error.gif", this.customSettings.parm);
			progress.setStatus("有错误！");
			progress.toggleCancel(false);
			alert(serverData);

		}


	} catch (ex) {
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			progress.setStatus("Upload Error: " + message);
			this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			progress.setStatus("Upload Failed.");
			this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			progress.setStatus("Server (IO) Error");
			this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			progress.setStatus("Security Error");
			this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			progress.setStatus("Upload limit exceeded.");
			this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			progress.setStatus("Failed Validation.  Upload skipped.");
			this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			// If there aren't any files left (they were all cancelled) disable the cancel button
			if (this.getStats().files_queued === 0) {
				document.getElementById(this.customSettings.cancelButtonId).disabled = true;
			}
			progress.setStatus("Cancelled");
			progress.setCancelled();
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			progress.setStatus("Stopped");
			break;
		default:
			progress.setStatus("Unhandled Error: " + errorCode);
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}

function uploadComplete(file) {
	if (this.getStats().files_queued === 0) {
		document.getElementById(this.customSettings.cancelButtonId).disabled = true;
	}
}

// This event comes from the Queue Plugin
function queueComplete(numFilesUploaded) {
	//var status = document.getElementById("divStatus_"+this.customSettings.index);
	//status.innerHTML = numFilesUploaded + " file" + (numFilesUploaded === 1 ? "" : "s") + " uploaded.";
}


function addImage(res,parm){
	
	var index = parm.index;
	var button_id = parm.button_id;
	var thumbnails = $("#"+button_id+"_res").find(".thumbnails");
	var set_input = parm.set_input;
	$("#"+button_id+"_default").remove();
	var res = eval("("+res+")");
	
	var obj = thumbnails;
	setImg(res,obj)
	
	
	if(set_input) //填充json到input
	{
		setInput(parm);
	}
	
	
	
}

function setImg(res,obj){
	var ext = res.type;
	if(Main.inArray(ext,['xls','xlsx']))
	{
			var src = s_path+'/images/xls.png';		
	}
	else if(Main.inArray(ext , ['jpg','png','gif']))
	{
			var src = res.url;		
	}
	else if(Main.inArray(ext , ['doc','docx','wps']))
	{
			var src = s_path+'/images/word.png';		
	}
	else if(Main.inArray(ext , ['ppt']))
	{
			var src = s_path+'/images/ppt.png';		
	}
	else if(Main.inArray(ext , ['rar','zip','gz','biz']))
	{
			var src = s_path+'/images/zip.png';		
	}
	else if(Main.inArray(ext , ['pdf']))
	{
			var src = s_path+'/images/zip.png';		
	}
	else
	{
			var src = s_path+'/images/file.png';	
	}
	var html= '<dl ><dd><img src="'+src+'" /></dd><dd><input type="text" value="'+res.file_name+'" urls="'+res.url+'"/></dd><dt class="none" >作为主图<input type="radio" onclick="delImage(\''+res.url+'\')" /></dt><dt onclick="delImage(\''+res.url+'\',$(this))" class="pointer">[删除]</dt></dl>';	
	obj.html(obj.html()+html);
}
function setInput(parm){
	var button_id = parm.button_id;
	var thumbnails = $("#"+button_id+"_res").find(".thumbnails");
	var dl = thumbnails.find("dl");
	var input = $("#"+button_id+"s");
	
	
	var data = new Object;
	var array = new Array();
	for(var j=0;j<dl.length;j++)
	{
		var arr = new Object;
		var ele = dl.eq(j);
		var ipt = ele.find("input");
		arr.file_name = ipt.val();
		arr.url = ipt.attr("urls");
		arr.icon = ele.find("img").attr("src");
		array[j] = arr.url;
		data[j]  = arr;
	}
	if(parm.data_type=='json')
	{
		var json = $.toJSON(data);
		input.val(json)
	}
	else
	{
		input.val(array)	
	}
	
	
}

function delImage(url,obj){
	var obj = obj.parents("dl");
	obj.remove();
	
	var data = "act=del_file&url=" + url;
	Main.AJAX(sep+"/ajax.php", data);
	
}


