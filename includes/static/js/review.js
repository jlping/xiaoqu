	/*
			评论功能
		*/
		function Comment(){
			
			this.form = document.comment_form
			this.form.reset()
			this.imgs = $("img[name='avater']")
			this.imgs.eq(0).addClass("img_check")
			this.avater = 1
			
		
		}
		//取得评分
		Comment.prototype.getRank = function(obj){
			
			for(i=0;i<obj.length;i++)
			{
				sel = this.form.price[i]
				
				if(sel.checked==true)
				{
					return sel.value
				}
			}	
		}
		
		//提交评论
		Comment.prototype.subComment = function(){
			price   = $("#price_start input").val()
			service =  $("#se_start input").val()
			quality = $("#qu_start input").val()
			
			user_name = this.form.user_name.value
			content   = this.form.content.value
			msg = ''
			if(chk.isEmpty(user_name))
			{
				msg = 'Please enter your username/nickname!\n'
			}
			if(chk.isEmpty(content))
			{
				msg +=	'Please enter your comment!'
			}
			
		
			if(msg)
			{
				alert(msg)	
			}
			else
			{
				comment = new Object()
				comment.user_name = user_name
				comment.content   = content
				comment.user_id   = user_id
				comment.id_value  = goods_id
				comment.avater    = ''+this.avater
				comment.rank      = (quality ? quality : 5)+','+(price ? price : 5)+','+(service ? service : 5)
				data = 'act=add_comment&ajax=1&comment='+$.toJSON(comment)
				Main.AJAX("user.php",data,this.subCommentRes)					
			}
		}
		Comment.prototype.subCommentRes = function(msg){
			if(msg=='ok')
			{
				alert('Your message have submitted  successfully!')
				href = location.href
				reg_exp = /#.*/
				href = href.replace(reg_exp,"")		
				location.href = href
			}
			else
			{
				alert('Please wait '+msg+'s ...')
			}
			
		}
		//用户名
		Comment.prototype.checkUserName = function(name,obj){
			obj = $(obj)
			if(chk.isEmpty(name))
			{
				Main.inputErr(obj,'Please enter your username/nickname!')
			}
			else
			{
				Main.inputOk(obj)	
			}
		}
		//设置头像
		Comment.prototype.setAvater = function(img_id){
			this.avater = img_id
			imgs = this.imgs
			imgs.removeClass('img_check')
			imgs.eq(img_id-1).addClass('img_check')
		}
		//内容
		Comment.prototype.checkContent = function(content,obj){
			
			obj = $(obj)
			if(chk.isEmpty(content))
			{
				Main.inputErr(obj,'Please enter your comment!')	
			}
			else
			{
				Main.inputOk(obj)	
			}
		}
		comm = new Comment()