/*
	收货人信息
*/
function Address(){
	
}

//初始化
Address.prototype.init = function(form_obj){
	this.form  = form_obj
	this.addr_td = $(".addr_td")
	this.addr_list = $("#addr_list")
	for(var i=0;i<form_obj.length;i++)
	{
		var ipt = form_obj.eq(i).find("input[type=text]")
		ipt.attr({"onblur":"addr.checkEmpty($(this))"})	
		var sel = form_obj.eq(i).find("select[name=province]")
		sel.attr({"onchange":"addr.checkEmpty($(this))"})	
	}
}



//打开地址框
Address.prototype.openAddrDialog = function(address_id){
	var address_id = address_id ? address_id : 0
	window.Mask.show()
	addr.getObj('address_id').val(address_id)
	this.dialog = addr.form.dialog({"width":700,"height":420,resizable:false,
			buttons: {
				"Use This Shipping Address": function() {
					addr.addAddr()
				}
			},close:function(){Mask.hide()}})

	
}
//检查对象的值
Address.prototype.checkEmpty = function(obj){
	var val = obj.val()
	var name = obj.attr("name")
	
	if(val=="")
	{
		var msg =  obj.attr("title") 
		
		Main.error(obj,'err',msg)	
	}
	else
	{
		Main.error(obj,'ok')	
	}
}
//取得表单中的子对象
Address.prototype.getObj = function(name){
	var obj =  this.form.find("*[name="+name+"]")
	return obj
}
//检查对象的值
Address.prototype.checkValue = function(){
	
	var data = new Object
	data.user_name = this.getObj('user_name').val()
	data.email     = this.getObj('email').val()
	data.tel       = this.getObj('tel').val()
	data.address   = this.getObj('address').val()
	data.country   = this.getObj('country').val()
	data.city      = this.getObj('city').val()
	data.zipcode   = this.getObj('zipcode').val()
	data.province  = this.getObj('province').val()//this.getProvince()
	data.province_t  = this.getObj('province_t').val()
	data.address_id  = this.getObj('address_id').val()
	var err0 = 0
	for(var i in data)
	{
		var val = data[i]
		var ele = this.getObj(i)
		if(chk.isEmpty(val))
		{
			var msg = ele.attr("title") || ele.attr("placeholder");
			if(ele.css("display")=='none' || !msg)
			{
				continue;
			}
			
			
			Main.error(ele,'err',msg)
			err0+=1	
			
		}
		else
		{
			Main.error(ele)
			if(i=='email')
			{
				if(!chk.isEmail(data.email))
				{
					
					Main.error(this.getObj('email'),'err','Invalid Email');
					err0+=1	;
				}	
			}	
		}	
	}
	
	
	if(err0==0)
	{
		
		return data
	}
	return false
	//alert($.toJSON(data))
	
}
//增加收货人信息
Address.prototype.addAddr = function(){
	
	var data = this.checkValue()
	
	if(data)
	{
		Ajax_loading.fadeIn()
		check.rightLoading()
		var address_id = new Number(data.address_id)
	
		if(address_id>0)
		{
			var data = "act=update_addr&data="+$.toJSON(data)
			Main.AJAX("ajax.php",data,this.addAddrRes)
		}
		else
		{
			var data = "act=add_addr&data="+$.toJSON(data)
			Main.AJAX("ajax.php",data,this.addAddrRes)	
		}
	}	
}
//增加收货人信息
Address.prototype.addAddr2 = function(){
	
	var data = this.checkValue()
	
	if(data)
	{
		var data = "act=add_addr&data="+$.toJSON(data)
		Main.AJAX("ajax.php",data)
	}	
}
Address.prototype.addAddr2Res= function(res){
	
	alert(res)
}
Address.prototype.addAddrRes = function(res){
	addr.getObj('address_id').val("")
	var res = eval("("+res+")")
	var list = res.list
	
	var li = ''
	for(var i=0;i<list.length;i++)
	{
		var addr_id = list[i].address_id ? list[i].address_id : ''
		var address = list[i].addr
		
		if(address.length !=0)
		{
		 var c_name = (address.checked) ? 'class="ui-selected"' : ''
		li += '<li '+c_name+'><span title="'+address.addr_str+'" name="'+addr_id+'" onclick="addr.setAddr('+addr_id+')">'+address.addr_str+'</span> <em class="ui-icon ui-icon-close btn_jq" title="Delet" onclick="addr.delAddr('+addr_id+')"></em><em title="Edit" class="ui-icon ui-icon-pencil ui-selectee btn_jq" onclick="addr.editAddr('+addr_id+')"></em></li>'
		}
	}

	addr.addr_list.html(li)
	addr.addr_td.html(res.addr.addr_str)
	Ajax_loading.fadeOut()
	check.rightComplete()
	
	
	if(addr.dialog)
	{
		addr.dialog.dialog("close")
	}
	
}
//编辑地址信息
Address.prototype.editAddr = function(addr_id){
	var addr_id = addr_id ? addr_id : 0
	
	var data = "act=get_addr&address_id="+addr_id
	Main.AJAX("ajax.php",data,this.editAddrRes)
}
Address.prototype.editAddrRes = function(res){
	var res = eval("("+res+")")
	var address_id = res.address_id
	var address = res.address
	
	addr.setFormValue(address)
	
	addr.openAddrDialog(address_id)
	
	
}

//填充表单
Address.prototype.setFormValue = function(address){
	
	addr.getObj('user_name').val(address.user_name)
	addr.getObj('email').val(address.email)
	addr.getObj('tel').val(address.tel)
	addr.getObj('address').val(address.address)
	addr.getObj('country').val(address.country)
	addr.getObj('zipcode').val(address.zipcode)
	addr.getObj('city').val(address.city)
	
	addr.selected('country',address.country)
	if(address.province)
	{
		addr.getObj('province_t').hide()
		addr.getObj('province').show()
		addr.selected('province',address.province)
	}
	else
	{
		addr.getObj('province').hide()
		addr.getObj('province_t').show().val(address.province_t)	
	}
	
}
//删除地址
Address.prototype.delAddr = function(address_id){
	var c = confirm("Are you sure to delete?","");
	var address_id = address_id ? address_id : 0
	if(c)
	{
		check.rightLoading()
		var data = "act=del_addr&address_id="+address_id
		Main.AJAX("ajax.php",data,this.addAddrRes)
	}
}


//选中selected
Address.prototype.selected = function(name,value){
	var opt = addr.getObj(name).find("option")
	for(var i=0;i<opt.length;i++)
	{
		var ele = opt.eq(i)
			
		if(ele.val()==value)
		{
			ele.attr({"selected":true})	
		}	
	}
}
//取得省
Address.prototype.getProvince = function(){
	var sel = this.getObj('province')
	var sel_t =  this.getObj('province_t')
	var province = ''
	if(sel.css("display")=='none')
	{
		province = sel_t.val()
	}
	else
	{
		province = 	sel.val()
	}
	
	return province
}
//获取region
Address.prototype.getRegion = function(region_id){
	if(region_id)
	{
		var	data = "act=get_region&region_id="+region_id
		Main.AJAX("ajax.php",data,this.getRegionRes)	
	}
	
}

Address.prototype.getRegionRes = function(res){
	var res = eval("("+res+")")
	var opt = ''
	for(var i=0;i<res.length;i++)
	{
		opt+='<option value="'+res[i].region_id+'">'+res[i].region_name
	}
	var sel = addr.getObj('province')
	var sel_t =  addr.getObj('province_t')
	if(opt)
	{
		opt = ' <option value="">Please Select'+opt
		sel_t.hide().val("")
		sel.show().html(opt)
		sel_t.next("label").hide()
	}
	else
	{
		sel.next("label").hide()
		sel.val("").hide()
		sel_t.show()
	}
}
//设置收货地址
Address.prototype.setAddr = function(address_id){
	var address_id = address_id ? address_id : 0
	
	$(".addr_list").removeClass('ui-selected');
	$(".list"+address_id).addClass('ui-selected');
	var data = "act=set_addr&address_id="+address_id
	check.rightLoading()
	Main.AJAX("ajax.php",data,this.setAddrRes)
}
Address.prototype.setAddrRes = function(res){
	var res = eval("("+res+")")
	var address = res.address
	addr.setFormValue(address)
	$(".addr_td").html(res.addr_str)
	check.rightComplete()
	
}
//设置google地图
Address.prototype.setMaps = function(addr){
	if(addr)
	{
		//window.frames['check_addr'].doSearch(addr)
	}
	
}


var addr = new Address()
