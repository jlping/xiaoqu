/*

	checkout class
*/

function Checkout(){
	
}

//选中样式
Checkout.prototype.selected = function(idx,obj){
	var obj = $("form[name="+obj+"]").find("dl")
	obj.removeClass("selected")
	var sel = obj.eq(idx)
	sel.addClass("selected")
	var ipt = sel.find("input")
	ipt.attr({"checked":true})	
}


//设置运费
Checkout.prototype.shippingFee = function(shipping_id,idx){
	var dl = $(".shipping_list dl")
	var ipt = dl.find("input[type=radio]")
	dl.removeClass("selected")
	dl.eq(idx).addClass("selected")
	ipt.eq(idx).attr({"checked":true})
	var data = "act=shipping&shipping_id="+shipping_id
	addr.getObj("shipping_id").val(shipping_id)
	check.rightLoading()
	Main.AJAX("ajax.php",data,this.shippingFeeRes)	
	
}
Checkout.prototype.shippingFeeRes = function(res){
	var res = eval("("+res+")")
	$(".subtotal").html(res.subtotald)
	$(".shipping_fee").html(res.shipping_feed)
	$(".grand_total").html(res.grandd)
	$(".free_moneyd").html(res.free_moneyd)
	$(".discount").html(res.discountd)
	if(res.free_money==0)
	{
		$(".shipping_free").hide()	
	}
	else
	{
		$(".shipping_free").show()		
	}
	
	check.rightComplete()
}
//选择支付方式
Checkout.prototype.payment = function(pay_id,idx){
	var dl = $(".payment dl")
	var ipt = dl.find("input[type=radio]")
	dl.removeClass("selected")
	dl.eq(idx).addClass("selected")
	ipt.eq(idx).attr({"checked":true})
	var data = "act=payment&payment_id="+pay_id
	
	check.rightLoading()
	Main.AJAX("ajax.php",data,this.shippingFeeRes)	
	
}
//右侧样式
Checkout.prototype.rightLoading = function(){
	this.right_table = $(".list_table")
	this.right_box = $(".right_box")
	this.ajax_img = Main.cusAjaxImg()
	this.right_box.html(this.ajax_img).fadeIn()
	this.right_table.fadeOut()
	
}
Checkout.prototype.rightComplete = function(){
	check.right_box.fadeOut()
	check.right_table.fadeIn()
}

//优惠码
Checkout.prototype.discount = function(){
	Mask.show()
	this.discount_dialog = 	$(".discount_box").dialog({close:function(){Mask.hide()}})
}
//使用优惠码
Checkout.prototype.applyDiscount = function(){
	var code = $("input[name=code]").val()
	if(code)
	{
		check.rightLoading()
		var data = "act=apply_discount&code="+code
		Main.AJAX("ajax.php",data,this.applyDiscountRes)	
	}
	
}

Checkout.prototype.applyDiscountRes = function(res){
	Mask.hide()
	
	check.discount_dialog.dialog("close")
	check.shippingFeeRes(res)
	
	
	
}
//选择默认运费和支付
Checkout.prototype.checkedDefault = function(type){
	var val = Main.getRadioValue(type)
	var data = "act="+type+"&"+type+"_id="+val
	Main.AJAX("ajax.php",data)	
}
//增加留言
Checkout.prototype.addMessage = function(res){
	var val = Main.getObj("message").val()
	var data = "act=add_message&message="+val
	Main.AJAX("ajax.php",data,res)
}
//Order
Checkout.prototype.order = function(){
	var data = new Object;
	data.addr = addr.checkValue();
	data.shipping = Main.getRadioValue('shipping');
	data.payment  = Main.getRadioValue('payment');
	data.message  = Main.getObj("message").val();
	if(data.addr && data.shipping && data.payment)
	{
		Ajax_loading.fadeIn()
		check.orderProcess(data)
	}
	
}
Checkout.prototype.orderProcess = function(data){
	
	var data = "act=check_order&data="+$.toJSON(data);
	Main.AJAX("ajax.php",data,this.orderRes)	
}
Checkout.prototype.orderRes = function(res){
	
	var res = eval("("+res+")")
	Ajax_loading.fadeOut()
	if(res.length>0)
	{
		$(".right_box").addClass("ui-state-error").html(res.join('<br>')).show()
		return false
	}
	else
	{
		$(".right_box").hide()
		location.href = 'redirect'	
	}
}
//更改购物车价格
Checkout.prototype.changeCartPrice = function(rec_id){
	var num = $(".rec_"+rec_id).find(".num").val()
	
	if(num && rec_id)
	{
		data = "act=change_cart_price&goods_number="+num+'&rec_id='+rec_id;
		Main.AJAX("ajax.php",data,this.changeCartPriceRes);
	}
}
Checkout.prototype.changeCartPriceRes = function(res){
	var obj = $(".cart_list");
	var res = eval("("+res+")");
	var list = res.goods_list;
	var total = res.total;
	for(var i=0;i<list.length;i++)
	{
		var rec_id = list[i].rec_id;
		$(".rec_"+rec_id).find(".cart_subtotal").html(list[i].subtotald);
		//obj.eq(i).find(".cart_subtotal").html(list[i].subtotald);
	}
	$(".cart_saving").html(total.saving);
	$(".cart_amount").html(total.goods_amountd);
	//$(".total").html(res);
}




var check = new Checkout()