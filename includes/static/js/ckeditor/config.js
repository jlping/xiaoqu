/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'links' },
		{ name: 'insert' ,groups: [ 'clipboard', 'undo' ]},
		{ name: 'tools' },
		//
		{ name: 'others' },
	
		{ name: 'basicstyles'},
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		'/',
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'styles' },
		
		{ name: 'colors' }
		
	];
   config.font_names=' 宋体/宋体;黑体/黑体;仿宋/仿宋_GB2312;楷体/楷体_GB2312;隶书/隶书;幼圆/幼圆;微软雅黑/微软雅黑;'+ config.font_names;
   config.filebrowserImageUploadUrl  = "ajax.php?act=upload_ckeditor";
   config.height = 100; 
  
	
};

