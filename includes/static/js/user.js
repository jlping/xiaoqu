//user
function User(){
	
}
User.prototype.checkUser = function(username,email,passwd){
	
	if(!chk.isEmpty(username) && !chk.isEmpty(email) && !chk.isEmpty(passwd))
	{
		this.email  = email
		this.passwd = passwd
		return true
	}
	else
	{
		return false
	}	
}
//签到
User.prototype.qiandao = function(){
	var data = "act=qiandao";
	Main.AJAX(sep+"/ajax.php",data,this.qiandaoRes);
	
}
User.prototype.qiandaoRes = function(res){
	
	var res = $.parseJSON(res);
	if(res)
	{
		alert('在你购买的 '+res.order_num+' 个产品中，还有 '+res.un_review+' 个没有评价！评价是种美德，你的评价会让其他民生青年少走弯路。对产品进行评价还可以获得额外的小区币哟~');	
		location.href = location.href;	
	}
	else
	{
		alert("您已签到！");
	}
}
//用户登录
User.prototype.login = function(obj){
	
	var obj = $(obj);
	var email_obj = obj.find("input[name=email]");
	var pass_obj  =  obj.find("input[name=passwd]");
	var data = new Object;
	data.email = email_obj.val();
	data.passwd = pass_obj.val();
	data.field = obj.find("input[name=field]");
	if(!chk.isEmpty(data.email))
	{
		Main.error(email_obj,'err','Invalid Email!');	
	}
	else
	{
		Main.error(email_obj);	
	}
	if(chk.isEmpty(data.passwd))
	{
		Main.error(pass_obj,'err','Invalid password!');	
	}
	else
	{
		Main.error(pass_obj);	
	}
	
	if(!Main.isError(obj))
	{
		data = 'act=user_login&data='+$.toJSON(data);
		Main.AJAX(sep+'/ajax.php',data,this.loginRes);
	}
}
User.prototype.loginRes = function(res){
	
	if(res == '1')
	{
		Main.locationHref();
	}
	else
	{
		alert('email or password is invalid!')	
	}	
}

//用户注册
User.prototype.register = function(obj){
	var obj = $(obj);
	var email_obj = obj.find("input[name=email]");
	var pass_obj  =  obj.find("input[name=passwd]");
	var re_pass_obj  =  obj.find("input[name=re_passwd]");
	var data = new Object;
	data.email = email_obj.val();
	data.passwd = pass_obj.val();
	var re_passwd  = re_pass_obj.val()
	
	if(!chk.isEmail(data.email))
	{
		Main.error(email_obj,'err','Invalid Email!');	
	}
	else
	{
		Main.error(email_obj);	
	}
	this.checkPasswd(pass_obj,data.passwd);
	this.checkRePasswd(re_pass_obj,re_passwd,data.passwd);
	
	if(!Main.isError(obj))
	{
		var da = "act=user_add&data="+$.toJSON(data);
		Main.AJAX(sep+"/ajax.php",da,this.registerRes);
	}
	
}
User.prototype.registerRes = function(res){
	if(res=='1')
	{
		Main.locationHref()	;
	}
	else
	{
		alert("Failed: mailbox already exists, please try again!");	
	}
	
}

//找回密码
User.prototype.resetPasswd = function(){
	var btn_obj = $("#reset_passwd_btn")
	var obj = $("#reset_email")
	
	var email = obj.val()
	
	if(!chk.isEmail(email))
	{
		Main.error(obj,'err','Invalid Email')	
	}
	else
	{
		btn_obj.append(Main.cusAjaxImg())
		btn_obj.find("input").hide()
		Main.error(obj,'')	
		var data = "act=reset_passwd&email="+email
		Main.AJAX(sep+"/ajax.php",data,this.resetPasswdRes)	
	}	
}
User.prototype.resetPasswdRes = function(res){
	var btn_obj = $("#reset_passwd_btn")
	if(res=='1')
	{
		var obj = $("#reset_email")
		Main.error(obj,'err','Reset password email has been sent to your mailbox!')
		btn_obj.hide()
	}
	else
	{
		var obj = $("#reset_email")
		Main.error(obj,'err','Invalid Email')
		btn_obj.find("#ajax_img").hide()
		btn_obj.find("input").show()		
	}
	
}
//检查密码
User.prototype.checkPasswd = function(obj,val){
	
	if(chk.isEmpty(val))
	{
		Main.error(obj,'err','Invalid password!');	
	}
	else
	{
		if(val.length<6 || val.length>12)
		{
			Main.error(obj,'err','Password has to be between 6 and 16 characters!');	
		}
		else
		{
			if(!chk.isUserName(val))
			{
				Main.error(obj,'err','Passwords should contain only alphanumeric and letters!');		
			}
			else
			{
				Main.error(obj);
			}
		}
	}
}
//检查re密码
User.prototype.checkRePasswd = function(obj,val,pass){
	if(chk.isEmpty(val))
	{
		Main.error(obj,'err','Invalid password!');	
	}
	else
	{
		if(val != pass)
		{
			Main.error(obj,'err','Two passwords are not the same!');	
		}
		else
		{
			Main.error(obj);	
		}
	}
}
//用户设置
User.prototype.userSetting = function(obj){
	var obj = $(obj)
	var data =  new Object
	old_passwd_obj  =  obj.find("input[name=old_passwd]");
	passwd_obj 		=  obj.find("input[name=passwd]");
	re_passwd_obj   =  obj.find("input[name=re_passwd]");
	birthday_obj    =  obj.find("input[name=birthday]");
	data.passwd = passwd_obj.val();
	re_passwd = re_passwd_obj.val();
	data.old_passwd = old_passwd_obj.val();
	data.birthday = birthday_obj.val();
	
	this.checkPasswd(old_passwd_obj,data.old_passwd);
	this.checkPasswd(passwd_obj,data.passwd);
	this.checkRePasswd(re_passwd_obj,re_passwd,data.passwd);
	
	if(!chk.isDate(data.birthday,'yyyy-MM-dd'))
	{
		Main.error(birthday_obj,'err','Invalid Value!');		
	}
	else
	{
		Main.error(birthday_obj);	
	}
	if(!Main.isError(obj))
	{
		var da = 'act=user_setting&data='+$.toJSON(data);
		Main.AJAX(sep+"/ajax.php",da,this.userSettingRes);	
	}
	
} 
User.prototype.userSettingRes = function(res){
	if(res=='1')
	{
		alert('successful!');
		Main.locationHref();
	}
	else
	{
		alert("The old password is wrong!")	;
	}
}
//user default
User.prototype.userDefault = function(obj){
	var obj = $(obj);
	var file = Main.getFiles(obj);
	if(file)
	{
		var url = file[0].url
		var data = "act=set_avater&avater="+url;
		Main.AJAX(sep+"/ajax.php",data,this.userSettingRes);		
	}


}
//fb 用户直接登陆
User.prototype.autoLogin = function(res){
	var data = "act=auto_login&data="+$.toJSON(res)
	Main.AJAX("ajax.php",data,Main.locationHref)
}
//设置fb用户的头像
User.prototype.fbAvater = function(url){
	var data = "act=set_avater&avater="+url
	Main.AJAX("ajax.php",data)
}

//用户注销
User.prototype.logout = function(){
	var data = "act=log_out"
	Main.AJAX("ajax.php",data,Main.result)	
}
//增加评论
User.prototype.addReview = function(obj){
	var obj = $(obj)
	var data = new Object
	email_obj  =  obj.find("input[name=email]");
	content_obj =  obj.find("textarea[name=content]");
	price   = obj.find("#price_start input").val();
	service = obj.find("#se_start input").val();
	quality = obj.find("#qu_start input").val();
	data.rank      = (quality ? quality : 5)+','+(price ? price : 5)+','+(service ? service : 5);
	data.email   = email_obj.val();
	data.content = content_obj.val();
	data.avater  =  $("input[name=avater]").val();
	data.goods_id = goods_id
	if(!chk.isEmail(data.email))
	{
		Main.error(email_obj,'err','Invalid Email')	;
	}
	else
	{
		Main.error(email_obj);			
	}
	if(chk.isEmpty(data.content))
	{
		Main.error(content_obj,'err','Please enter the content!');
	}
	else
	{
		Main.error(content_obj);	
	}
	if(!Main.isError(obj))
	{
		var da = "act=add_review&data="+$.toJSON(data);	
		Main.AJAX(sep+"/ajax.php",da,Main.result);
	}
	
}
//设置头像
User.prototype.setReviewAvater = function(img_id){
	var avater = $("img[name=avater]")
	$("input[name=avater]").val(img_id)
	avater.removeClass('img_check')
	avater.eq(img_id).addClass('img_check')
	
}

var user = new User()