<?php
/*
	处理分类

*/

class Cate extends Main{

	//取得当前分类列表
	function catList($cat_id,$start=0,$limit=10,$order='asc'){
		$where = '';
		if(is_int($cat_id))
		{
			$where.= "a.typeid='".$cat_id."'";
		}
		elseif(is_string($cat_id))
		{
			$where = $cat_id;
		}
		if($limit=='all')
		{
			$s_limit = '';
		}
		else
		{
			$s_limit = "limit ".$start.",".$limit."";
		}
		
		$sql = "select a.id,a.typeid,a.writer,a.title,a.shorttitle ,a.flag,a.pubdate as time,a.mid,a.keywords,a.description,a.litpic as img,al.body from
		archives as a inner join addonarticle as al on al.aid=a.id  where a.arcrank>=-1 and (".$where.")  order by a.money ".$order." ".$s_limit." ";
	   
		$data = $GLOBALS['mysql']->getAll($sql);
		foreach($data as $key=>$val)
		{
			
			$val['url'] = $this->buildUri($val['title'],$val['id'],'article');
			$val['time']= date('Y-m-d',$val['time']);
			$cat = $this->catDetail($val['typeid']);
			$val['cat_name'] = @$cat['name'];
			$val['img'] = preg_replace("/\.\.\//","./",$val['img']);
			$body = preg_replace("/[\s]/","",strip_tags($val['body']));
			if($body)
			{
				$val['body']= mb_substr($body,0,180).'...';
			}
			$data[$key] = $val;
		}
		
		
		return $data;
	}
	//取得最新文章
	function newGoods($start=0,$limit=10){
		$sql = "select a.id,a.title,a.flag,a.pubdate as time,a.mid,a.keywords,a.description,a.litpic as img from
		archives as a inner join  arctype as at on a.typeid=at.id where at.ishidden=0 order by a.id desc limit ".$start.",".$limit." ";
		
		$data = $GLOBALS['mysql']->getAll($sql);
		foreach($data as $key=>$val)
		{
			
			$val['url'] = $this->buildUri($val['title'],$val['id'],'article');
			$val['time']= date('Y-m-d H:i:s',$val['time']);
			$val['body']=strip_tags($val['body']);
			$val['img'] = preg_replace("/\.\.\//","./",$val['img']);
			$data[$key] = $val;
		}
		
		return $data;
	}
	//取得当前分类详细
	function catDetail($cat_id){
		$sql = "select typename as name,id,topid,description,keywords from  arctype where id='".$cat_id."'; ";
		$data = $GLOBALS['mysql']->getAll($sql);
		
		$data = @$data[0];
		$sons =  $this->getSons($cat_id);
		$data['son'] =$sons['data'];
		$data['ids'] =$sons['ids'];
		
		return $data;
	}

	private function getSons($topid){
		$sql = "select typename as name,id,topid from  arctype where topid='".$topid."' order by sortrank asc ;";
		$data = $GLOBALS['mysql']->getAll($sql);
		$son_ids = array();
		foreach($data as $key=>$val)
		{
			$son_ids[]=$val['id'];
		}
		$son_ids[] = $topid;
		$da['data'] = $data;
		$da['ids']  = $son_ids;
		return $da;
	}

	//取得父级分类
	
	function getFathers($son_id,&$father){
		$sql = "select topid,id,typename as name from arctype where id='".$son_id."';";
		
		$data = $GLOBALS['mysql']->getAll($sql);
		$data = $data[0];
		if($data['topid'])
		{
			$this->getFathers($data['topid'],$father);
		}
		$father[] = $data;
	}

	//取得员工之声
	function zs(){
		$sql = "select * from  zs_cat where 1";
		$data = $GLOBALS['mysql']->getAll($sql);
		
		foreach($data as $key=>$val)
		{
			if($val['son'])
			{
				$son = preg_split("/\|/",$val['son']);
				$sub = preg_split("/\n/",$val['sub_son']);
				$son1 = preg_split("/#/",$val['sub_son1']);
				
				$val['son'] = $son;
				$val['sub_son'] = $sub;
				$val['sub_son1'] = $son1;
			}
			$data[$key] = $val;
		}
		
		foreach($data as $s_k=>$s_v)
		{
			if($s_v['son'])
			{
				foreach($s_v['sub_son'] as $ss_k=>$ss_v )
				{
					$sub_son = preg_split("/\|/",$ss_v);
					$s_v['sub_son'][$ss_k] = $sub_son;
				}
				
			}
			$data[$s_k] = $s_v;
		}
		
		foreach($data as $ss_k=>$ss_v)
		{
			if($ss_v['son'])
			{
				$sub_son1 = array();
				foreach($ss_v['sub_son'] as $ss_k1=>$ss_v1 )
				{
					
					$son1 = $ss_v['sub_son1'][$ss_k1];
					$sp = preg_split("/\|/",$son1);
					foreach($sp as $s_k2=>$s_v2)
					{
						$sp2 = preg_split("/,/",$s_v2);
						$sub_son1[$ss_k1][] = $sp2;
					}
				}
				$data[$ss_k]['sub_son1']= $sub_son1;
			}
		}
		
	
		return $data;
	}

	//写入评论内容
	function review($json){
		$cat_id = !empty($json->cat_id) ? intval($json->cat_id) : 0;
		$title  = !empty($json->title) ?	$json->title: '';
		$name   = !empty($json->name) ?	$json->name: '';
		$con  = !empty($json->con) ?	$json->con: '';
		$lm0  = !empty($json->lm0) ?	intval($json->lm0): 0;
		$lm1_v  = !empty($json->lm1_v) ?	intval($json->lm1_v): 0;
		$lm2_v  = !empty($json->lm2_v) ?	intval($json->lm2_v): 0;
		$_SESSION['user'] = $name;
		$sql = "insert into zs_review(cat_id,son_id,sub_id,sub1_id,title,name,con,date) values('".$cat_id."','".$lm0."','".$lm1_v."','".$lm2_v."','".$title."','".$name."','".$con."',".time().")";
		
		$res = mysql_query($sql);
		if($res)
		{
		
			return $this->loadReview($cat_id);
		}
		return false;
	
	}
	//加载评论
	function loadReview($cat_id,$start=0,$size=8){
		if($size=='all')
		{
			$sql1 = "select * from zs_review where cat_id = '".$cat_id."'  order by date desc ";
		}
		else
		{
			$sql1 = "select * from zs_review where cat_id = '".$cat_id."'  order by date desc limit ".$start.",".$size."";
		}	
		$data = $GLOBALS['mysql']->getAll($sql1);
		foreach($data as $key=>$val)
		{
			$date = date("Y-m-d",$val['date']);
			$val['date'] = $date;
			
			$cat = $this->reviewCat($val['cat_id'],$val['son_id'],$val['sub_id'],$val['sub1_id']);
			
			$val['son'] = $cat['son'];
			$val['sub_son'] = $cat['sub_son'];
			
			$data[$key] = $val;
		}
		
		return $data;
	}

	function reviewCon($id){
		$sql = "select * from zs_review where id='".$id."';";
		$data = $GLOBALS['mysql']->getAll($sql);
		$data = @$data[0];
		$data['date'] =  date("Y-m-d",$data['date']);
		$cat = $this->reviewCat($data['cat_id'],$data['son_id'],$data['sub_id'],$data['sub1_id']);
		$data['cat'] = $cat;
		return $data;
	}

	function reviewCat($cat_id,$son,$sub_son,$sub1_son){
		$son = (int)($son);
		$sub_son = (int)($sub_son);
		$sub1_son = (int)($sub1_son);
		$res = array();
		if($cat_id==1)  //功能优化建议
		{
			$zs = $this->zs();
			$gn = $zs[0];
			$res['son'] = $gn['son'][$son];
			$res['sub_son'] = $gn['sub_son'][$son][$sub_son];
			$res['sub1_son']= $gn['sub_son1'][$son][$sub_son][$sub1_son];
		
		}
		else
		{
			$res['son'] = '';
			$res['sub_son']='';
			$res['sub1_son'] ='';
		}
		return $res;
	}
	//删除评论
	function delReview($id){
		$sql = "delete from zs_review where id='".$id."';";
		return mysql_query($sql);
	}




}

?>