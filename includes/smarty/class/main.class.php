<?php
/*
	全局类
*/

class Main{
	
	//取得配置信息
	function config(){
		$sql = "select value,varname from  sysconfig where groupid=1";
		
		$data = $GLOBALS['mysql']->getAll($sql);
		$config = array();
		foreach($data as $key=>$val)
		{
			$k = $val['varname'];
			$v = $val['value'];
			$config[$k] = $v;
		}
		return $config;
	}

	
	//取得频道列表
	function chanelList($topid=0){
		$sql = "select typename as name,id,topid from  arctype where topid='".$topid."' and ishidden=0 order by sortrank asc ;";
		$chanel = $GLOBALS['mysql']->getAll($sql);
		
		foreach($chanel as $key=>$val)
		{
			if($val['topid']==0)
			{
				$son = $this->chanelList($val['id']);
				
				$val['son']=$son;
				
			}
			$val['url']   = $this->buildUri($val['name'],$val['id'],'category');
			$chanel[$key] = $val;
		}
		
		return $chanel;
	}
	
	//构建url
	function buildUri($name,$id,$type){
		$name = preg_replace("/\//","",$name);
		$name = $name ? $name : 'page';
		if($type)
		{
			if(preg_match("/\?/",$type))
			{
				return sprintf('%s&page=%s',$type,$id);
			}
			else
			{
				return sprintf('%s-%s-%s.html',$type,$name,$id);
			}
		}
		
		else
		{
			return sprintf('%s',$id);
		}
	}

	//构造翻页
	function pager($count,$size,$type,$l_r=true){
		$page =	isset($GLOBALS['page']) ? intval($GLOBALS['page']) : 0; 
		
		$list = array();
		$amount = ceil($count/$size);
		$left = $page-2;
		$right = $page+2;
		for($i=0;$i<$amount;$i++)
		{
			if($l_r)
			{
				if($left>=0)
				{
					if($i>=$left && $i<=$right)
					{
						$list[] = $this->buildUri('pager',$i,$type);
					}
				}
				else
				{
					if($i<=4)
					{
						$list[] = $this->buildUri('pager',$i,$type);
					}
				}
			}
			else
			{
				$list[] = $this->buildUri('pager',$i,$type);
			}
				
		}
		
		$pager['first'] = @$list[0];
		$pager['last']  = @$list[count($list)-1];
		$pager['list']  = $list;
		$pager['page']  = $page; 
		$pager['total'] = $amount; 
		return $pager;
	}
	
	//计算列表总数
	function counts($sql){
		return $GLOBALS['mysql']->getone($sql);
	}

	//构造当前位置
	function position($type,$id=0,$art=null,$cat=null){
		
		
		$title = $GLOBALS['config']['cfg_webname'];

		$keywords = $GLOBALS['config']['cfg_keywords'];

		$description = $GLOBALS['config']['cfg_description'];

		$fathers = array();
		
		$ur_here = '';

		
		//构造文章页
		if($type=='article')
		{
			$article = $art->articles($id);
			$cat->getFathers($article['typeid'],$fathers);
			
			foreach($fathers as $key=>$val)
			{
				$url = $this->buildUri($val['name'],$val['id'],'category');
				$ur_here[] = '<a href="'.$url.'">'.$val['name'].'</a>';
			}
			$a_url = $this->buildUri($article['title'],$article['id'],'article');
			$ur_here[] = '<a href="'.$a_url.'">'.$article['title'].'</a>';

			$title = $article['title'].' | '.$title;
			$keywords = !empty($article['keywords']) ? $article['keywords'] : $keywords;
			$description = !empty($article['description']) ? $article['description'] : $description;

		}

		//构造分类页
		if($type=='category')
		{
			$cate = $cat->catDetail($id);
			
			$cat->getFathers($id,$fathers);
			foreach($fathers as $key=>$val)
			{
				$url = $this->buildUri($val['name'],$val['id'],'category');
				$ur_here[] = '<a href="'.$url.'">'.$val['name'].'</a>';
			}
			$title = $cate['name'].' | '.$title;
			
			$keywords = !empty($cate['keywords']) ? $cate['keywords'] : $keywords;
			$description = !empty($cate['description']) ? $cate['description'] : $description;
		}
		$position = array('title'=>$title,'keywords'=>$keywords,'description'=>$description,'ur_here'=>join($ur_here,'<code> >> </code>'));
		
		return $position ;
		
	}
	
	//写入用户祝福
	function subZhuFu($json){
		
		$user_name = $this->escape($json->user_name);
		$user_bm   = $this->escape($json->user_bm);
		$content   = mysql_escape_string(strip_tags($json->content));
		$sql = "insert into zhufu(`user_name`,`user_bm`,`content`,`add_time`) values('".$user_name."','".$user_bm."','".$content."','".time()."');";
		$res = mysql_query($sql);
		
		return $res;
	}


	//过滤字符串
	public function escape($str){
		$str = preg_replace("/[@%\/\\;#\$\^&\*]/","",$str);
		return mysql_escape_string($str);
		
	}

	//取得祝福寄语列表
	function zhufuList($start=0,$limit=5){
		$sql = "select id,user_name,user_bm,content from zhufu where 1 order by id desc limit ".$start.",".$limit.";";
		
		$data = $GLOBALS['mysql']->getAll($sql);
		return $data;
	}
	//删除祝福
	function delZhufu($id){
		$sql = "delete from zhufu where id='".$id."';";
		return mysql_query($sql);
	}
	//往昔回忆图片上传
	function uploadImg($user_name,$user_bm,$img_path,$desc,$cat_id){
		$sql = "insert into wangxi(`user_name`,`user_bm`,`img_path`,`img_desc`,`cat_id`) values('".$this->escape($user_name)."','".$this->escape($user_bm)."','".$img_path."','".$this->escape($desc)."','".$cat_id."');";
		
		return mysql_query($sql);
	}
	
	//取得往昔回忆
	function wangxiList($start=0,$limit=5,$cat_id=0){
		if($cat_id)
		{
		  $sql = "select * from wangxi as w inner join wangxi_cat as wc on w.cat_id=wc.cat_id where w.cat_id='".$cat_id."' order by id desc limit ".$start.",".$limit.";";
		}
		else
		{
			$sql = "select `id`,`user_name`,`user_bm`,`img_path`,`img_desc`,`cat_id` from wangxi where 1 order by id desc limit ".$start.",".$limit.";";
		}
		
		$data = $GLOBALS['mysql']->getAll($sql);

		foreach($data as $key=>$val)
		{
			$desc = $val['img_desc'].' '.$val['user_name'];
			$descs= $val['img_desc'].' '.$val['user_name'].' '.$val['user_bm'];
			$thumb = preg_replace("/wangxi/","wangxi/thumb",$val['img_path']); 
			$thumb = strtolower($thumb);
			if(file_exists($thumb))
			{
				$val['img_path'] = $thumb;
			}
			$val['desc'] = $desc;
			$val['descs'] = $descs;
			$data[$key] = $val;
		}
		
		return $data;
	}
	//删除往昔回忆
	function delWangxi($id){
		$sql = "delete from wangxi where id='".$id."';";
		return mysql_query($sql);
	}
	//删除往昔回忆
	function delWXCat($cat_id){
		$sql = "delete from wangxi_cat where cat_id='".$cat_id."';";
		return mysql_query($sql);
	}
	//增加往昔回忆分类
	function addWXCat($cat_name,$cat_desc,$p_id){
		$sql = "insert into wangxi_cat(`cat_name`,`cat_desc`,`p_id`) values('".$cat_name."','".$cat_desc."','".$p_id."');";
		return mysql_query($sql);
	}
	//取得往昔分类列表
	function WXCatList($p_id=0){
		$sql = "select * from wangxi_cat where p_id='".$p_id."'";
		$data = $GLOBALS['mysql']->getAll($sql);
		if($data)
		{
			foreach($data as $key=>$val)
			{
				$data[$key]['son'] = $this->WXCatList($val['cat_id']);
			}
		}
		
		return $data;
	}
	//msg
	function msg($msg,$url,$back=false){
		if(!$back)
		{
			echo "<script>alert('".$msg."');location.href='".$url."'</script>";
			exit();
		}
		else
		{
			echo "<script>alert('".$msg."');location.href=history.back()</script>";
			exit();
		}
	}
	//搜索
	function search($kw,$start=0,$limit=5){
		$sql = "select a.id,a.title,at.body from  archives as a inner join addonarticle as at on a.id=at.aid where arcrank>=0 and (a.title like '%".$kw."%')  limit ".$start.",".$limit.";";
		$data = $GLOBALS['mysql']->getAll($sql);
	
		foreach($data as $key=>$val)
		{

			$body = preg_replace("/\s/","",$val['body']);
			$title = preg_replace("/\s/","",strip_tags($val['title']));
			
			$val['title'] = preg_replace("/(".$kw.")/","<span class='pink'>$1</span>",$title);
			
			$val['description']= mb_substr(strip_tags($body),0,100);
			$data[$key] = $val;
		}
		
		return $data;
	}
	//封装导出的html数据
	function exportHtml($data){
		$html = '<table border=1 align=center width=100%>';
		$html .= <<<EN
			<tr>
				<th width="5%">编号</th>
				<th width="10%">日期</th>
				<th width="10%">分类</th>
				<th width="20%">标题</th>
				<th width="55%">内容</th>
			</tr>
EN;
		foreach($data as $key=>$val)
		{
			$html .=<<<EN
				<tr>
					<td>{$val['id']}</td>
				    <td>{$val['date']}</td>
				    <td>{$val['son']}-{$val['sub_son']}</td>
					<td>{$val['title']}</td>
					<td>{$val['con']}</td>
				</tr>
EN;
		}
		$html .= '</table>';
		return $html;
	}
	//添加统计人数
	function countAdd($data){
		$sql = "insert into counts(ip,date) values('".$data['ip']."','".$data['date']."');";
		return mysql_query($sql);
	}
	function countDetail($cat_id,$a_id){
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = "insert into count_detail(cat_id,a_id,ip,date) values('".$cat_id."','".$a_id."','".$ip."','".time()."')";
			
		return mysql_query($sql); 
	}

	//计算访问人数
	function countDeatil($id,$type='cat_id'){
		$sql = "select * from count_detail where ".$type."='".$id."';";
		
		$data = $GLOBALS['mysql']->getAll($sql);
		foreach($data as $key=>$val)
		{
			$val['date'] = date("Y年m月d日 H时i分秒");
			$data[$key] = $val;
		}
		return $data;
	}
	//取得分类中个页面的访问数
	function countDetailArticle($categories,$cate){
		foreach($categories as $key=>$val)
		{
			$cat_id = $val['id'];
			if(strpos($cat_id,'_')) //获取建议分类
			{
				$sp = preg_split("/_/",$cat_id);
				$cat_id = @$sp[1];
				
				
			}
			else //获取普通分类
			{
				
			}
		}
	}
	//取得受访问的页面
	function visArt(){
		$sql = "select distinct a_id from count_detail where a_id!=0 or length(a_id)>1";
		
		$data = $GLOBALS['mysql']->getAll($sql);
		$arr = array();
		foreach($data as $key=>$val)
		{
			if(intval($val['a_id']))
			{
				$sql = "select title from  archives where id='".$val['a_id']."';";
				
			}
			else
			{
				$a_id = preg_replace("/y_/","",$val['a_id']);
				
				$sql = "select title from zs_review where id='".$a_id."';";
			
			}
			$title = $GLOBALS['mysql']->getOne($sql);
			if(!empty($title))
			{
			$val['name'] = $title;
			$val['count'] = $this->counts("select count(*) from count_detail where a_id='".$val['a_id']."'");
			$arr[$key] = $val;
			}
			
		}
		
		
		
		
		return $arr;
	}
	//增加评分
	function score($a_id,$type){
		$count = $this->counts("select count(*) from score where a_id='".$a_id."'");
		if($count)
		{
			$sql = "update score set ".$type."=".$type."+1 where a_id='".$a_id."'";
		}
		else
		{
			$sql = "insert into score(".$type.",a_id) values(1,'".$a_id."');";
		}
		if(mysql_query($sql))
		{
			return $this->getScore($a_id);
		}
		return false;
	}

	function getScore($a_id){
		$score = array('good'=>0,'bad'=>0);
		$sql = "select good,bad from score where a_id='".$a_id."';";
		$data = $GLOBALS['mysql']->getAll($sql);
		$data = @$data[0];
		if(!empty($data))
		{
			$score = $data;
		}
		return $score;
	}

	//增加游戏
	function gameAdd($json){
		
		$img1 = !empty($json->img1) ? $json->img1 : '';
		$img2 = !empty($json->img2) ? $json->img2 : '';
		$name = !empty($json->name) ? $json->name : '';
		$time = !empty($json->time) ? $json->time : 60;
		$pos = !empty($json->pos) ? $json->pos : array();
		$pos = join($pos,"|");
	
		$url = preg_replace("/\.\./",".",$img1).'|'.preg_replace("/\.\./",".",$img2);
		
		$sql = "insert into zhaocha(`url`,`pos`,`time`,`name`) values('".$url."','".$pos."','".$time."','".$name."')";
		
		return mysql_query($sql);
	}
	//游戏列表
	function gameList($img_id=0){
		if($img_id)
		{
			$sql = "select * from zhaocha where img_id='".$img_id."' order by img_id desc;";
		}
		else
		{
			$sql = "select * from zhaocha where 1 order by img_id desc;";
		}
		
		$data = $GLOBALS['mysql']->getAll($sql);
		foreach($data as $key=>$val)
		{
			$sp = preg_split("/\|/",$val['url']);
			$val['img1'] = @$sp[0];
			$val['img2'] = @$sp[1];
			
			$sp1=  preg_split("/\|/",$val['pos']);
			$pos = array();
			foreach($sp1 as $s_k=>$s_v)
			{
				$sp2 = preg_split("/,/",$s_v);
				$pos[$s_k] = $sp2;
				
			}
			
			unset($val['url']);
			$val['pos'] = $pos;
			$data[$key] = $val;

		}
		
		return $data;
	}
	//写入找茬回复
	function zhaochaAns($time,$user_name){
		$sql = "insert into zhaocha_ans(`user_name`,`time`,`add_time`) values('".$user_name."','".$time."','".time()."')";
		return mysql_query($sql);
	}
	//取得玩家反馈
	function getGameAns($start,$size){
		$sql = "select * from zhaocha_ans where 1 order by id desc limit ".$start.",".$size." ;";
		$data = $GLOBALS['mysql']->getAll($sql);
		foreach($data as $key=>$val)
		{
			if($val['add_time'])
			{

			$add_time = date("Y/m/d H/i/s",$val['add_time']);
			$val['add_time'] = $add_time;
			$data[$key] = $val;
			}
		}
		return $data;
	}

	//缩略图生成 $path--原图路径,$per--缩略比例,可以传0.5，也可以传array($width,$height),$remove--是否删除原图
	function resizeImage($path,$per=0.5,$remove=false){
		
		if(!file_exists($path))
		{
			return false;
		}
		$size = @getimagesize($path);
		if(!$size)
		{
			return $path;
		}
		$width = $size[0];
		$height = $size[1];
		$info = pathinfo($path);
		if(is_array($per))
		{
			$new_width = $per[0];
			$new_height =$per[1];
		}
		else
		{
			$new_width = $width*$per;
			$new_height = $height*$per;
		}
		//创建画板
		$thumb = imagecreatetruecolor($new_width,$new_height);
	
		//获取图片
		switch($size['mime'])
		{
			case 'image/png':
				$source = imagecreatefrompng($path);
				break;
			case 'image/gif':
				$source = imagecreatefromgif($path);
				break;
			case 'image/jpeg':
				$source = imagecreatefromjpeg($path);
				
				break;
		}
		if(function_exists('imagecopyresampled'))
		{	
			imagecopyresampled($thumb,$source,0,0,0,0,$new_width,$new_height,$width,$height);
		}
		else
		{
			imagecopyresized($thumb,$source,0,0,0,0,$new_width,$new_height,$width,$height);
		}
		$new_path = $info['dirname'].'/thumb_'.$info['filename'].'.jpg';
		$new_path = strtolower($new_path);
			
		imagejpeg($thumb,$new_path);
		if($remove)
		{
			unlink($path);
		}
		return $new_path;
			
	}
	
}

?>