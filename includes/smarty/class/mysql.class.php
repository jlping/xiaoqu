<?php
	class MYSQL{
		
		/*
			初始化mysql
		*/
		public function __construct($host,$user,$passwd,$db=''){
			$this->host		= $host;
			$this->user     = $user;
			$this->passwd	= $passwd;
			$this->db		= $db;
		
		}

		/*
			主机连接
			成功返回：conn
			失败：直接终止
		*/
		public function connect(){
			$conn = @mysql_connect($this->host,$this->user,$this->passwd) or die("不能连接主机:{$this->host}!".mysql_error());
			
			if(isset($conn))
			{
				if($this->db)
				{
					mysql_query("set names utf8");
					mysql_select_db($this->db,$conn);
				}
				return $conn;
			}
			
		}
		
		/*
			关闭资源
			失败：直接终止
		*/
		public function close($conn){
			if(isset($conn))
			{
				@mysql_close($conn) or die("不能断开连接！".mysql_error());
			}
		}
		
		/*
			取出一个数据
		*/
		public function getOne($sql){
			
			$query = mysql_query($sql);
			$row = mysql_fetch_row($query);
			return $row[0];
			
			
		}

		/*
			取出所有数据
		*/
		public function getAll($sql,$type=MYSQL_ASSOC){
			
			$query = mysql_query($sql);

			$res = array();
			while($row   = mysql_fetch_array($query,$type))
			{
				$res[] = $row;
			}

			return $res;
			
		}
		

		/*
			取出所有数据库
		*/

		public function getAllDatabases(){
			$sql = "show databases;";
			$res = $this->getAll($sql,MYSQL_ASSOC);
			
			foreach($res as $key=>$val)
			{
				$db_list[] =  $val['Database'];
				
			
			}
			return $db_list;
		
		}
	

		/*
			创建数据库
		*/
		public function createDatabase($db_name){
			$sql = "create database if not exists ".$db_name.";";
			$query = mysql_query($sql);
			return $query;
		}

		/*
			删除数据库

		*/

		public function dropDatabase($db_name){
			$sql = "drop database if exists ".$db_name.";";
			$query = mysql_query($sql);
			return $query;
			
		}
	
	}



?>