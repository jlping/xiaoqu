<?php
/*
	处理分类

*/

class Cate extends Main{

	public function __construct(){
		$this->featured_arr = array(183,99,100,93,65,91,89,92);
		$this->feadture_cat = array();
	}
    //取得分类树
	public function categoryTree($parent_id=0,$chd=true,$img=false){
		$sql = "select * from category where parent_id=$parent_id and is_show=1 order by sort_order asc";

		$data = $GLOBALS['db']->getAll($sql);

		foreach($data as $key=>$val)
		{
			$cat_id = $val['cat_id'];
			$val['cat_name'] = ucwords($val['cat_name']);

			$ids = $GLOBALS['Goods']->ids($cat_id);
			$join = ' (cat_id='.join($ids,' or cat_id=').')';
			$val['count'] = $GLOBALS['db']->getOne("select count(*) from goods where $join and is_delete=0");

			//$val['url'] = $this->buildUri($val['cat_name'],$cat_id,CAT_NAME);
			$val['url'] = $this->buildUri($val['cat_name'],'category.php?cat_id='.$cat_id);
			if($cat_id && $chd)
			{
				$son = $this->categoryTree($cat_id,$chd,$img);
				if($son) $val['children'] = $son;

			}
			if($val['img'])
			{
				$val['img'] = preg_split("/,/",$val['img']);
			}

			if(in_array($val['cat_id'],$this->featured_arr))
			{
				$this->feadture_cat[$val['cat_id']] = $val;
			}

			$data[$key] = $val;
		}

		//print_r($data);
		return $data;
	}
	public function categoryAll($start=0,$size=10,$where="where is_show=1 order by cat_id desc"){
		$sql = "select * from category $where limit $start,$size";


		$data = $GLOBALS['db']->getAll($sql);
		foreach($data as $key=>$val)
		{
			if($val['img'])
			{
				$img = preg_split("/,/",$val['img']);
				$val['img'] = $img;

				foreach($img as $k=>$v)
				{
					if($k==0)
						$thumb = $this->resizeImage($GLOBALS['_SEP'].'/'.$v,$GLOBALS['_CFG']['thumb_size']);
				}
				$val['thumb'] = $thumb;

			}
			$val['mag'] = $GLOBALS['User']->userInfo($val['mag_id']);
			$val['url'] = $this->buildUri($val['cat_name'],'category.php?cat_id='.$val['cat_id']);
			$data[$key] = $val;
		}

		return $data;
	}
	//取得分类信息
	public function catInfo($cat_id,$img=false){
		$sql = "select * from category where cat_id=$cat_id and is_show>=1";
		$data = $GLOBALS['db']->getRow($sql);
		if($data)
		{
			$data['cat_name'] = ucwords($data['cat_name']);
			$data['url'] = "category.php?cat_id=$cat_id";//$this->buildUri($data['cat_name'],$cat_id,CAT_NAME);
			$data['match'] = CAT_NAME.'-\d+(-\d+)?-'.$this->buildUri($data['cat_name'],-1);


		}

		return $data;
	}
	//取得分类的图片
	public function catImg($cat){
		$img = array();
		if($cat['img_id'])
		{
			$g = $GLOBALS['Goods']->goodsBasic($cat['img_id']);

			$img =  isset($g['original_img']) ? $g['original_img'] : '' ;
		}
		else
		{
			$idss =  $GLOBALS['Goods']->ids($cat['cat_id']);
			$where = ' where cat_id='.join($idss,' or cat_id=').' '.GOODS_DELETE_SHOW.' order by click_count desc';
			$g = $GLOBALS['Goods']->goodsList(0,1,$where);
			$g = @$g[0];
			$img =  @$g['goods_thumb'];

		}
		return $img;
	}
	//取得文章分类
	public function artCatTree($parent_id=0){
		$sql = "select * from article_cat where parent_id=$parent_id ";
		$data = $GLOBALS['db']->getAll($sql);
		foreach($data as $key=>$val)
		{
			$cat_id = $val['cat_id'];
			$val['cat_name'] = ucwords($val['cat_name']);

			$val['url'] = $this->buildUri($val['cat_name'],$cat_id,ART_CAT);
			if($val['parent_id']==0)
			{
				$val['children'] = $this->artCatTree($val['cat_id']);
			}

			$data[$key] = $val;
		}

		return $data;
	}
	//取得文章分类信息
	public function artCatInfo($cat_id){
		$sql = "select * from article_cat where cat_id=$cat_id ";
		$data = $GLOBALS['db']->getRow($sql);
		if($data)
		{
			$data['url'] = $this->buildUri($data['cat_name'],$cat_id,ART_CAT);
			$data['match'] = ART_CAT.'-\d+(-\d+)?-'.$this->buildUri($data['cat_name'],-1);
		}
		return $data;
	}
	//取得分类文章
	public function catArticles($start = 0, $size = 20,$where='')
	{
		$sql = "select article_id,title,add_time,is_open from article  $where order by add_time desc limit ".$start.",".$size."";

		$data = $GLOBALS['db']->getall($sql);
		foreach($data as $key=>$val)
		{
			$val['add_time'] = $this->dateFormat($val['add_time']);
			$val['url']      =  $this->buildUri($val['title'],$val['article_id'],ART);
			$data[$key] = $val;
		}
		return $data;
	}
	//取得文章内容
	public function article($a_id)
	{
		$sql = "select * from article where article_id=$a_id";
		$data = $GLOBALS['db']->getRow($sql);
		if(!$data) return false;
		$data['url'] = $this->buildUri($data['title'],$a_id,ART);
		$data['content'] = preg_replace("#http.*?images\/#","../images/",$data['content']);
		if($GLOBALS['_SEP']=='.')
		{
			$data['content'] = preg_replace("#\.\./images#","./images",$data['content']);

		}
		return $data;
	}
}

?>
