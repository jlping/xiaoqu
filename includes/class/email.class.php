<?php
/*
	发送邮件
*/
class Mails{
	
	//初始化
	public function __construct(){
		$cfg = $GLOBALS['_CFG'];
		require_once(ROOT_PATH . "includes/class/mail/class.phpmailer.php");
		
		$this->mail = $mail             = new PHPMailer();

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = $cfg['email']['smtp_server']; // SMTP server
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = 'ssl'; 
		$mail->Port       = $cfg['email']['port'];                    // set the SMTP port for the GMAIL server
		$mail->Username   = $cfg['email']['account']; // SMTP account username
		$mail->Password   = $cfg['email']['passwd'];        // SMTP account password
		$mail->Is_SSL = true;
		
		
	}

	//发送邮件
	public function sendMail($info){
		
		$mail = $this->mail;
		
		$mail->SetFrom($mail->Username, 'Ethnicharms');   //回复地址，显示的发件人
		$mail->Subject    = $info['subject'];  //主题
		$body = $info['body'];
		$mail->MsgHTML($body);
		$mail->SMTPDebug  = 2; 
		$address = $info['to_mail'];
		
		$mail->AddAddress($address, $info['to_name']);           //收件人名称，邮箱
		//$mail->AddAttachment("images/phpmailer.gif");      // 附件
		return $this->mail->Send();
	}



}
//$info =array('user_name'=>'service8customer@gmail.com','passwd'=>'519185tianx','subject'=>'subject','body'=>'测试','to_mail'=>'man0sions@hotmail.com','to_name'=>'last last');

//$mail = new Mails($info);

?>
