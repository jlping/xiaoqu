<?php

/*
	全局类
*/
class Debug extends Main {
	//删除分类
	public function delCats($list)
	{
		$where = ' cat_id='.join($list,' or cat_id=');
		$sql = "delete from category where $where";
		$res =  mysql_query($sql);
		return $res;
	}
	//更新分类
	public function updateCats($data){
		
		foreach($data as $key=>$val)
		{
			$sql = "update category set cat_name='".$val['cat_name']."', title='".$val['title']."',parent_id='".$val['parent_id']."',
			keywords='".$val['keywords']."',cat_desc='".$val['cat_desc']."',style='".$val['style']."' where cat_id='".$val['cat_id']."';";
			echo $sql."\n";
			mysql_query($sql);
		}
	}
	//删除商品
	public function delGoods($list){
		
		$where = ' goods_id='.join($list,' or goods_id=');
		$sql = "delete from goods where $where";
		$res =  mysql_query($sql);
		return $res;
	}
	//更新商品
	public function updateGoods($data){
		
		foreach($data as $key=>$val)
		{
			$sql = "update goods set goods_name='".$val['goods_name']."', title='".$val['title']."',cat_id='".$val['cat_id']."',
			keywords='".$val['keywords']."',goods_desc='".$val['goods_desc']."',goods_brief='".$val['goods_brief']."' ,
			market_price='".$val['market_price']."',shop_price='".$val['shop_price']."', cat_id='".$val['cat_id']."'  where goods_id='".$val['goods_id']."';";
			
			mysql_query($sql);
		}
		return true;
	}
	//更新分类下的商品
	public function updateCatGoods($data){
		$field = $data['field'];
		
		$s_name = !empty($data['s_name']) ? trim($data['s_name']).' ' : '';
		$e_name = !empty($data['e_name']) ? ' '.trim($data['e_name']) : '';
		$f_replace = (strtolower($data['f_replace']));
		$t_replace = (strtolower($data['t_replace']));
		$cat_id = $data['cat_id'];
		$ids = $GLOBALS['Goods']->ids($cat_id);
		$cat_ids = ' cat_id='.join($ids,' or cat_id=');
		
		
		if(!$f_replace)
		{
			$f_replace = '  ';
			$t_replace = ' ';

		}
		$val = "concat('$s_name', `$field`, '$e_name')";
		$sql = "update goods set `$field`=$val where $cat_ids";
		
		mysql_query($sql);
		$sql2 = "update goods set `$field` = replace(lower(`$field`),'$f_replace','$t_replace') where $cat_ids";
		
		mysql_query($sql2);
	
	}
	//更新分类下的商品价格
	public function updateCatGoodsPrice($data){
		$field = $data['field'];
		$val   = intval($data['val']);
		$cat_id = intval($data['cat_id']);
		$type = $this->escapeString($data['type']);
		if($type=='price')
		{
			$sql = "update goods set `$field` = `$field`+$val where cat_id=$cat_id";
		}
		elseif($type=='per')
		{
			$val = $val/100;
			$sql = "update goods set `$field` = `$field`+(`$field`*$val) where cat_id=$cat_id";
		}
		mysql_query($sql);
	
	
	}
	//生成meta
	public function createMeta($data){
		$title = $data['title'];
		$description = $data['description'];
		$cat_id = $data['cat_id'];
		$cat = $GLOBALS['Cate']->catInfo($cat_id);
		$ids = $GLOBALS['Goods']->ids($cat_id);
		
		//3中方式
		//前面加
		$keys1 = $this->randKeywords($cat['keywords'],4);
		
		$title1 = $title.' | '.$keys1[0];
		$keywords1 = $keys1[0].','.$keys1[1].','.$keys1[2];
		$description1 = $description.','.$keys1[2].','.$keys1[3];
		$sql1 = "update goods set title='".$title1."',keywords='".$keywords1."',goods_brief='".$description1."',
		goods_desc=concat(goods_desc,'|".join($keys1,',')."') where (goods_id%3=0) and cat_id=$cat_id";
		
		$keys2 = $this->randKeywords($cat['keywords'],4);

		$title2 = $keys2[0].','.$title.','.$keys2[1];
		$keywords2 = $keys2[0].','.$keys2[1].','.$keys2[2];
		$description2 = $keys2[2].','.$description.','.$keys2[3];
		$sql2 = "update goods set title='".$title2."',keywords='".$keywords2."',goods_brief='".$description2."',goods_desc=concat(goods_desc,'|".join($keys1,',')."') where (goods_id%3=1) and cat_id=$cat_id";
		

		$keys3 = $this->randKeywords($cat['keywords']);

		$title3 = $keys3[0].','.$keys3[1].','.$title;
		$keywords3 = $keys3[0].','.$keys1[1].','.$keys3[2];
		$description3 = $keys3[2].','.$keys3[3].','.$description;
		$sql3 = "update goods set title='".$title3."',keywords='".$keywords3."',goods_brief='".$description3."',goods_desc=concat(goods_desc,'|".join($keys1,',')."') where (goods_id%2=1) and cat_id=$cat_id";


		$keys4 = $this->randKeywords($cat['keywords']);

		$title4 = $title.','.$keys4[0].','.$keys4[2];
		$keywords4 = $keys4[0].','.$keys4[1].','.$keys4[2];
		$description4 = $keys4[2].','.$keys4[3].','.$description.','.$keys4[0];
		$sql4 = "update goods set title='".$title4."',keywords='".$keywords4."',goods_brief='".$description4."',goods_desc=concat(goods_desc,'|".join($keys1,',')."') where title='' and cat_id=$cat_id";
		mysql_query($sql1);
		mysql_query($sql2);
		mysql_query($sql3);
		mysql_query($sql4);
		return true;
	}
	//取得随机关键词,默认返回三个
	public function randKeywords($keywords,$num=4){
		$sp = preg_split("/,/",$keywords);
		if($num>count($sp)) $num = count($sp);
		$rand_keys = array_rand($sp,$num);
		$arr = array();
		foreach($rand_keys as $key=>$val)
		{
			$arr[] = $sp[$val];
		}
		return $arr;
	}
	//全部批处理
	public function batchAll($data){
		$data = json_decode(stripcslashes($data));
		$f = mysql_escape_string(strtolower($data->f_replace));
		$t = mysql_escape_string(strtolower($data->t_replace));
		$sql1 = "update goods set goods_name=replace(lower(goods_name),'$f','$t'),goods_desc=replace(lower(goods_desc),'$f','$t'),title=replace(lower(title),'$f','$t')
		,goods_brief=replace(lower(goods_brief),'$f','$t'),keywords=replace(lower(keywords),'$f','$t');";
		
		$res1 = mysql_query($sql1);
		$sql2 = "update category set cat_name=replace(lower(cat_name),'$f','$t'), keywords=replace(lower(keywords),'$f','$t'),title=replace(lower(title),'$f','$t'),cat_desc=replace(lower(cat_desc),'$f','$t')";
		$res2 = mysql_query($sql2);
		//echo $sql1."\n";
		//echo $sql2;
		return $res1 && $res2;
	}
	//导出商品
	public function exportGoods($goods_list){
		$table = "<table border=1>";
		$table.="<tr><th>分类ID</th><th>商品名称</th><th>货号</th><th>市场价</th><th>本店价</th><th>原图</th><th>相册</th><th>商品说明</th><th>属性</th><th>关键词</th><th>描述</th>
		<th>标题</th><th>数量</th><th>是否显示</th>";
		foreach($goods_list as $key=>$val)
		{
			
			$goods_id = $val['goods_id'];
			$gals = $GLOBALS['Goods']->gallerys($goods_id);
			$imgs = $this->combineGals($gals,$val['descs']);
			$attr = $GLOBALS['Goods']->attrListStr($val['attr_list']);
			$goods_desc = $val['goods_desc'];
			$goods_number = 99;
			$is_show = 1;
			$table.="<tr>
			<td>".$val['cat_id']."</td>
			<td>".$val['goods_name']."</td>
			<td>".$val['goods_sn']."</td>
			<td>".$val['market_price']."</td>
			<td>".$val['shop_price']."</td>
			<td>".$val['original_img']."</td>
			<td>".join($imgs,"|||")."</td>
			<td>".htmlspecialchars($goods_desc)."</td>
			<td>".$attr."</td>
			<td>".$val['keywords']."</td>
			<td>".$val['goods_brief']."</td>
			<td>".$val['title']."</td>
			<td>".$goods_number."</td>
			<td>".$is_show."</td>
			</tr>";
		}

		$table.="</table>";
		
		return $table;
		
	}
	//合并相册和descs
	public function combineGals($gals,$descs){
		$imgs = array();
		foreach($gals as $s_k=>$s_v)
		{
			$ori = $s_v['img_original'];
			if($ori)
			{
				$imgs[] = $ori;
			}
		}
		#$imgs = join($imgs,"|||");
		$sp = preg_split("/\|\|\|/",$descs);
		foreach($sp as $key=>$val)
		{
			if($val)
			{
				$imgs[] = 'images/con_pic/'.$val;
			}
		}
		return $imgs;
	}
	//导入数据
	public function importGoods($lines,$img_path){
			
		foreach($lines as $key=>$val)
		{
			
			if(!is_numeric($val['3']) &&  !is_numeric($val['4'])) continue;
			$data = array();
			
			$data['cat_id']		= $val['0'];
			$data['goods_name'] = $val['1'];
			$data['goods_sn']	= $val['2'];
			$data['market_price'] = $val['3'];
			$data['shop_price']	  = $val['4'];
			$data['original_img'] = $val['5'];
			$data['imgs']		  = $val['6'];
			$data['goods_desc']   = $val['7'];
			$data['attr']		  = $GLOBALS['Goods']->strToAttr(trim($val['8']));
			$data['keywords']	  = $val['9'];
			$data['goods_brief']  = $val['10'];
			$data['title']		  = $val['11'];
			$data['goods_number'] = $val['12'];
			$data['is_delete']	  = $val['13'];
			
			$GLOBALS['Goods']->goodsEdit($data,$img_path);
			
			
		}
	}
}
?>

