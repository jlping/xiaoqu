<?php

/*
	其他扩展类
*/

Class Other extends Main {

	public function typeids() {
		$sql = "select * from typeids  order by p_id asc";
		$data = $GLOBALS['db']->getAll($sql);
		return $data;
	}
	public function typeid($type_id) {
		$sql = "select name from typeids  where type_id=$type_id";
		$data = $GLOBALS['db']->getOne($sql);
		return $data;
	}
	//取得店铺销售总金额

	public function catOrder($cat_id) {
		$order_amount = 0;
		$goods_ids = $GLOBALS['db']->getAll("select goods_id from goods where cat_id=$cat_id");
		if (!$goods_ids)
			return false;
		$ids = array ();
		foreach ($goods_ids as $k => $v) {
			$ids[] = $v['goods_id'];
		}
		$join = join(",", $ids);

		$order_goods = $GLOBALS['db']->getAll("select * from order_goods where goods_id in ($join)");

		foreach ($order_goods as $key => $val) {
			$order_amount += $val['goods_price'];
		}

		$order['order_amount'] = $order_amount;
		$order['download'] = $GLOBALS['db']->getOne("select sum(download) from goods where cat_id=$cat_id");
		;
		$order['order_goods'] = $order_goods;
		$order['goods_amount'] = $this->counts("select count(*) from goods where cat_id=$cat_id");
		$order['goods_number'] = count($order_goods);
		return $order;
	}

	//封装小区商店
	public function xqCatInfo($cat) {

		$cat_id = $cat['cat_id'];
		$order = $this->catOrder($cat_id);

		$sql = "update category set download='" . $order['download'] . "',click_count=click_count+1,order_amount='" . $order['order_amount'] . "'" .
		",goods_amount='" . $order['goods_amount'] . "',goods_number='" . $order['goods_number'] . "' where cat_id=$cat_id";

		mysql_query($sql);

		$cat['members'] = @ $this->json2Array($cat['members']);
		$cat['mag']= $user = $GLOBALS['User']->userInfo($cat['mag_id']);
		$u['cat_id'] = $cat['cat_id'];
		$u['mag'] = 1;
		$GLOBALS['db']->autoExecute('users',$u,'update',"user_id=".$user['user_id']);

		$cat['img'] = preg_split("/,/", $cat['img']);
		$cat['add_time'] = $this->dateFormat($cat['add_time'], 'Y-m-d');
		$cat['zan'] = $this->counts("select count(*) from zan where id=$cat_id and type=1");
		$cat['log'] = $this->getCatLog(0, 2, " where l.cat_id=$cat_id  and l.r_id=0 order by l.add_time desc ");
		$cat['rank'] = @ $this->json2Array($cat['rank']);

		return $cat;
	}
	public function getCatLog($start = 0, $size = 10, $where, $replay = true) {
		$sql = "select l.*,u.user_name,u.avater from cat_log as l left join users as u on u.user_id = l.user_id $where limit $start,$size";

		$res = $GLOBALS['db']->getAll($sql);

		foreach ($res as $key => $val) {
			$log_id = $val['log_id'];
			$val['add_time'] = $this->dateFormat($val['add_time']);
			$val['user_info'] = $GLOBALS['User']->userInfo($val['user_id']);
			$val['reply_num'] = $GLOBALS['db']->getOne("select count(*)from cat_log where r_id=$log_id");
			if ($replay)
				$val['reply'] = $this->getCatLog(0, 100, " where l.r_id=$log_id order by l.add_time desc");
			$res[$key] = $val;
		}
		return $res;
	}
	//封装小区商品
	public function xqGoods($goods) {
		$goods_id = $goods['goods_id'];
		$cat_info = $this->xqCatInfo($GLOBALS['Cate']->catInfo($goods['cat_id']));

		$mag_names = @join(",",$this->magNames($cat_info));

		$type_id = intval($cat_info['mag']['type_id']);
		$info = '来自 ' . $cat_info['mag']['type'] . ' 的 ' . $cat_info['cat_name'] . ' 小店';

		//商品销售次数---goods_number
		$sell = $GLOBALS['db']->getOne("select count(*) from order_goods where goods_id=$goods_id");

		//更新好评
		$zan_type = 2;
		if($goods['cat_type']==1)
			$zan_type = 3;
		$zan = $goods['zan'] = $this->counts("select count(*) from zan where id=$goods_id and type=$zan_type");

		$h['h3'] = $h3 = $GLOBALS['db']->getOne("select count(*) from review where id=" . $goods_id . " and type=3 and score=3");
		$h['h2'] = $h2 = $GLOBALS['db']->getOne("select count(*) from review where id=" . $goods_id . " and type=3 and score=2");
		$h['h1'] = $h1 = $GLOBALS['db']->getOne("select count(*) from review where id=" . $goods_id . " and type=3 and score=1");

		$haop = $h3 * 100 + $h2 * 60 + $h1 * 20;
		$goods['h'] = $h;


		$sql = "update goods set zan=$zan, haoping=$haop,goods_number=$sell,style_name='$info',relates=$type_id,mag_names='$mag_names' where goods_id=$goods_id";
		//echo $sql;die();
		mysql_query($sql);


		$gal = $goods['gallery'];
		$gals = array ();
		foreach ($gal as $key => $val) {
			$gals[] = $val['img'];
		}
		$goods['gal'] = join($gals, ',');
		$goods['goods_briefs'] = @ $this->json2Array($goods['goods_brief']);



		$goods['rank'] = @ $this->json2Array($goods['rank']);
		//print_r($goods);die();
		return $goods;
	}
    public function magNames($cat){
    	$names = array();
    	foreach($cat['members'] as $key=>$val)
    	{
			$names[] = $val['name'];
    	}
    	$names[] = $cat['mag']['info']['name'];
		return $names;
    }
	//设置用户权限
	public function setUserMag($data, $c_id) {

		$members = $this->json2Array(stripcslashes($data['members']));
		foreach ($members as $key => $val) {
			$mag = $val['mag'];
			$user_id = $val['user_id'];
			mysql_query("update users set mag=$mag,cat_id=$c_id where user_id=$user_id");
		}
		mysql_query("update users set mag=1,cat_id=$c_id where user_id='" . $_SESSION['user_id'] . "'");

	}

	//取得用户签到排行
	public function qiandaoList($start = 0, $size = 10, $where = "") {
		$sql = "select sum(amount) as qd_amount,count(*) as qd_count ,geter from gold_log where buyer='login' group by geter order by qd_amount desc limit $start,$size";

		$data = $GLOBALS['db']->getAll($sql);
		foreach ($data as $key => $val) {

			$user_id = str_replace("u_", "", $val['geter']);
			$user = $GLOBALS['User']->userInfo($user_id);
			$val['user'] = $user;
			$data[$key] = $val;
		}
		//print_r($data);die();
		return $data;

	}

	//更新小店排名
	public function updateCatRank($cat, $type, $start) {

		foreach ($cat as $key => $val) {
			$cat_type = $this->catType($val['cat_id']);

			$or_rank = @ $this->json2Array($val['rank']);
			$rank = array (
				'add_time' => intval($or_rank['add_time']
			),
			'order_amount' => intval($or_rank['order_amount']), 'click_count' => intval($or_rank['click_count']), 'goods_amount' => intval($or_rank['goods_amount']), 'download' => intval($or_rank['download']), 'goods_number' => intval($or_rank['goods_number'])

			);

			$rank[$type] = $key + $start +1;

			$cat_id = $val['cat_id'];
			$c['rank'] = json_encode($rank);
			$c['type_id'] = $val['mag']['type_id'];
			$c['type'] = $cat_type;
			$GLOBALS['db']->autoExecute('category', $c, 'update', "cat_id=$cat_id");

		}

	}
	//更新商品排名
	public function updateGoodsRank($goods_list, $type, $start) {
		$xl = $GLOBALS['db']->getRow("select max(goods_number) as max,avg(goods_number) as avg from goods");

		foreach ($goods_list as $key => $val) {
			//print_r($val);die();
			$haoping_per = @($val['haoping']/($val['h']['h1']+$val['h']['h2']+$val['h']['h3']));
			$haoping_per = $haoping_per>0 ? round($haoping_per) : 60;
			$or_rank = @ $this->json2Array($val['rank']);
			$rank = array (
				'add_time' => intval($or_rank['add_time']
			), 'shop_price' => intval($or_rank['shop_price']), 'click_count' => intval($or_rank['click_count']), 'haoping' => intval($or_rank['haoping']), 'download' => intval($or_rank['download']), 'goods_number' => intval($or_rank['goods_number']),'haoping_per'=>$haoping_per);
			$rank[$type] = $key + $start +1;

			$goods_id = $val['goods_id'];
			$R = $haoping_per/100;

			$a1 = 3*$xl['max']*86400;
			$a2 = time()-strtotime($val['add_time'])+86400;
			$a3 = $val['goods_number'];
			$a4 = pow(2.7828,10*$R);
			$a5 = 120*$xl['avg'];

			$L = ($a1/$a2)+$a3+($a4/$a5);
			//echo $haoping_per.'='.$val['goods_id'].'='.$a1.'='.$a2.'='.$a3.'='.$a4.'='.$a5.'='.$L."\n";

			$c['rank'] = json_encode($rank);
			$c['haoping_per'] = $haoping_per;
			$c['L'] = $L;
			$GLOBALS['db']->autoExecute('goods', $c, 'update', "goods_id=$goods_id");

		}
		//die();

	}
	//取得左侧列表数据
	public function leftData($cat_type=0) {
		$jf_goods_ids = $this->jfGoodsIds();
		$jf = JF_MALL_ID;
		$new_cat = $GLOBALS['Cate']->categoryAll(0, 20, "where is_show=2 order by add_time desc");

		$order_cat = $GLOBALS['Cate']->categoryAll(0, 5, "where cat_id!=$jf order by order_amount desc");
		//取得最近订单商品
		$hot_goods = $this->recentOrderGoods(0, 5,$cat_type);

		//取得最近交易
		$recent_order = $this->recentOrder(0, 15,$cat_type);
		//print_r($recent_order);die();
		//$hot_goods = $GLOBALS['Goods']->goodsList(0,5,"where cat_id!=$jf order by goods_number desc");
		//$week_cat = $GLOBALS['Cate']->categoryAll(0,5,'where sort_order<10 order by sort_order asc');
		$week_goods = $GLOBALS['Goods']->goodsList(0, 10, 'where cat_type='.$cat_type.' and  sort_order<10 order by sort_order asc');

		foreach ($hot_goods as $key => $val) {
			preg_match("/来自 ([^ ]+) 的 [^ ]+ 小店/", $val['style_name'], $m);
			$val['type_name'] = @ $m[1];
			$hot_goods[$key] = $val;
		}

		foreach ($week_goods as $key => $val) {
			preg_match("/来自 ([^ ]+) 的 [^ ]+ 小店/", $val['style_name'], $m);
			$val['type_name'] = @ $m[1];
			$week_goods[$key] = $val;
		}

		$qd_user = $GLOBALS['Other']->qiandaoList(0, 5);
		//$logs = $GLOBALS['Other']->getCatLog(0,10," where  l.r_id=0 order by l.add_time desc ",false);
		//print_r($order_cat);die();

		$notice = $GLOBALS['Cate']->catArticles(0, 10, " where cat_id=1 and is_open=1  ");
		//print_r($notice);die();
		$GLOBALS['sm']->assign('notice', $notice);

		$GLOBALS['sm']->assign('new_cat', $new_cat);
		$GLOBALS['sm']->assign('order_cat', $order_cat);
		$GLOBALS['sm']->assign('hot_goods', $hot_goods);
		$GLOBALS['sm']->assign('qd_user', $qd_user);
		//$GLOBALS['sm']->assign('logs_tip',	$logs);
		//$GLOBALS['sm']->assign('week_cat',	$week_cat);
		$GLOBALS['sm']->assign('week_goods', $week_goods);

		$GLOBALS['sm']->assign('recent_order', $recent_order);
		$GLOBALS['sm']->assign('jf_goods_ids', $jf_goods_ids);

		$cnzz = $GLOBALS['db']->getOne("select value from config where code='cnzz'");
		$cnzz = $this->json2Array($cnzz);

		$GLOBALS['sm']->assign('cnzz', $cnzz);
		//print_r($GLOBALS['Goods']->recentGoods());die();
		$GLOBALS['sm']->assign('recentGoods', $GLOBALS['Goods']->recentGoods());

	}
	//取得最近订单商品
	public function recentOrderGoods($start = 0, $size = 10,$cat_type=0) {
		$time = strtotime("-5 day");

		$jf = JF_MALL_ID;
		$order = $GLOBALS['db']->getAll("select order_id from order_info where add_time>=$time  order by add_time desc");

		$ids = array ();
		foreach ($order as $key => $val) {
			$ids[] = $val['order_id'];
		}
		$ids = $ids ? $ids : array(0);
		$goods_ids = $GLOBALS['db']->getAll("select goods_id from order_goods where order_id in (" . join(',', $ids) . ")");

		$gids = array ();
		foreach ($goods_ids as $key => $val) {
			$gids[] = $val['goods_id'];
		}
		$gids = array_count_values ($gids);

		asort($gids);

		$gids = array_reverse($gids,true);

		$gkeys = array_slice(array_keys($gids),0,100,true);

		$gkeys = $gkeys ? $gkeys : array(0);


		$sql = "where cat_id!=$jf and goods_id in (" . join(',', $gkeys) . ") and cat_type=$cat_type order by goods_number desc";
		$hot_goods = $GLOBALS['Goods']->goodsList(0, 20, $sql);
		//print_r($hot_goods);die();
		//echo time();die();
		foreach($hot_goods as $key=>$val)
		{
			$val['order_times'] = $gids[$val['goods_id']];
			$hot_goods[$key] = $val;
		}

		$hot_goods = $this->array_sort($hot_goods,'order_times','desc');

		return $hot_goods;
	}
	//积分商城的goods_id
	public function jfGoodsIds() {
		$jf = JF_MALL_ID;
		$goods = $GLOBALS['db']->getAll("select goods_id from goods where cat_id=$jf");
		$gids = array ();
		foreach ($goods as $key => $val) {
			$gids[] = $val['goods_id'];
		}
		return $gids;
	}

	//recentOrder
	public function recentOrder($start, $size,$cat_type=0) {
		$jf = JF_MALL_ID;
		$sql = "select og.goods_name,og.goods_id,o.user_id,o.add_time,o.order_id,u.user_name,u.info,u.type_id from order_goods as og inner join order_info as o
		 		on og.order_id=o.order_id inner join goods as g on g.goods_id=og.goods_id inner join users as u on u.user_id=o.user_id and g.cat_id!=$jf and g.cat_type=$cat_type order by o.add_time desc limit $start,$size";
		$data = $GLOBALS['db']->getAll($sql);
		foreach ($data as $key => $val) {
			$val['info'] = $this->json2Array($val['info']);
			$type_id = $val['type_id'];
			$val['type'] = $GLOBALS['db']->getOne("select name from typeids where type_id=$type_id");
			$data[$key] = $val;
		}
		//print_r($data);die();
		return $data;
	}
	//log filter
	public function logFilter($table, $type, $kw = '', $kwd, $start, $size) {

		if ($table == 'cat_log') {
			if ($kw)
				$kwd = "  (content like '%$kw%') and";
			$where = " where  $kwd l.r_id=0  and  l.type=$type order by l.add_time desc ";

			$log = $this->getCatLog($start, $size, $where, false);

			$GLOBALS['sm']->assign('logs', $log);
			$count = $this->counts("select count(*) from cat_log as l $where");
			$pager = $this->pager($count, $size, "index.php?index_act=cat_log&kw=$kw&log_table=$table&log_type=$type");
			$GLOBALS['sm']->assign('pagers', $pager);
			//print_r($log);die();
		}
		elseif ($table == 'gold_log') {
			if ($kw)
				$kwd = "  (comment like '%$kw%') and";
			$where = "where $kwd  type=$type ";
			$log = $GLOBALS['User']->goldLog($start, $size, $where);
			$GLOBALS['sm']->assign('logs', $log);
			$count = $this->counts("select count(*) from gold_log  $where");
			$pager = $this->pager($count, $size, "index.php?index_act=cat_log&kw=$kw&log_table=$table&log_type=$type");
			$GLOBALS['sm']->assign('pagers', $pager);
			//print_r($log);die();
		}
		elseif ($table == 'review') {
			if ($kw)
				$kwd = "  (content like '%$kw%') and";
			$where = "where $kwd  type=$type and rp_id=0 order  by add_time desc";
			$log = $GLOBALS['User']->reviewList($start, $size, $where, true,true);
			//print_r($log);die();
			$GLOBALS['sm']->assign('logs', $log);
			$count = $this->counts("select count(*) from review  $where");
			$pager = $this->pager($count, $size, "index.php?index_act=cat_log&kw=$kw&log_table=$table&log_type=$type");
			$GLOBALS['sm']->assign('pagers', $pager);
			//print_r($log);die();
		}
	}
	//keywords group
	public function keywords($start = 0, $size = 10, $where = "") {
		$sql = "select * from keywords $where limit $start,$size";

		$data = $GLOBALS['db']->getAll($sql);
		return $data;
	}


	//goodsKeywordsMaiDian
	public function goodsKeywordsMaiDian($type=1,$field='keywords') {
		if ($_POST) {
			//$_POST[$field.'7']= preg_replace("/，/",",",$_POST[$field.'7']);

			$_POST[$field] = @ join(",", $_POST[$field]);
			$_POST[$field] .= ',' . $_POST[$field.'7'];

			$_POST[$field] = preg_split("/,/", $_POST[$field]);
			foreach ($_POST[$field] as $key => $val) {
				if (empty ($val))
				{
					unset ($_POST[$field][$key]);
					continue;
				}

				$k['times'] = 'times+1';
				$k = $GLOBALS['db']->getRow("select * from keywords where keyword='$val' and type=$type");
				if ($k) {
					$k['times'] = $k['times'] + 1;
					$GLOBALS['db']->autoExecute('keywords', $k, "update", "k_kd=" . $k['k_kd']);
				} else {
					$k['keyword'] = $val;
					$k['times'] = 1;
					$k['type'] = $type;

					$GLOBALS['db']->autoExecute('keywords', $k);
				}
			}
			$_POST[$field] = @ join(",", $_POST[$field]);


		}

		return $_POST[$field];

	}
	/*
	 * 取得精艺数据
	 * */
	 public function jyData(){
		$db = new Mysql('localhost:3306','root', 'root', 'tem');
		$data = $db->getAll('select * from item_group limit 300');
		foreach($data as $key=>$val)
		{
			$zuzhang = $val['zuzhang'];

			$members = preg_split("/,/",$val['g_member']);
			//$members = "'".join("','",$members)."'";
			$user = @$GLOBALS['User']->userInfo($val['u_id']);

			if(!isset($user['user_id']))
			{

				continue;
			}
			$img = preg_replace("#/uploads/allimg/\d+#","",$val['litpic']);
			$img = $img ? 'uploads/jingyi'.$img: '';
			$sql = "insert into category(`mag_id`,`cat_name`,`guwen`,`phone`,`img`,`type`,`add_time`) values('".$user['user_id']."'," .
					"'".$val['g_name']."','".$val['jishuguwen']."','".$val['tel']."','".$img."',1,'".time()."');";
			//$GLOBALS['db']->query($sql);
			//$cat_id = mysql_insert_id();
			echo $sql."\n";
			$data['key'] = $val;
			//print_r($val);
		}
		//print_r($data);
	 }
	 /*
	  * 取得店铺 类型 0-两者无，1-有小区无精益，2-无小区有精益，3-两者有
	  * */
	  public function catType($cat_id){
		$arr = array();
		$types = $GLOBALS['db']->getAll("select distinct cat_type from goods where cat_id=$cat_id");
		foreach($types as $key=>$val)
		{
			$arr[] = $val['cat_type'];
		}
		$sum = array_sum($arr);
		if(count($arr)==0) //两者无
			return 0;
		if(count($arr)==1)
		{
			if($sum==0) //有小区无精益
				return 1;
			if($sum==1) //无小区有精益
				return 2;
		}
		if(count($arr)==2)
			return 3;

	  }
	  /*
	   * 判断当前访问 该调取那些内容
	   * */
	   public function inCatTypes($type=0){
	   	if(isset($_SESSION['cat_type']) || isset($_COOKIE['cat_type'])) //用户已经选择过
	   	{
	   		if($type==0) //之前用户选择小区
	   		{
	   			$types = '0,1,3';
	   		}
	   		elseif($type==1) //之前用户选择精益
	   		{
	   			$types = '0,2,3';
	   		}
	   	}
	   	else //如果用户没选择过
	   	{
			$types = '0,1,3';
	   	}

		return $types;
	   }

}
?>