<?php

$gold =  array(
	'gold'=>array(
		'register'=>50,   //完善注册，+50
		'login'=>array(   //每天登陆  +10，每天只加1次 (+users)
			'every_day'=>1,
			'gold'=>5
		),

		'cat_log'=>array(  //店铺日志 +10,每天只加1次  (+category)
			'every_day'=>1,
			'gold'=>5
		),
		'category'=>array(
			'member'=>10, //每个个店员 +10
			'img'=>5,     //每张图片 +5
			'video'=>100  //每个视频 +100
		),
		'order_review'=>array(
			'return' =>0.5, //订单评价 用户+ 订单金额*0.5
			'3'=>0.5,       //推广成功，返回category *0.5
			'2'=>0,         //暂无效果，不返回
			'1'=>-0.5       //推广失败，-0.5
		)



	),

	'actions'=>array(    //店铺日志
		'cat_add'=>'团聚小区的第<b class="yellow">%d</b>家店铺——%s 已于 %s 开业了，进去逛逛吧', //记录店铺创建
		'goods_add'=>'%s 于 %s 上架了新的产品——%s，售价 %d 积分，去看看吧',	//记录商品添加
		'jf_goods_add'=>'%s 于 %s 上架了新的产品——%s，兑换 %d 积分，去看看吧',	//记录商品添加

	)

);

//定义积分商城的id
define('JF_MALL_ID',100);
//聊天定义管理者id
$chat_manager = array(1,7,88,2137);
?>