-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 01 月 22 日 12:32
-- 服务器版本: 5.1.66
-- PHP 版本: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `xiaoqu`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `user_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `add_time` int(11) NOT NULL DEFAULT '0',
  `last_login` int(11) NOT NULL DEFAULT '0',
  `last_ip` varchar(15) NOT NULL DEFAULT '',
  `action_list` text NOT NULL,
  `nav_list` text NOT NULL,
  `lang_type` varchar(50) NOT NULL DEFAULT '',
  `agency_id` smallint(5) unsigned NOT NULL,
  `suppliers_id` smallint(5) unsigned DEFAULT '0',
  `todolist` longtext,
  `role_id` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_name` (`user_name`),
  KEY `agency_id` (`agency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `admin_user`
--

INSERT INTO `admin_user` (`user_id`, `user_name`, `email`, `password`, `add_time`, `last_login`, `last_ip`, `action_list`, `nav_list`, `lang_type`, `agency_id`, `suppliers_id`, `todolist`, `role_id`) VALUES
(3, 'admin', 'xcb@cmbc.com.cn', '21232f297a57a5a743894a0e4a801fc3', 1312418168, 1372238411, '127.0.0.1', 'all', '商品列表|goods.php?act=list,订单列表|order.php?act=list,用户评论|comment_manage.php?act=list,会员列表|users.php?act=list,商店设置|shop_config.php?act=list_edit', '', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `article_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` smallint(5) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `author` varchar(30) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `is_open` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`article_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `article_cat`
--

CREATE TABLE IF NOT EXISTS `article_cat` (
  `cat_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `cat_desc` varchar(255) NOT NULL DEFAULT '',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`cat_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `attribute`
--

CREATE TABLE IF NOT EXISTS `attribute` (
  `attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attr_name` varchar(60) NOT NULL DEFAULT '',
  `attr_type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`attr_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `rec_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `session_id` char(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `goods_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `goods_sn` varchar(60) NOT NULL DEFAULT '',
  `goods_name` varchar(120) NOT NULL DEFAULT '',
  `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `goods_number` smallint(5) unsigned NOT NULL DEFAULT '0',
  `goods_attr` text NOT NULL,
  `goods_attr_id` varchar(255) NOT NULL DEFAULT '',
  `pack_goods_id` int(10) NOT NULL,
  PRIMARY KEY (`rec_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `mag_id` int(10) NOT NULL COMMENT '创建者ID',
  `members` text NOT NULL,
  `cat_name` varchar(90) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sort_order` tinyint(1) unsigned NOT NULL DEFAULT '50',
  `style` tinyint(3) NOT NULL,
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `type_id` int(10) NOT NULL,
  `img` text NOT NULL,
  `cat_content` text NOT NULL,
  `add_time` int(10) NOT NULL,
  `click_count` int(10) NOT NULL,
  `gold` int(10) NOT NULL COMMENT '店铺积分',
  PRIMARY KEY (`cat_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `category`
--

INSERT INTO `category` (`cat_id`, `mag_id`, `members`, `cat_name`, `address`, `phone`, `parent_id`, `sort_order`, `style`, `is_show`, `title`, `type_id`, `img`, `cat_content`, `add_time`, `click_count`, `gold`) VALUES
(11, 13, '{}', '桥东街里幸福事儿（太原分行）', '山西省太原市迎泽区桥东街', '18636180413', 0, 50, 0, 1, '', 0, 'uploads/20140118/39f8dd84ef.jpg,uploads/20140118/6b8fd7de45.jpg,uploads/20140118/69b5075545.jpg,uploads/20140118/8d0c9ad899.jpg,uploads/20140118/f7b70623bf.jpg', '', 1389945061, 47, 10),
(2, 6, '{}', '太原民生永乐苑幸福小区', '山西省太原市万柏林区千峰北路彭西二巷口往东100米路南', '0351-6267240', 0, 50, 0, 1, '微笑是我们的语言，专注是我们的信念！', 0, 'uploads/20140116/09e50f5d9e.jpg', '', 1389847907, 52, 10),
(12, 18, '{}', '大连佰佳店', '大连市沙河口区软件园路亿达东方圣克拉小区', '0411-62625441', 0, 50, 0, 1, '百家分店，百年基业，百年兴旺', 0, 'uploads/20140117/a2d42b887d.jpg,uploads/20140117/af71c22cac.jpg,uploads/20140117/c71aea746e.jpg,uploads/20140117/639c0f7712.jpg', '', 1389947245, 14, 25),
(8, 10, '{}', '枫叶舞秋山（民生大连）', '辽宁省大连市高新园区红凌路768-4号峦翠园公建', '0411-62596042', 0, 50, 0, 1, '无论春夏秋冬，民生助您成功！', 0, 'uploads/20140116/6ae1ea16a4.jpg,uploads/20140116/be696f37cd.jpg,uploads/20140116/1c58914ad0.jpg', '店掌柜承诺本店三体验——\\n1、高性价比体验：智能便民，功能齐全，财富增值，一生难忘。\\n2、增值服务体验：观影（视觉）、听歌（听觉）、品茗（味觉）、养身（触觉）——全方位尊享。\\n3、财富管理体验：民生财富，让您的钱包鼓起来！！！\\n\\n您信不信\\n      反正我信了。', 1389861226, 53, 20),
(9, 14, '{}', '福享御庭阁（民生大连）', '大连市沙河口区台山街202号大华御庭二期', '0411-62274033', 0, 50, 0, 1, '存入人生价值，取出款款价值。', 0, 'uploads/20140116/ee296f4e95.jpg,uploads/20140116/1860c3e265.jpg,uploads/20140116/6fd7df089e.jpg,uploads/20140116/d5b4a28b88.jpg', '', 1389866121, 31, 20),
(7, 9, '{}', '太原水西关支行铜锣湾城市府邸', '铜锣湾宝地小区B1座33号民生银行', '18647425681', 0, 50, 0, 1, '用民生的心系小区的情 用真心感动每一个家庭', 0, 'uploads/20140116/1fef39a1b1.jpg,uploads/20140116/654b9814c4.jpg,uploads/20140116/da77aebd3b.jpg,uploads/20140116/e3b016c0bd.jpg,uploads/20140116/d508747cb8.jpg,uploads/20140116/320a5a134a.jpg,uploads/20140116/cde16dee51.jpg,uploads/20140116/358736003c.jpg', '火车票、智能派件箱、智家贷都是我们的营销利器\\n  四大特点：1.智能派件箱，快速、便捷，随到随取\\n             2.火车售票机，一证、一卡，来店即取\\n             3.网银服务机，理财、基金，当下办理\\n             4.手机体验机，积分、礼品，到店就领', 1389858222, 78, 10),
(13, 11, '', '文兴苑', '', '', 0, 50, 0, 1, NULL, 0, 'uploads/20140117/7e7bf084a7.jpg,uploads/20140117/982978fb45.jpg,uploads/20140117/94dbea709f.jpg,uploads/20140117/7d707a0a97.jpg,uploads/20140117/347df404e3.jpg,uploads/20140117/1837ee1a45.jpg,uploads/20140117/505450fc17.jpg', '', 1389964521, 6, 0),
(15, 3, '{}', '（公测样板间）七号店', '虚拟店', '13810101521', 0, 50, 0, 1, '请大家来看哦！', 0, 'uploads/20140122/bec3a4182a.jpg,uploads/20140122/fa3631e271.jpg,uploads/20140122/cf27f77192.jpg', '', 1390362466, 2, 15);

-- --------------------------------------------------------

--
-- 表的结构 `cat_log`
--

CREATE TABLE IF NOT EXISTS `cat_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-店铺日志，2-店铺动作',
  `content` varchar(280) NOT NULL,
  `add_time` int(10) NOT NULL,
  `cat_id` int(6) NOT NULL,
  `r_id` int(10) NOT NULL COMMENT '回复id',
  `user_id` int(6) NOT NULL COMMENT '发布者id',
  `status` tinyint(1) NOT NULL COMMENT '1-当前，0-过期',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=110 ;

--
-- 转存表中的数据 `cat_log`
--

INSERT INTO `cat_log` (`log_id`, `type`, `content`, `add_time`, `cat_id`, `r_id`, `user_id`, `status`) VALUES
(109, 2, '团聚小区的第<b class="yellow">8</b>家店铺——<a href="category.php?cat_id=15">（公测样板间）七号店</a> 已于 2014-01-22 11:48:15 开业了，进去逛逛吧', 1390362495, 15, 0, 3, 0),
(108, 1, '铜锣湾自助银行和物业公司合作，在物业收费处装乐收银，通过小区平台推送短信，为小区住户办理智家卡，吸引客户前来我行办理业务。', 1390216860, 7, 0, 9, 0),
(107, 1, '<img src=\\"http://dq.cmbc.com.cn/xiaoqu/includes/static/ueditor/dialogs/emotion/images/tsj/t_0006.gif\\"/>', 1389949037, 12, 106, 5, 0),
(105, 1, '够喜庆', 1389945828, 11, 104, 5, 0),
(106, 2, '团聚小区的第<b class="yellow">6</b>家店铺——<a href="category.php?cat_id=12">大连佰佳店</a> 已于 2014-01-17 16:28:54 开业了，进去逛逛吧', 1389947334, 12, 0, 18, 0),
(102, 1, '<img src=\\"http://dq.cmbc.com.cn/xiaoqu/includes/static/ueditor/dialogs/emotion/images/jx2/j_0006.gif\\"/>', 1389868368, 0, 99, 5, 0),
(101, 2, '团聚小区的第<b class="yellow">5</b>家店铺——<a href="category.php?cat_id=9">福享御庭阁</a> 已于 2014-01-16 17:58:31 开业了，进去逛逛吧', 1389866311, 9, 0, 14, 0),
(100, 2, '团聚小区的第<b class="yellow">5</b>家店铺——<a href="category.php?cat_id=9">福享御庭阁</a> 已于 2014-01-16 17:57:54 开业了，进去逛逛吧', 1389866274, 9, 0, 14, 0),
(99, 1, '万众瞩目、殷切盼望——枫叶舞秋山店隆重开业啦~~~<img src=\\"http://dq.cmbc.com.cn/xiaoqu/includes/static/ueditor/dialogs/emotion/images/jx2/j_0028.gif\\"/>', 1389864056, 8, 0, 10, 0),
(98, 2, '团聚小区的第<b class="yellow">4</b>家店铺——<a href="category.php?cat_id=8">枫叶舞秋山</a> 已于 2014-01-16 16:48:00 开业了，进去逛逛吧', 1389862080, 8, 0, 10, 0),
(97, 2, '团聚小区的第<b class="yellow">6</b>家店铺——<a href="category.php?cat_id=7">水西关支行铜锣湾城市府邸</a> 已于 2014-01-16 15:43:55 开业了，进去逛逛吧', 1389858235, 7, 0, 9, 0),
(93, 2, '团聚小区的第<b class="yellow">2</b>家店铺——<a href="category.php?cat_id=2">永乐苑幸福小区</a> 已于 2014-01-16 12:54:05 开业了，进去逛逛吧', 1389848045, 2, 0, 6, 0),
(94, 1, '恭喜开店成功!', 1389848881, 2, 93, 7, 0),
(104, 2, '团聚小区的第<b class="yellow">5</b>家店铺——<a href="category.php?cat_id=11">桥东街里幸福事儿（太原分行）</a> 已于 2014-01-17 15:52:21 开业了，进去逛逛吧', 1389945141, 11, 0, 13, 0);

-- --------------------------------------------------------

--
-- 表的结构 `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) NOT NULL DEFAULT '2' COMMENT '1-系统变量,2-自定义变量',
  `value_type` varchar(10) NOT NULL DEFAULT 'text',
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- 转存表中的数据 `config`
--

INSERT INTO `config` (`c_id`, `type`, `value_type`, `name`, `code`, `value`) VALUES
(7, 1, 'text', '网站名称', 'shop_name', '小区金融'),
(8, 1, 'text', '网站标题', 'shop_title', '民生银行'),
(10, 1, 'text', '关键词', 'shop_keywords', ''),
(13, 1, 'text', '网站描述', 'shop_desc', ''),
(23, 2, 'text', '汇率', 'currency', 'EUR:€%s:0.7639,USD:$%s:1.000,GBP:￡%s:0.6460,JPY:￥%s円(税込):99.0880'),
(24, 2, 'text', '分享', 'add_this', ''),
(26, 2, 'text', 'google代码', 'google_code', ''),
(27, 2, 'text', 'google翻译', 'google_translate', '<div id="google_translate_element"></div><script type="text/javascript">\nfunction googleTranslateElementInit() {\n  new google.translate.TranslateElement({pageLanguage: ''en'', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, ''google_translate_element'');\n}\n</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>'),
(28, 2, 'text', 'facebook评论', 'fb_comment', ''),
(29, 1, 'text', '缩略图尺寸', 'thumb_size', '350*350'),
(30, 1, 'text', '缓存时间', 'cache_time', '7200'),
(31, 2, 'text', '网站编号', 'site_id', '001'),
(32, 1, 'json', 'email', 'email', '{"service_type":"2","smtp_server":"40.49.79.8","smtp_port":"25","account":"youth@cmbc.com.cn","passwd":"a111111","smtp_ssl":"on"}'),
(33, 2, 'text', '调试模式', 'debug', '0'),
(34, 2, 'text', '模板路径', 't_path', '/templates/default'),
(35, 2, 'text', 'shipping_from', 'shipping_from', ''),
(36, 2, 'text', 'cat_keywords', 'cat_keywords', ''),
(37, 2, 'text', 'cat_title', 'cat_title', ''),
(38, 2, 'text', 'cat_description', 'cat_description', ''),
(39, 2, 'text', 'fb_init', 'fb_init', ''),
(41, 2, 'text', 'live_chat', 'live_chat', ''),
(42, 2, 'text', 'cnzz', 'cnzz', '');

-- --------------------------------------------------------

--
-- 表的结构 `discount`
--

CREATE TABLE IF NOT EXISTS `discount` (
  `discount_id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) CHARACTER SET utf8 NOT NULL,
  `value` tinyint(3) NOT NULL,
  `add_time` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-未使用,2-已使用',
  `use_info` varchar(255) CHARACTER SET utf8 NOT NULL,
  `order_id` int(10) NOT NULL COMMENT '对应订单id生产的优惠码',
  PRIMARY KEY (`discount_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- 转存表中的数据 `discount`
--

INSERT INTO `discount` (`discount_id`, `code`, `value`, `add_time`, `status`, `use_info`, `order_id`) VALUES
(41, 'fc9fc90e70', 5, 1383272182, 1, '', 0),
(42, 'd314381b70', 5, 1383272189, 1, '', 0),
(43, 'afd3ddce7f', 5, 1383272194, 1, '', 0),
(44, '15737468f1', 5, 1383272199, 1, '', 0),
(45, '281d544414', 5, 1383272205, 1, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `gold_log`
--

CREATE TABLE IF NOT EXISTS `gold_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `amount` int(10) NOT NULL COMMENT '金额',
  `buyer` varchar(10) NOT NULL COMMENT '消费金币者',
  `geter` varchar(10) NOT NULL COMMENT '得到金币者',
  `type` tinyint(1) NOT NULL COMMENT '1-购买商品,2-店铺划分,3-系统',
  `comment` varchar(255) NOT NULL COMMENT '注释',
  `add_time` int(10) NOT NULL COMMENT '日期',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- 转存表中的数据 `gold_log`
--

INSERT INTO `gold_log` (`log_id`, `amount`, `buyer`, `geter`, `type`, `comment`, `add_time`) VALUES
(1, 50, 'system', 'u_5', 3, '注册完成，系统返回50 小区币 给 fanyifei  操作人:system', 1389840906),
(2, 10, 'login', 'u_5', 3, '签到成功，系统返回10 小区币 给 fanyifei 操作人:system', 1389840915),
(3, 50, 'system', 'u_7', 3, '注册完成，系统返回50 小区币 给 zhangdujian  操作人:system', 1389845065),
(4, 50, 'system', 'u_6', 3, '注册完成，系统返回50 小区币 给 gaoqian  操作人:system', 1389846731),
(5, 0, 'system', 'c_2', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389848045),
(6, 5, 'system', 'c_2', 3, '添加图片1张 系统分配 5 小区币 操作人:system', 1389848045),
(7, 10, 'login', 'u_6', 3, '签到成功，系统返回10 小区币 给 gaoqian 操作人:system', 1389848321),
(8, 50, 'system', 'u_8', 3, '注册完成，系统返回50 小区币 给 pangwenhui  操作人:system', 1389848801),
(9, 5, 'cat_log', 'c_2', 3, '发布日志，系统返回5 小区币 操作人:zhangdujian', 1389848881),
(10, 50, 'system', 'u_9', 3, '注册完成，系统返回50 小区币 给 zhangwei  操作人:system', 1389853156),
(11, 5, 'cat_log', 'c_', 3, '发布日志，系统返回5 小区币 操作人:', 1389855559),
(12, 10, 'login', 'u_9', 3, '签到成功，系统返回10 小区币 给 zhangwei 操作人:system', 1389856361),
(13, 0, 'system', 'c_4', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389857071),
(14, 5, 'system', 'c_4', 3, '添加图片1张 系统分配 5 小区币 操作人:system', 1389857071),
(15, 0, 'system', 'c_7', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389858235),
(16, 5, 'system', 'c_7', 3, '添加图片1张 系统分配 5 小区币 操作人:system', 1389858235),
(17, 50, 'system', 'u_10', 3, '注册完成，系统返回50 小区币 给 dlcmbc  操作人:system', 1389859461),
(18, 10, 'login', 'u_7', 3, '签到成功，系统返回10 小区币 给 zhangdujian 操作人:system', 1389861335),
(19, 0, 'system', 'c_8', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389862080),
(20, 15, 'system', 'c_8', 3, '添加图片3张 系统分配 15 小区币 操作人:system', 1389862080),
(21, 10, 'login', 'u_10', 3, '签到成功，系统返回10 小区币 给 dlcmbc 操作人:system', 1389862116),
(22, 50, 'system', 'u_13', 3, '注册完成，系统返回50 小区币 给 nanmuru  操作人:system', 1389863093),
(23, 10, 'login', 'u_13', 3, '签到成功，系统返回10 小区币 给 nanmuru 操作人:system', 1389863493),
(24, 5, 'cat_log', 'c_8', 3, '发布日志，系统返回5 小区币 操作人:dlcmbc', 1389864056),
(25, 50, 'system', 'u_14', 3, '注册完成，系统返回50 小区币 给 lixiaoxu  操作人:system', 1389864852),
(26, 0, 'system', 'c_9', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389866274),
(27, 10, 'system', 'c_9', 3, '添加图片2张 系统分配 10 小区币 操作人:system', 1389866274),
(28, 0, 'system', 'c_9', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389866311),
(29, 10, 'system', 'c_9', 3, '添加图片2张 系统分配 10 小区币 操作人:system', 1389866311),
(30, 50, 'system', 'u_15', 3, '注册完成，系统返回50 小区币 给 cmbc0906  操作人:system', 1389928148),
(31, 10, 'login', 'u_1', 3, '签到成功，系统返回10 小区币 给 liuchao 操作人:system', 1389934840),
(32, 0, 'system', 'c_10', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389940584),
(33, 5, 'system', 'c_10', 3, '添加图片1张 系统分配 5 小区币 操作人:system', 1389940584),
(34, 0, 'system', 'c_11', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389945141),
(35, 5, 'system', 'c_11', 3, '添加图片1张 系统分配 5 小区币 操作人:system', 1389945141),
(36, 5, 'cat_log', 'c_11', 3, '发布日志，系统返回5 小区币 操作人:fanyifei', 1389945828),
(37, 50, 'system', 'u_18', 3, '注册完成，系统返回50 小区币 给 mashuai1  操作人:system', 1389946942),
(38, 0, 'system', 'c_12', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1389947334),
(39, 20, 'system', 'c_12', 3, '添加图片4张 系统分配 20 小区币 操作人:system', 1389947334),
(40, 10, 'login', 'u_18', 3, '签到成功，系统返回10 小区币 给 mashuai1 操作人:system', 1389947345),
(41, 5, 'cat_log', 'c_12', 3, '发布日志，系统返回5 小区币 操作人:fanyifei', 1389949037),
(42, 10, 'login', 'u_13', 3, '签到成功，系统返回10 小区币 给 nanmuru 操作人:system', 1389950021),
(43, 10, 'login', 'u_5', 3, '签到成功，系统返回10 小区币 给 fanyifei 操作人:system', 1389952968),
(44, 50, 'system', 'u_11', 3, '注册完成，系统返回50 小区币 给 wenxingyuan  操作人:system', 1389963544),
(45, 10, 'login', 'u_11', 3, '签到成功，系统返回10 小区币 给 wenxingyuan 操作人:system', 1389964870),
(46, 10, 'login', 'u_10', 3, '签到成功，系统返回10 小区币 给 dlcmbc 操作人:system', 1390097827),
(47, 10, 'login', 'u_13', 3, '签到成功，系统返回10 小区币 给 nanmuru 操作人:system', 1390180773),
(48, 10, 'login', 'u_15', 3, '签到成功，系统返回10 小区币 给 cmbc0906 操作人:system', 1390189474),
(49, 10, 'login', 'u_10', 3, '签到成功，系统返回10 小区币 给 dlcmbc 操作人:system', 1390190630),
(50, 5, 'cat_log', 'c_7', 3, '发布日志，系统返回5 小区币 操作人:zhangwei', 1390216860),
(51, 0, 'system', 'c_15', 3, '添加用户0个 系统分配 0 小区币 操作人:system', 1390362495),
(52, 15, 'system', 'c_15', 3, '添加图片3张 系统分配 15 小区币 操作人:system', 1390362495);

-- --------------------------------------------------------

--
-- 表的结构 `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `goods_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `goods_sn` varchar(60) NOT NULL DEFAULT '',
  `goods_name` varchar(220) NOT NULL DEFAULT '',
  `style_name` varchar(220) NOT NULL DEFAULT '',
  `relates` varchar(220) NOT NULL,
  `click_count` int(10) unsigned NOT NULL DEFAULT '0',
  `download` int(10) NOT NULL,
  `haoping` int(10) unsigned NOT NULL DEFAULT '0',
  `goods_number` smallint(5) unsigned NOT NULL DEFAULT '0',
  `goods_weight` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
  `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `shop_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `goods_brief` text NOT NULL,
  `goods_desc` text NOT NULL,
  `descs` text NOT NULL,
  `goods_thumb` varchar(255) NOT NULL DEFAULT '',
  `original_img` varchar(255) NOT NULL DEFAULT '',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `sort_order` smallint(4) unsigned NOT NULL DEFAULT '100',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`goods_id`),
  KEY `goods_sn` (`goods_sn`),
  KEY `cat_id` (`cat_id`),
  KEY `brand_id` (`haoping`),
  KEY `goods_weight` (`goods_weight`),
  KEY `goods_number` (`goods_number`),
  KEY `sort_order` (`sort_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `goods_attr`
--

CREATE TABLE IF NOT EXISTS `goods_attr` (
  `goods_attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attr_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `attr_value` varchar(255) NOT NULL,
  `attr_price` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`goods_attr_id`),
  KEY `goods_id` (`goods_id`),
  KEY `attr_id` (`attr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `goods_gallery`
--

CREATE TABLE IF NOT EXISTS `goods_gallery` (
  `img_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `img_url` varchar(255) NOT NULL DEFAULT '',
  `img_desc` varchar(255) NOT NULL DEFAULT '',
  `thumb_url` varchar(255) NOT NULL DEFAULT '',
  `img_original` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`img_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `goods_package`
--

CREATE TABLE IF NOT EXISTS `goods_package` (
  `p_id` int(10) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(255) NOT NULL,
  `p_desc` text NOT NULL,
  `p_ids` varchar(255) NOT NULL COMMENT '商品ids',
  `f_id` int(4) NOT NULL COMMENT '父id',
  `price` float NOT NULL,
  `shipping_fee` float NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-显示，0-关闭',
  PRIMARY KEY (`p_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `keywords`
--

CREATE TABLE IF NOT EXISTS `keywords` (
  `k_kd` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  `engine` varchar(20) NOT NULL,
  `times` int(10) NOT NULL,
  PRIMARY KEY (`k_kd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `mail_templates`
--

CREATE TABLE IF NOT EXISTS `mail_templates` (
  `t_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL DEFAULT '',
  `is_html` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(200) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  PRIMARY KEY (`t_id`),
  UNIQUE KEY `template_code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- 转存表中的数据 `mail_templates`
--

INSERT INTO `mail_templates` (`t_id`, `code`, `is_html`, `subject`, `content`) VALUES
(19, 'order_confirm', 0, 'Order confirmation', '<p>Hi, %s:</p><p>You have to pay successful.We will be in a working days for your shipment , The detailed information about your order is below.<br />This order Numbers：%s.</p><p>Subtotal : %s<br />Shipping Fee :%s<br />Dicount : %s<br />Grand Total : %s</p><p>Consignee information: %s</p><p>More information:%s</p><p>HTLP ? mail to: customer8service@126.com</p>'),
(17, 'reset_passwd', 0, 'Retrieve password', '<p>你好, %s!<br /><br />您的密码已经修改:<br /><br />系统为您创建的临时密码为:[%s] 请登录后及时修改!<br /><br />该邮件为系统发送，请勿回复!<br /><br />From:%s<br />Date:%s</p>');

-- --------------------------------------------------------

--
-- 表的结构 `order_goods`
--

CREATE TABLE IF NOT EXISTS `order_goods` (
  `rec_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `goods_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `goods_name` varchar(120) NOT NULL DEFAULT '',
  `goods_sn` varchar(60) NOT NULL DEFAULT '',
  `goods_number` smallint(5) unsigned NOT NULL DEFAULT '1',
  `market_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `goods_attr` text NOT NULL,
  `is_gift` smallint(5) unsigned NOT NULL DEFAULT '0',
  `goods_attr_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`rec_id`),
  KEY `order_id` (`order_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `order_info`
--

CREATE TABLE IF NOT EXISTS `order_info` (
  `order_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(20) NOT NULL DEFAULT '',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `shipping_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pay_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `review_id` int(10) NOT NULL,
  `address` text NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `shipping_id` tinyint(3) NOT NULL DEFAULT '0',
  `shipping_name` varchar(120) NOT NULL DEFAULT '',
  `pay_id` tinyint(3) NOT NULL DEFAULT '0',
  `pay_name` varchar(120) NOT NULL DEFAULT '',
  `goods_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `shipping_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `order_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `confirm_time` int(10) unsigned NOT NULL DEFAULT '0',
  `shipping_time` int(10) unsigned NOT NULL DEFAULT '0',
  `to_buyer` varchar(255) NOT NULL DEFAULT '',
  `discount` decimal(10,2) NOT NULL,
  `pay_account` varchar(255) DEFAULT NULL COMMENT '付款账号',
  `ip` varchar(15) NOT NULL,
  `os` varchar(25) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `shipping_status` (`shipping_status`),
  KEY `pay_status` (`pay_status`),
  KEY `shipping_id` (`shipping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `pay_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(20) NOT NULL,
  `config` text NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`pay_id`),
  KEY `pay_code` (`pay_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `payment`
--

INSERT INTO `payment` (`pay_id`, `pay_code`, `config`, `status`) VALUES
(1, 'paypal', '{"pay_name":"Paypal","pay_desc":"You will be redirected to PayPal website when you place an order.","pay_code":"paypal","account":"service8customer@gmail.com","currency":"USD"}', 1),
(2, 'paypal', '{"pay_name":"Paypal","pay_desc":"You will be redirected to PayPal website when you place an order.","pay_code":"paypal","account":"service8customer@gmail.com","currency":"USD"}', 1);

-- --------------------------------------------------------

--
-- 表的结构 `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `region_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `region_name` varchar(120) NOT NULL DEFAULT '',
  `region_type` tinyint(1) NOT NULL DEFAULT '0',
  `code` varchar(5) NOT NULL,
  PRIMARY KEY (`region_id`),
  KEY `parent_id` (`parent_id`),
  KEY `region_type` (`region_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=313 ;

--
-- 转存表中的数据 `region`
--

INSERT INTO `region` (`region_id`, `parent_id`, `region_name`, `region_type`, `code`) VALUES
(1, 0, 'United States', 0, 'US'),
(2, 0, 'Afghanistan', 0, 'AF'),
(3, 0, 'Albania', 0, 'AL'),
(4, 0, 'Algeria', 0, 'DZ'),
(5, 0, 'American Samoa', 0, 'AS'),
(6, 0, 'Andorra', 0, 'AD'),
(7, 0, 'Angola', 0, 'AO'),
(8, 0, 'Anguilla', 0, 'AI'),
(9, 0, 'Antarctica', 0, 'AQ'),
(10, 0, 'Antigua and Barbuda', 0, 'AG'),
(11, 0, 'Argentina', 0, 'AR'),
(12, 0, 'Armenia', 0, 'AM'),
(13, 0, 'Aruba', 0, 'AW'),
(14, 0, 'Australia', 0, 'AU'),
(15, 0, 'Austria', 0, 'AT'),
(16, 0, 'Azerbaijan', 0, 'AZ'),
(17, 0, 'Bahamas', 0, 'BS'),
(18, 0, 'Bahrain', 0, 'BH'),
(19, 0, 'Bangladesh', 0, 'BD'),
(20, 0, 'Barbados', 0, 'BB'),
(21, 0, 'Belarus', 0, 'BY'),
(22, 0, 'Belgium', 0, 'BE'),
(23, 0, 'Belize', 0, 'BZ'),
(24, 0, 'Benin', 0, 'BJ'),
(25, 0, 'Bermuda', 0, 'BM'),
(26, 0, 'Bhutan', 0, 'BT'),
(27, 0, 'Bolivia', 0, 'BO'),
(28, 0, 'Bosnia and Herzegovina', 0, 'BA'),
(29, 0, 'Botswana', 0, 'BW'),
(30, 0, 'Bouvet Island', 0, 'BV'),
(31, 0, 'Brazil', 0, 'BR'),
(32, 0, 'British Indian Ocean Territory', 0, 'IO'),
(33, 0, 'British Virgin Islands', 0, 'VG'),
(34, 0, 'Brunei', 0, 'BN'),
(35, 0, 'Bulgaria', 0, 'BG'),
(36, 0, 'Burkina Faso', 0, 'BF'),
(37, 0, 'Burundi', 0, 'BI'),
(38, 0, 'Cambodia', 0, 'KH'),
(39, 0, 'Cameroon', 0, 'CM'),
(40, 0, 'Canada', 0, 'CA'),
(41, 0, 'Cape Verde', 0, 'CV'),
(42, 0, 'Cayman Islands', 0, 'KY'),
(43, 0, 'Central African Republic', 0, 'CF'),
(44, 0, 'Chad', 0, 'TD'),
(45, 0, 'Chile', 0, 'CL'),
(46, 0, 'China', 0, 'CN'),
(47, 0, 'Christmas Island', 0, 'CX'),
(48, 0, 'Cocos [Keeling] Islands', 0, 'CC'),
(49, 0, 'Colombia', 0, 'CO'),
(50, 0, 'Comoros', 0, 'KM'),
(51, 0, 'Congo - Brazzaville', 0, 'CG'),
(52, 0, 'Congo - Kinshasa', 0, 'CD'),
(53, 0, 'Cook Islands', 0, 'CK'),
(54, 0, 'Costa Rica', 0, 'CR'),
(55, 0, 'Croatia', 0, 'HR'),
(56, 0, 'Cuba', 0, 'CU'),
(57, 0, 'Cyprus', 0, 'CY'),
(58, 0, 'Czech Republic', 0, 'CZ'),
(59, 0, 'Côte d''Ivoire', 0, 'CI'),
(60, 0, 'Denmark', 0, 'DK'),
(61, 0, 'Djibouti', 0, 'DJ'),
(62, 0, 'Dominica', 0, 'DM'),
(63, 0, 'Dominican Republic', 0, 'DO'),
(64, 0, 'Ecuador', 0, 'EC'),
(65, 0, 'Egypt', 0, 'EG'),
(66, 0, 'El Salvador', 0, 'SV'),
(67, 0, 'Equatorial Guinea', 0, 'GQ'),
(68, 0, 'Eritrea', 0, 'ER'),
(69, 0, 'Estonia', 0, 'EE'),
(70, 0, 'Ethiopia', 0, 'ET'),
(71, 0, 'Falkland Islands', 0, 'FK'),
(72, 0, 'Faroe Islands', 0, 'FO'),
(73, 0, 'Fiji', 0, 'FJ'),
(74, 0, 'Finland', 0, 'FI'),
(75, 0, 'France', 0, 'FR'),
(76, 0, 'French Guiana', 0, 'GF'),
(77, 0, 'French Polynesia', 0, 'PF'),
(78, 0, 'French Southern Territories', 0, 'TF'),
(79, 0, 'Gabon', 0, 'GA'),
(80, 0, 'Gambia', 0, 'GM'),
(81, 0, 'Georgia', 0, 'GE'),
(82, 0, 'Germany', 0, 'DE'),
(83, 0, 'Ghana', 0, 'GH'),
(84, 0, 'Gibraltar', 0, 'GI'),
(85, 0, 'Greece', 0, 'GR'),
(86, 0, 'Greenland', 0, 'GL'),
(87, 0, 'Grenada', 0, 'GD'),
(88, 0, 'Guadeloupe', 0, 'GP'),
(89, 0, 'Guam', 0, 'GU'),
(90, 0, 'Guatemala', 0, 'GT'),
(91, 0, 'Guernsey', 0, 'GG'),
(92, 0, 'Guinea', 0, 'GN'),
(93, 0, 'Guinea-Bissau', 0, 'GW'),
(94, 0, 'Guyana', 0, 'GY'),
(95, 0, 'Haiti', 0, 'HT'),
(96, 0, 'Heard Island and McDonald Islands', 0, 'HM'),
(97, 0, 'Honduras', 0, 'HN'),
(98, 0, 'Hong Kong SAR China', 0, 'HK'),
(99, 0, 'Hungary', 0, 'HU'),
(100, 0, 'Iceland', 0, 'IS'),
(101, 0, 'India', 0, 'IN'),
(102, 0, 'Indonesia', 0, 'ID'),
(103, 0, 'Iran', 0, 'IR'),
(104, 0, 'Iraq', 0, 'IQ'),
(105, 0, 'Ireland', 0, 'IE'),
(106, 0, 'Isle of Man', 0, 'IM'),
(107, 0, 'Israel', 0, 'IL'),
(108, 0, 'Italy', 0, 'IT'),
(109, 0, 'Jamaica', 0, 'JM'),
(110, 0, 'Japan', 0, 'JP'),
(111, 0, 'Jersey', 0, 'JE'),
(112, 0, 'Jordan', 0, 'JO'),
(113, 0, 'Kazakhstan', 0, 'KZ'),
(114, 0, 'Kenya', 0, 'KE'),
(115, 0, 'Kiribati', 0, 'KI'),
(116, 0, 'Kuwait', 0, 'KW'),
(117, 0, 'Kyrgyzstan', 0, 'KG'),
(118, 0, 'Laos', 0, 'LA'),
(119, 0, 'Latvia', 0, 'LV'),
(120, 0, 'Lebanon', 0, 'LB'),
(121, 0, 'Lesotho', 0, 'LS'),
(122, 0, 'Liberia', 0, 'LR'),
(123, 0, 'Libya', 0, 'LY'),
(124, 0, 'Liechtenstein', 0, 'LI'),
(125, 0, 'Lithuania', 0, 'LT'),
(126, 0, 'Luxembourg', 0, 'LU'),
(127, 0, 'Macau SAR China', 0, 'MO'),
(128, 0, 'Macedonia', 0, 'MK'),
(129, 0, 'Madagascar', 0, 'MG'),
(130, 0, 'Malawi', 0, 'MW'),
(131, 0, 'Malaysia', 0, 'MY'),
(132, 0, 'Maldives', 0, 'MV'),
(133, 0, 'Mali', 0, 'ML'),
(134, 0, 'Malta', 0, 'MT'),
(135, 0, 'Marshall Islands', 0, 'MH'),
(136, 0, 'Martinique', 0, 'MQ'),
(137, 0, 'Mauritania', 0, 'MR'),
(138, 0, 'Mauritius', 0, 'MU'),
(139, 0, 'Mayotte', 0, 'YT'),
(140, 0, 'Mexico', 0, 'MX'),
(141, 0, 'Micronesia', 0, 'FM'),
(142, 0, 'Moldova', 0, 'MD'),
(143, 0, 'Monaco', 0, 'MC'),
(144, 0, 'Mongolia', 0, 'MN'),
(145, 0, 'Montenegro', 0, 'ME'),
(146, 0, 'Montserrat', 0, 'MS'),
(147, 0, 'Morocco', 0, 'MA'),
(148, 0, 'Mozambique', 0, 'MZ'),
(149, 0, 'Myanmar [Burma]', 0, 'MM'),
(150, 0, 'Namibia', 0, 'NA'),
(151, 0, 'Nauru', 0, 'NR'),
(152, 0, 'Nepal', 0, 'NP'),
(153, 0, 'Netherlands', 0, 'NL'),
(154, 0, 'Netherlands Antilles', 0, 'AN'),
(155, 0, 'New Caledonia', 0, 'NC'),
(156, 0, 'New Zealand', 0, 'NZ'),
(157, 0, 'Nicaragua', 0, 'NI'),
(158, 0, 'Niger', 0, 'NE'),
(159, 0, 'Nigeria', 0, 'NG'),
(160, 0, 'Niue', 0, 'NU'),
(161, 0, 'Norfolk Island', 0, 'NF'),
(162, 0, 'North Korea', 0, 'KP'),
(163, 0, 'Northern Mariana Islands', 0, 'MP'),
(164, 0, 'Norway', 0, 'NO'),
(165, 0, 'Oman', 0, 'OM'),
(166, 0, 'Pakistan', 0, 'PK'),
(167, 0, 'Palau', 0, 'PW'),
(168, 0, 'Palestinian Territories', 0, 'PS'),
(169, 0, 'Panama', 0, 'PA'),
(170, 0, 'Papua New Guinea', 0, 'PG'),
(171, 0, 'Paraguay', 0, 'PY'),
(172, 0, 'Peru', 0, 'PE'),
(173, 0, 'Philippines', 0, 'PH'),
(174, 0, 'Pitcairn Islands', 0, 'PN'),
(175, 0, 'Poland', 0, 'PL'),
(176, 0, 'Portugal', 0, 'PT'),
(177, 0, 'Puerto Rico', 0, 'PR'),
(178, 0, 'Qatar', 0, 'QA'),
(179, 0, 'Romania', 0, 'RO'),
(180, 0, 'Russia', 0, 'RU'),
(181, 0, 'Rwanda', 0, 'RW'),
(182, 0, 'Réunion', 0, 'RE'),
(183, 0, 'Saint Barthélemy', 0, 'BL'),
(184, 0, 'Saint Helena', 0, 'SH'),
(185, 0, 'Saint Kitts and Nevis', 0, 'KN'),
(186, 0, 'Saint Lucia', 0, 'LC'),
(187, 0, 'Saint Martin', 0, 'MF'),
(188, 0, 'Saint Pierre and Miquelon', 0, 'PM'),
(189, 0, 'Saint Vincent and the Grenadines', 0, 'VC'),
(190, 0, 'Samoa', 0, 'WS'),
(191, 0, 'San Marino', 0, 'SM'),
(192, 0, 'Saudi Arabia', 0, 'SA'),
(193, 0, 'Senegal', 0, 'SN'),
(194, 0, 'Serbia', 0, 'RS'),
(195, 0, 'Seychelles', 0, 'SC'),
(196, 0, 'Sierra Leone', 0, 'SL'),
(197, 0, 'Singapore', 0, 'SG'),
(198, 0, 'Slovakia', 0, 'SK'),
(199, 0, 'Slovenia', 0, 'SI'),
(200, 0, 'Solomon Islands', 0, 'SB'),
(201, 0, 'Somalia', 0, 'SO'),
(202, 0, 'South Africa', 0, 'ZA'),
(203, 0, 'South Georgia and the South Sandwich Islands', 0, 'GS'),
(204, 0, 'South Korea', 0, 'KR'),
(205, 0, 'Spain', 0, 'ES'),
(206, 0, 'Sri Lanka', 0, 'LK'),
(207, 0, 'Sudan', 0, 'SD'),
(208, 0, 'Suriname', 0, 'SR'),
(209, 0, 'Svalbard and Jan Mayen', 0, 'SJ'),
(210, 0, 'Swaziland', 0, 'SZ'),
(211, 0, 'Sweden', 0, 'SE'),
(212, 0, 'Switzerland', 0, 'CH'),
(213, 0, 'Syria', 0, 'SY'),
(214, 0, 'São Tomé and Príncipe', 0, 'ST'),
(215, 0, 'Taiwan', 0, 'TW'),
(216, 0, 'Tajikistan', 0, 'TJ'),
(217, 0, 'Tanzania', 0, 'TZ'),
(218, 0, 'Thailand', 0, 'TH'),
(219, 0, 'Timor-Leste', 0, 'TL'),
(220, 0, 'Togo', 0, 'TG'),
(221, 0, 'Tokelau', 0, 'TK'),
(222, 0, 'Tonga', 0, 'TO'),
(223, 0, 'Trinidad and Tobago', 0, 'TT'),
(224, 0, 'Tunisia', 0, 'TN'),
(225, 0, 'Turkey', 0, 'TR'),
(226, 0, 'Turkmenistan', 0, 'TM'),
(227, 0, 'Turks and Caicos Islands', 0, 'TC'),
(228, 0, 'Tuvalu', 0, 'TV'),
(229, 0, 'U.S. Minor Outlying Islands', 0, 'UM'),
(230, 0, 'U.S. Virgin Islands', 0, 'VI'),
(231, 0, 'Uganda', 0, 'UG'),
(232, 0, 'Ukraine', 0, 'UA'),
(233, 0, 'United Arab Emirates', 0, 'AE'),
(234, 0, 'United Kingdom', 0, 'GB'),
(235, 0, 'Uruguay', 0, 'UY'),
(236, 0, 'Uzbekistan', 0, 'UZ'),
(237, 0, 'Vanuatu', 0, 'VU'),
(238, 0, 'Vatican City', 0, 'VA'),
(239, 0, 'Venezuela', 0, 'VE'),
(240, 0, 'Vietnam', 0, 'VN'),
(241, 0, 'Wallis and Futuna', 0, 'WF'),
(242, 0, 'Western Sahara', 0, 'EH'),
(243, 0, 'Yemen', 0, 'YE'),
(244, 0, 'Zambia', 0, 'ZM'),
(245, 0, 'Zimbabwe', 0, 'ZW'),
(246, 0, 'Åland Islands', 0, 'AX'),
(247, 0, 'Other Country', 0, ''),
(248, 1, 'Alabama', 0, ''),
(249, 1, 'Alaska', 0, ''),
(250, 1, 'American Samoa', 0, ''),
(251, 1, 'Arizona', 0, ''),
(252, 1, 'Arkansas', 0, ''),
(253, 1, 'Armed Forces Africa', 0, ''),
(254, 1, 'Armed Forces Americas', 0, ''),
(255, 1, 'Armed Forces Canada', 0, ''),
(256, 1, 'Armed Forces Europe', 0, ''),
(257, 1, 'Armed Forces Middle East', 0, ''),
(258, 1, 'Armed Forces Pacific', 0, ''),
(259, 1, 'California', 0, ''),
(260, 1, 'Colorado', 0, ''),
(261, 1, 'Connecticut', 0, ''),
(262, 1, 'Delaware', 0, ''),
(263, 1, 'District of Columbia', 0, ''),
(264, 1, 'Federated States Of Micronesia', 0, ''),
(265, 1, 'Florida', 0, ''),
(266, 1, 'Georgia', 0, ''),
(267, 1, 'Guam', 0, ''),
(268, 1, 'Hawaii', 0, ''),
(269, 1, 'Idaho', 0, ''),
(270, 1, 'Illinois', 0, ''),
(271, 1, 'Indiana', 0, ''),
(272, 1, 'Iowa', 0, ''),
(273, 1, 'Kansas', 0, ''),
(274, 1, 'Kentucky', 0, ''),
(275, 1, 'Louisiana', 0, ''),
(276, 1, 'Maine', 0, ''),
(277, 1, 'Marshall Islands', 0, ''),
(278, 1, 'Maryland', 0, ''),
(279, 1, 'Massachusetts', 0, ''),
(280, 1, 'Michigan', 0, ''),
(281, 1, 'Minnesota', 0, ''),
(282, 1, 'Mississippi', 0, ''),
(283, 1, 'Missouri', 0, ''),
(284, 1, 'Montana', 0, ''),
(285, 1, 'Nebraska', 0, ''),
(286, 1, 'Nevada', 0, ''),
(287, 1, 'New Hampshire', 0, ''),
(288, 1, 'New Jersey', 0, ''),
(289, 1, 'New Mexico', 0, ''),
(290, 1, 'New York', 0, ''),
(291, 1, 'North Carolina', 0, ''),
(292, 1, 'North Dakota', 0, ''),
(293, 1, 'Northern Mariana Islands', 0, ''),
(294, 1, 'Ohio', 0, ''),
(295, 1, 'Oklahoma', 0, ''),
(296, 1, 'Oregon', 0, ''),
(297, 1, 'Palau', 0, ''),
(298, 1, 'Pennsylvania', 0, ''),
(299, 1, 'Puerto Rico', 0, ''),
(300, 1, 'Rhode Island', 0, ''),
(301, 1, 'South Carolina', 0, ''),
(302, 1, 'South Dakota', 0, ''),
(303, 1, 'Tennessee', 0, ''),
(304, 1, 'Texas', 0, ''),
(305, 1, 'Utah', 0, ''),
(306, 1, 'Vermont', 0, ''),
(307, 1, 'Virgin Islands', 0, ''),
(308, 1, 'Virginia', 0, ''),
(309, 1, 'Washington', 0, ''),
(310, 1, 'West Virginia', 0, ''),
(311, 1, 'Wisconsin', 0, ''),
(312, 1, 'Wyoming', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `r_id` int(10) NOT NULL AUTO_INCREMENT,
  `rp_id` int(10) NOT NULL COMMENT '回复id',
  `user_id` int(10) NOT NULL,
  `user_name` varchar(60) NOT NULL,
  `id` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0-留言，1-店铺，2-商品，3-订单',
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-未审核，2--已审核',
  `add_time` int(10) NOT NULL,
  `content` varchar(500) NOT NULL,
  `score` tinyint(1) NOT NULL,
  `question` varchar(100) NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `review`
--

INSERT INTO `review` (`r_id`, `rp_id`, `user_id`, `user_name`, `id`, `type`, `status`, `add_time`, `content`, `score`, `question`) VALUES
(1, 0, 0, '', 3, 1, 2, 1389855363, '厉害呀', 0, ''),
(2, 0, 7, '', 3, 1, 2, 1389855406, '美女店长', 0, ''),
(3, 0, 0, '', 7, 1, 2, 1389858635, '有人吗', 0, ''),
(4, 0, 7, '', 1, 1, 2, 1389861183, '多传一点图片呗', 0, ''),
(5, 0, 7, '', 2, 1, 2, 1389861195, '多传一点图片呗', 0, ''),
(6, 0, 10, '', 9, 1, 2, 1389866704, '恭祝御庭阁胜利开店~~~', 0, ''),
(7, 0, 14, '', 8, 1, 2, 1389867200, '开门大吉~~~', 0, ''),
(8, 0, 0, 'aaa', 0, 0, 2, 1389946523, 'fsda', 0, ''),
(9, 0, 0, '', 12, 1, 2, 1389949080, '很温馨！~', 0, ''),
(10, 0, 0, '', 0, 0, 2, 1389950741, 'fdsa', 0, ''),
(11, 0, 0, '', 0, 0, 2, 1389954019, 'sdfsdfs', 0, ''),
(12, 0, 0, '', 0, 0, 2, 1389954507, 'sadfsdfsadfsdfsdfsdf111', 0, 'on'),
(13, 0, 0, '', 0, 0, 2, 1389954624, '11111', 0, '界面不够美观'),
(14, 0, 0, '', 7, 1, 2, 1390189122, '怎么木有产品哦？', 0, '0'),
(15, 0, 0, '', 0, 0, 2, 1390293073, '修改小店还有紊乱的情况', 0, '界面不够美观'),
(16, 0, 5, '', 0, 0, 2, 1390293123, '修改小店界面是别人的店子。', 0, '界面不够美观'),
(17, 0, 5, '', 0, 0, 2, 1390293389, '建议网站设立下载中心，把浏览器等软件挂上，因为有的小区店电脑比较滞后，不能完全看到完整界面。', 0, '界面不够美观'),
(18, 0, 5, '', 0, 0, 2, 1390293454, '点击逛逛小店的时候，右边没有按时间、按热度这些选项了，建议还是添加。', 0, '界面不够美观');

-- --------------------------------------------------------

--
-- 表的结构 `shipping`
--

CREATE TABLE IF NOT EXISTS `shipping` (
  `shipping_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `config` text NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`shipping_id`),
  KEY `shipping_code` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `shipping`
--

INSERT INTO `shipping` (`shipping_id`, `config`, `status`) VALUES
(1, '{"name":"Standard Shipping","desc":"7 to 10 Business Days","base_fee":"0","free_money":"0"}', 1),
(2, '{"name":"Expedited Shipping","desc":"3 to 5 Business Days","base_fee":"19.9","free_money":"0"}', 1);

-- --------------------------------------------------------

--
-- 表的结构 `typeids`
--

CREATE TABLE IF NOT EXISTS `typeids` (
  `type_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `p_id` int(10) NOT NULL DEFAULT '0' COMMENT '父id',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- 转存表中的数据 `typeids`
--

INSERT INTO `typeids` (`type_id`, `name`, `p_id`) VALUES
(1, '总行部室', 0),
(2, '地产金融事业部', 0),
(3, '能源金融事业部', 0),
(4, '交通金融事业部', 0),
(5, '冶金金融事业部', 0),
(6, '贸易金融部', 0),
(7, '信用卡中心', 0),
(8, '现代农业金融事业部', 0),
(9, '文化产业金融事业部', 0),
(10, '石材产业金融事业部', 0),
(11, '电子银行部', 0),
(12, '金融市场部', 0),
(13, '北京管理部', 0),
(14, '广州分行', 0),
(15, '上海分行', 0),
(16, '深圳分行', 0),
(17, '武汉分行', 0),
(18, '太原分行', 0),
(19, '石家庄分行', 0),
(20, '大连分行', 0),
(21, '杭州分行', 0),
(22, '南京分行', 0),
(23, '重庆分行', 0),
(24, '西安分行', 0),
(25, '福州分行', 0),
(26, '济南分行', 0),
(27, '宁波分行', 0),
(28, '成都分行', 0),
(29, '天津分行', 0),
(30, '昆明分行', 0),
(31, '泉州分行', 0),
(32, '苏州分行', 0),
(33, '汕头分行', 0),
(34, '青岛分行', 0),
(35, '温州分行', 0),
(36, '厦门分行', 0),
(37, '郑州分行', 0),
(38, '长沙分行', 0),
(39, '长春分行', 0),
(40, '合肥分行', 0),
(41, '南昌分行', 0),
(42, '南宁分行', 0),
(43, '呼和浩特分行', 0),
(44, '沈阳分行', 0),
(45, '香港分行', 0),
(46, '贵阳分行', 0),
(47, '三亚分行', 0),
(48, '拉萨分行', 0),
(49, '其他', 0);

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `type_id` int(3) NOT NULL COMMENT '机构',
  `email` varchar(60) NOT NULL DEFAULT '',
  `passwd` varchar(32) NOT NULL,
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1-男，2-女',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `avater` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `cat_id` int(10) NOT NULL COMMENT '隶属的cat_id',
  `mag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '管理权：1-管理员,0-普通用户',
  `gold` int(10) NOT NULL COMMENT '金币',
  `valid_pic` varchar(255) NOT NULL,
  `last_login` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1--已审核，0--未审核',
  PRIMARY KEY (`user_id`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `type_id`, `email`, `passwd`, `sex`, `birthday`, `avater`, `info`, `cat_id`, `mag`, `gold`, `valid_pic`, `last_login`, `status`) VALUES
(1, 'liuchao', 1, 'liuchao@cmbc.com.cn', '0bbc6ed46abe7ca2b60696186fbf2ac7', 1, '1981-05-27', 'uploads/20140110/8896e89c4c.jpg', '{\\"name\\":\\"\\\\u5218\\\\u8d85\\",\\"nick_name\\":\\"\\\\u5218\\\\u8d85\\",\\"tel\\":\\"13901013756\\",\\"weixin\\":\\"\\"}', 0, 0, 60, 'uploads/20140110/39171808d2.jpg', 1389934838, 1),
(2, 'airzhangdong', 1, 'zhangdong4@cmbc.com.cn', '4b6247efdf9b7c57f99b8d8240392f7a', 1, '1983-11-08', 'uploads/20140110/e0d9ad40da.jpg', '{\\"name\\":\\"\\\\u5f20\\\\u51ac\\",\\"nick_name\\":\\"airzd\\",\\"tel\\":\\"13810101521\\",\\"weixin\\":\\"\\"}', 2, 1, 60, 'uploads/20140110/295b7a2f35.jpg', 1389849670, 1),
(3, 'zhangdong4', 49, 'airzhangdong@hotmail.com', '4b6247efdf9b7c57f99b8d8240392f7a', 1, '2014-01-23', 'uploads/20140110/8b0b38ec8f.jpg', '{\\"name\\":\\"\\\\u5f20\\\\u4e8c\\\\u51ac\\",\\"nick_name\\":\\"\\\\u51ac\\\\u5b50\\",\\"tel\\":\\"13810101521\\",\\"weixin\\":\\"airzd\\"}', 15, 1, 60, 'uploads/20140110/0ca984f7fc.jpg', 1390360558, 1),
(4, 'airzd', 23, 'airzhangdong@googlemail.com', '4b6247efdf9b7c57f99b8d8240392f7a', 2, '0000-00-00', 'uploads/20140110/816683fea3.jpg', '{\\"name\\":\\"\\\\u51ac\\\\u51ac\\",\\"nick_name\\":\\"\\\\u8001\\\\u4e09\\",\\"tel\\":\\"13813131818\\",\\"weixin\\":\\"\\"}', 2, 1, 44, 'uploads/20140110/df94be4206.jpg', 0, 1),
(5, 'fanyifei', 18, 'fanyifei@cmbc.com.cn', '405e8a9472142c344e434f0db9e0061e', 2, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u8303\\\\u6bc5\\\\u98de\\",\\"nick_name\\":\\"\\\\u6696\\\\u6696\\\\u4ea6\\\\u98de\\",\\"tel\\":\\"18636149937\\",\\"weixin\\":\\"\\"}', 7, 1, 70, 'uploads/20140116/40e6aff743.jpg', 1390293097, 1),
(6, 'gaoqian', 18, 'jiaopan@cmbc.com.cn', '0cce886e39102b7d33b5a7337a166776', 1, '1991-09-25', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u9ad8\\\\u4e7e\\",\\"nick_name\\":\\"\\\\u9ad8\\\\u4e7e\\",\\"tel\\":\\"18035179288\\",\\"weixin\\":\\"576100643\\"}', 2, 1, 60, 'uploads/20140116/e4ea247ca3.jpg', 1390185936, 1),
(7, 'zhangdujian', 1, 'zhangdujian@cmbc.com.cn', '0b209fb0ba4df7bafaddb433736ea98a', 1, '1985-08-14', 'uploads/20140116/8561cb913a.jpg', '{\\"name\\":\\"\\\\u5f20\\\\u675c\\\\u6e10\\",\\"nick_name\\":\\"StarGzaer\\",\\"tel\\":\\"18674883329\\",\\"weixin\\":\\"zhangdujian\\"}', 10, 1, 60, 'uploads/20140116/4081694f06.png', 1390187897, 1),
(8, 'pangwenhui', 18, 'fuyuan@cmbc.com.cn', 'e793b714e02880034c71c4a76d24b558', 1, '1989-11-13', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u5e9e\\\\u6587\\\\u8f89\\",\\"nick_name\\":\\"\\\\u5e9e\\\\u6587\\\\u8f89\\",\\"tel\\":\\"13546414250\\",\\"weixin\\":\\"79808437\\"}', 0, 0, 50, 'uploads/20140116/a502e1a843.jpg', 1389848981, 1),
(9, 'zhangwei', 18, '309815056@qq.com', '5055b261efb4e949643341a72adbf6f4', 1, '1990-07-11', 'uploads/20140116/05c615532a.jpg', '{\\"name\\":\\"\\\\u738b\\\\u9e64\\",\\"nick_name\\":\\"\\\\u738b\\\\u9e64\\\\u9e64\\",\\"tel\\":\\"18647425681\\",\\"weixin\\":\\"18647425681\\"}', 7, 1, 60, 'uploads/20140116/6fefba13ee.jpg', 1390216292, 1),
(11, 'wenxingyuan', 18, 'zhaofei@cmbc.com.cn', 'b26ba131c2921ecff791063514d90b59', 0, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u674e\\\\u5349\\",\\"nick_name\\":\\"\\\\u6587\\\\u5174\\\\u82d1\\",\\"tel\\":\\"15034404502\\",\\"weixin\\":\\"\\"}', 0, 0, 60, 'uploads/20140117/e4b7e8c249.jpg', 1389963427, 1),
(10, 'dlcmbc', 20, 'duanyongji@cmbc.com.cn', '94e8cde4612b3fd390677d42e7b22002', 1, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u6bb5\\\\u6c38\\\\u5409\\",\\"nick_name\\":\\"\\\\u80a5\\\\u800c\\\\u4e0d\\\\u817b\\",\\"tel\\":\\"13889699858\\",\\"weixin\\":\\"\\"}', 8, 1, 80, 'uploads/20140116/0a2e5d9842.jpg', 1390190615, 1),
(12, 'wujin', 20, 'wujin3@cmbc.com.cn', '6bbf5de7bcace9ce3bacc1c7470c9ee9', 2, '0000-00-00', 'uploads/20140116/aae87f6cd0.jpg', '{\\"name\\":\\"\\\\u5927\\\\u8fde\\\\u4e07\\\\u8fbe\\\\u534e\\\\u5e9c\\\\u5c0f\\\\u533a\\\\u4fbf\\\\u6c11\\\\u5e97\\",\\"nick_name\\":\\"\\\\u4e07\\\\u8fbe\\\\u534e\\\\u5e9c\\\\u4fbf\\\\u6c11\\\\u5e97\\",\\"tel\\":\\"13654118634\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 1390004552, 0),
(13, 'nanmuru', 18, 'zhoubeibei@cmbc.com.cn', 'd9ce83610f37a1db416ab5e5ea906fe0', 1, '1989-04-09', 'uploads/20140118/cfc52ce8a4.jpg', '{\\"name\\":\\"\\\\u738b\\\\u65ed\\\\u5b87\\",\\"nick_name\\":\\"\\\\u5357\\\\u6728\\\\u5982\\",\\"tel\\":\\"18636180413\\",\\"weixin\\":\\"nanmuru\\"}', 11, 1, 80, 'uploads/20140116/f422cad325.jpg', 1390180723, 1),
(14, 'lixiaoxu', 20, 'lixiaoxu@cmbc.com.cn', '01280f9c176cad51f8bab6fca66b3c8f', 2, '2014-10-16', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u674e\\\\u6653\\\\u65ed\\",\\"nick_name\\":\\"\\\\u58a8\\\\u513f\\\\u672c\\\\uff0c\\\\u6674\\",\\"tel\\":\\"13322227878\\",\\"weixin\\":\\"\\"}', 9, 1, 50, 'uploads/20140116/9895360c43.jpg', 1389866940, 1),
(15, 'cmbc0906', 18, 'liuxingwang@cmbc.com.cn', '9ceb54e482db5191721875ca3dde8d37', 1, '1989-07-01', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u5218\\\\u5174\\\\u65fa\\",\\"nick_name\\":\\"\\\\u9a6c\\\\u4e0a\\\\u65fa\\\\u65fa\\",\\"tel\\":\\"15803437184\\",\\"weixin\\":\\"minmin689616\\"}', 0, 0, 60, 'uploads/20140117/ea78ffac3f.jpg', 1390188897, 1),
(16, 'tinalu', 32, '584665619@qq.com', '5899f88839c3b565996b6e5bb6f1f940', 2, '1985-02-11', 'uploads/20140117/606c75c829.jpg', '{\\"name\\":\\"\\\\u9646\\\\u5a77\\\\u5a77\\",\\"nick_name\\":\\"tina\\",\\"tel\\":\\"13771762170\\",\\"weixin\\":\\"584665619@qq.com\\"}', 0, 0, 0, '', 0, 0),
(17, 'ALLEN', 32, 'huaqi@cmbc.com.cn', 'd9faf4dfdab174786c5f6b33d32a0bed', 1, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u534e\\\\u68cb\\",\\"nick_name\\":\\"\\\\u534e\\\\u68cb\\",\\"tel\\":\\"13656242400\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 0, 0),
(18, 'mashuai1', 20, 'mashuai1@cmbc.com.cn', '4d2b2a700b5921fda8f820c933a9c92d', 1, '1987-11-13', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u9a6c\\\\u5e05\\",\\"nick_name\\":\\"\\\\u4f70\\\\u4f73\\\\u5e97\\",\\"tel\\":\\"15998591530\\",\\"weixin\\":\\"\\"}', 12, 1, 60, 'uploads/20140117/7bf0f25233.jpg', 1389946918, 1),
(19, 'wdhf', 20, 'wujin_dl@cmbc.com.cn', '6bbf5de7bcace9ce3bacc1c7470c9ee9', 0, '0000-00-00', 'uploads/20140118/22b358065c.jpg', '{\\"name\\":\\"\\\\u5927\\\\u8fde\\\\u4e07\\\\u8fbe\\\\u534e\\\\u5e9c\\\\u5c0f\\\\u533a\\\\u4fbf\\\\u6c11\\\\u5e97\\",\\"nick_name\\":\\"\\\\u534e\\\\u5e9c\\\\u6b63\\\\u80fd\\\\u91cf\\",\\"tel\\":\\"13654118634\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 0, 0),
(20, 'wenlinlin', 20, 'jiandanai1350@163.com', '986ae200978689742623743c361c093e', 2, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u6587\\\\u7433\\\\u7433\\",\\"nick_name\\":\\"\\\\u7b80\\\\u5355\\\\u7231\\",\\"tel\\":\\"15998651370\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 0, 0),
(21, 'zirui1989', 20, '985180629@qq.com', '15e9512bbd655537df8ab8361fc72396', 1, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u5510\\\\u6893\\\\u745e\\",\\"nick_name\\":\\"\\\\u6709\\\\u70b9\\\\u610f\\\\u601d\\",\\"tel\\":\\"15942839778\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 0, 0),
(22, 'wangshouyi', 20, 'wang_shouyi@hotmail.com', '96e79218965eb72c92a549dd5a330112', 2, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u738b\\\\u9996\\\\u6021\\",\\"nick_name\\":\\"\\\\u738b\\\\u72e0\\\\u4eba\\",\\"tel\\":\\"13604112872\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 0, 0),
(23, 'zhangqian5', 15, 'zhangqian5@cmbc.com.cn', '65a399d25b5b22ffc560f16aa5cd78d2', 2, '0000-00-00', './templates/default/images/avater.jpg', '{\\"name\\":\\"\\\\u5f20\\\\u831c\\",\\"nick_name\\":\\"\\\\u831c\\\\u513f\\",\\"tel\\":\\"13764689718\\",\\"weixin\\":\\"\\"}', 0, 0, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `user_address`
--

CREATE TABLE IF NOT EXISTS `user_address` (
  `address_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `zan`
--

CREATE TABLE IF NOT EXISTS `zan` (
  `z_id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT NULL COMMENT '1-店铺,2-商品',
  `id` int(10) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`z_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `zan`
--

INSERT INTO `zan` (`z_id`, `type`, `id`, `ip`, `user_name`, `add_time`) VALUES
(1, 1, 1, '195.0.1.35', 'zhangdujian', 1389847429),
(2, 1, 3, '195.0.1.35', '', 1389855354),
(3, 1, 3, '195.0.1.35', 'zhangdujian', 1389855455),
(4, 1, 7, '195.0.1.35', 'zhangdujian', 1389859424),
(5, 1, 8, '195.0.1.35', 'lixiaoxu', 1389867181),
(6, 1, 8, '195.0.1.35', '', 1389916440),
(7, 1, 12, '195.0.1.35', '', 1389948987),
(8, 1, 7, '195.0.1.35', '', 1390189164),
(9, 1, 8, '195.0.1.35', 'dlcmbc', 1390190768);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
