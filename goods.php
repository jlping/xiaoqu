<?php

define('MANS_', true);
require (dirname(__FILE__) . '/includes/init.php');

$goods_id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

/* 获得商品的信息 */
$goods = $Goods->goodsInfo($goods_id);
$goods = $Other->xqGoods($goods);


$sm->assign('is_cat_admin',	$User->isCatAdmin($goods['cat_id']));


if (!$goods) {
    header("location:./");
}
$this_url = $goods['url'];
//$Main->url301($this_url);
$sm->assign('isWished',$User->iSwished($goods_id,1));

if(!$sm->isCached('goods.htm',$cache_id))
{

	$Main->anchorMatch();
	$Main->cronGoods();
	$sm->assign('goods',	$goods);
    $len = @count($_COOKIE['recent_goods']);
	@setcookie("recent_goods[$len]",$goods_id,strtotime("+1 years"),'/ ');



	$rand_goods = $Goods->randGoods(array($goods['cat_id']),15);

	$rand_goods = array_chunk($rand_goods,5);
	$sm->assign('rand_goods',	$rand_goods);

	$user_info = @$User->userInfo($_SESSION['user_id']);
	$sm->assign('user_info', $user_info);
	$cat_ids = $Goods->ids($goods['cat_id']);

	$sm->assign('cat_ids',	@json_encode($cat_ids));

	//print_r($goods);


	$position = $Main->position($goods,GOODS_NAME);
	$Main->assigns($position);
	$reviews = $User->reviewList(0,10,"where id=$goods_id and type=2",false);
	$sm->assign('review',	$reviews);

	//print_r($order_reviews);die();
	$sm->assign('arr10', array_fill(0,10,''));
	$Goods->goodsClicks($goods_id);

	$faq = $Cate->article(76);
	$sm->assign('faq',	$faq);
	$sm->assign('id',	$goods_id);
	$sm->assign('review_type',	2);
	$user_id = @intval($_SESSION['user_id']);
	$sql = "select o.order_id,o.review_id,og.goods_id from order_goods as og inner  join order_info as o on o.order_id=og.order_id where
	o.user_id=$user_id and og.goods_id=$goods_id";

	$user_is_ordered = $GLOBALS['db']->getRow($sql);

	$sm->assign('user_is_ordered',	$user_is_ordered);


	//print_r($faq);


}

$sm->display("goods.htm",$cache_id);
?>

