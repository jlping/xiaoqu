<?php /* Smarty version Smarty-3.1.10, created on 2013-08-05 07:55:11
         compiled from "G:\web\htdocs\gothpunks\m_admin\templates\libs\act\config.htm" */ ?>
<?php /*%%SmartyHeaderCode:1007051fe33983dd353-38658526%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e6a45502b3a98c025bd911ef2ae92eb304574cb1' => 
    array (
      0 => 'G:\\web\\htdocs\\gothpunks\\m_admin\\templates\\libs\\act\\config.htm',
      1 => 1375689310,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1007051fe33983dd353-38658526',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_51fe339843b9b5_17802187',
  'variables' => 
  array (
    'conf' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51fe339843b9b5_17802187')) {function content_51fe339843b9b5_17802187($_smarty_tpl) {?>

<article class="module width_3_quarter">
  <header>
    <h3 class="tabs_involved">系统变量</h3>
    
  </header>
  <div class="tab_container" id="sys_conf">
    <div id="tab1" class="tab_content">
      <table class="tablesorter" cellspacing="0">
        <thead>
          <tr>
            <th width="150px;">名称</th>
            <th width="150px;">调用代码</th>
            <th>内容</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>网站名称</td>
            <td>{$conf.shop_name}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['conf']->value['shop_name'];?>
"  id="shop_name"/></td>
          </tr>
          <tr>
            <td>网站标题</td>
             <td>{$conf.shop_title}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['conf']->value['shop_title'];?>
" id="shop_title"/></td>
          </tr>
          <tr>
            <td>网站关键词</td>
            <td>{$conf.shop_keywords}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['conf']->value['shop_keywords'];?>
" id="shop_keywords"/></td>
          </tr>
           <tr>
            <td>网站描述</td>
           <td>{$conf.shop_desc}</td>
            <td><textarea id="shop_desc" style="height:100px"><?php echo $_smarty_tpl->tpl_vars['conf']->value['shop_desc'];?>
</textarea></td>
          </tr>
           <tr>
            <td>缩略图尺寸</td>
            <td>{$conf.thumb_size}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['conf']->value['thumb_size'];?>
" id="thumb_size"/>
            
            </td>
          </tr>
           <tr>
            <td>缓存时间</td>
             <td>{$conf.cache_time}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['conf']->value['cache_time'];?>
" id="cache_time"/>
            <br/><small class="green">设置为1--永不过期，0--无缓存,单位s(秒)</small>
            </td>
          </tr>
          <tr>
          	<td colspan="2" align="center"><input type="submit" class="alt_btn" value="保存" onclick="AD.saveSysConf()"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- end of #tab1 --> 
    
  </div>
  <!-- end of .tab_container --> 
  
</article>
<?php }} ?>