<?php /* Smarty version Smarty-3.1.10, created on 2013-08-06 03:17:10
         compiled from "G:\web\htdocs\gothpunks\m_admin\templates\libs\act\article_list.htm" */ ?>
<?php /*%%SmartyHeaderCode:2023752006ab66fa5d8-11736853%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4bbe8f05ae5f3ea413baf55f8d9d07f1777841e4' => 
    array (
      0 => 'G:\\web\\htdocs\\gothpunks\\m_admin\\templates\\libs\\act\\article_list.htm',
      1 => 1374657518,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2023752006ab66fa5d8-11736853',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cat_list' => 0,
    'list' => 0,
    'sep' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_52006ab67c90c6_14292128',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52006ab67c90c6_14292128')) {function content_52006ab67c90c6_14292128($_smarty_tpl) {?>
<article class="module width_3_quarter">
  <header>
    <h3 class="tabs_involved">文章分类</h3>
    <ul class="tabs">
      <li><a href="articleAdd">新增文章</a></li>
    </ul>
  </header>
  <div class="tab_container" >
    <div id="tab1" class="tab_content">
      <table class="tablesorter" cellspacing="0">
        <thead>
          <tr>
            <th width="50px;">ID</th>
            <th width="250px;">标题</th>
            <th width="100px;">时间</th>
            <th width="60px;">状态</th>
            <th width="80px;">操作</th>
          </tr>
        </thead>
        <tbody>
        
        <?php  $_smarty_tpl->tpl_vars['list'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list']->key => $_smarty_tpl->tpl_vars['list']->value){
$_smarty_tpl->tpl_vars['list']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['list']->key;
?>
        <tr class="cat_add<?php echo $_smarty_tpl->tpl_vars['list']->value['cat_id'];?>
">
          <td><input type="checkbox" name="list_checkbox" value="<?php echo $_smarty_tpl->tpl_vars['list']->value['article_id'];?>
"/><?php echo $_smarty_tpl->tpl_vars['list']->value['article_id'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['list']->value['title'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['list']->value['add_time'];?>
</td>
          <td><?php if ($_smarty_tpl->tpl_vars['list']->value['is_open']){?><small class="green">显示</small><?php }else{ ?><small>隐藏</small><?php }?></td>
          <td><a class="alt_btn" href="<?php echo $_smarty_tpl->tpl_vars['sep']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['list']->value['url'];?>
" target="_blank">查看</a> / 
          	  <a class="alt_btn" href="articleEdit-<?php echo $_smarty_tpl->tpl_vars['list']->value['article_id'];?>
">编辑</a></td>
        </tr>
        <?php } ?>
          </tbody>
        
      </table>
    </div>
    <!-- end of #tab1 --> 
    
  </div>
  <?php echo $_smarty_tpl->getSubTemplate ("../footer_bar.htm", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  <!-- end of .tab_container --> 
  <?php echo $_smarty_tpl->getSubTemplate ("../pages.htm", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
 
  </article>
<?php }} ?>