<?php /* Smarty version Smarty-3.1.10, created on 2013-08-01 03:49:24
         compiled from "G:\web\htdocs\ethnicharms2\m_admin\templates\libs\act\config.htm" */ ?>
<?php /*%%SmartyHeaderCode:956451f9dac4bef551-10224122%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e8ca510d0cd9f4440535b40c6789bb2e00267edb' => 
    array (
      0 => 'G:\\web\\htdocs\\ethnicharms2\\m_admin\\templates\\libs\\act\\config.htm',
      1 => 1373621321,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '956451f9dac4bef551-10224122',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cfg' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_51f9dac4c4f3a8_73489684',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51f9dac4c4f3a8_73489684')) {function content_51f9dac4c4f3a8_73489684($_smarty_tpl) {?>

<article class="module width_3_quarter">
  <header>
    <h3 class="tabs_involved">系统变量</h3>
    
  </header>
  <div class="tab_container" id="sys_conf">
    <div id="tab1" class="tab_content">
      <table class="tablesorter" cellspacing="0">
        <thead>
          <tr>
            <th width="150px;">名称</th>
            <th width="150px;">调用代码</th>
            <th>内容</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>网站名称</td>
            <td>{$cfg.shop_name}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['cfg']->value['shop_name'];?>
"  id="shop_name"/></td>
          </tr>
          <tr>
            <td>网站标题</td>
             <td>{$cfg.shop_title}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['cfg']->value['shop_title'];?>
" id="shop_title"/></td>
          </tr>
          <tr>
            <td>网站关键词</td>
            <td>{$cfg.shop_keywords}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['cfg']->value['shop_keywords'];?>
" id="shop_keywords"/></td>
          </tr>
           <tr>
            <td>网站描述</td>
           <td>{$cfg.shop_desc}</td>
            <td><textarea id="shop_desc" style="height:100px"><?php echo $_smarty_tpl->tpl_vars['cfg']->value['shop_desc'];?>
</textarea></td>
          </tr>
           <tr>
            <td>缩略图尺寸</td>
            <td>{$cfg.thumb_size}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['cfg']->value['thumb_size'];?>
" id="thumb_size"/>
            
            </td>
          </tr>
           <tr>
            <td>缓存时间</td>
             <td>{$cfg.cache_time}</td>
            <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['cfg']->value['cache_time'];?>
" id="cache_time"/>
            <br/><small class="green">设置为1--永不过期，0--无缓存,单位s(秒)</small>
            </td>
          </tr>
          <tr>
          	<td colspan="2" align="center"><input type="submit" class="alt_btn" value="保存" onclick="AD.saveSysConf()"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- end of #tab1 --> 
    
  </div>
  <!-- end of .tab_container --> 
  
</article>
<?php }} ?>