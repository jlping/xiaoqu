<?php /* Smarty version Smarty-3.1.10, created on 2013-08-06 03:17:19
         compiled from "G:\web\htdocs\gothpunks\m_admin\templates\libs\act\article_edit.htm" */ ?>
<?php /*%%SmartyHeaderCode:616052006abf437050-17809327%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd83e8afe1d67f1ed843a9e458243bb1b8ac632f' => 
    array (
      0 => 'G:\\web\\htdocs\\gothpunks\\m_admin\\templates\\libs\\act\\article_edit.htm',
      1 => 1374658992,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '616052006abf437050-17809327',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'article' => 0,
    'cat_list' => 0,
    'list' => 0,
    's_list' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_52006abf4b6c01_75429383',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52006abf4b6c01_75429383')) {function content_52006abf4b6c01_75429383($_smarty_tpl) {?>
<article class="module width_full" id="article_add">
    <header>
      <h3>编辑文章</h3>
    </header>
    
    <div class="module_content">
   
      <fieldset>
        <label>标题</label>
        <input type="text" name="title" value="<?php echo $_smarty_tpl->tpl_vars['article']->value['title'];?>
">
      </fieldset>
        <fieldset style="width:48%; float:left; margin-right: 3%;">
       
        <label>分类</label>
        <select style="width:92%;" name="cat_id">
          <option>请选择</option>
          <?php  $_smarty_tpl->tpl_vars['list'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cat_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list']->key => $_smarty_tpl->tpl_vars['list']->value){
$_smarty_tpl->tpl_vars['list']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['list']->key;
?>
          <option value="<?php echo $_smarty_tpl->tpl_vars['list']->value['cat_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['article']->value['cat_id']==$_smarty_tpl->tpl_vars['list']->value['cat_id']){?>selected="true"<?php }?>><?php echo $_smarty_tpl->tpl_vars['list']->value['cat_name'];?>
</option>
          	 <?php  $_smarty_tpl->tpl_vars['s_list'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s_list']->_loop = false;
 $_smarty_tpl->tpl_vars['s_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list']->value['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s_list']->key => $_smarty_tpl->tpl_vars['s_list']->value){
$_smarty_tpl->tpl_vars['s_list']->_loop = true;
 $_smarty_tpl->tpl_vars['s_key']->value = $_smarty_tpl->tpl_vars['s_list']->key;
?>
         	 <option value="<?php echo $_smarty_tpl->tpl_vars['s_list']->value['cat_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['article']->value['cat_id']==$_smarty_tpl->tpl_vars['s_list']->value['cat_id']){?>selected="true"<?php }?>><?php echo $_smarty_tpl->tpl_vars['s_list']->value['cat_name'];?>
</option>
          	<?php } ?>
          <?php } ?>
        
        </select>
      </fieldset>
      <fieldset style="width:48%; float:left;">
       
        <label>作者</label>
        <input type="text" style="width:92%" name="author" value="<?php echo $_smarty_tpl->tpl_vars['article']->value['author'];?>
">
      </fieldset>
      <fieldset style="width:48%; float:left;;margin-right: 3%;">
      
        <label>keywords</label>
        <input type="text" style="width:92%;" name="keywords" value="<?php echo $_smarty_tpl->tpl_vars['article']->value['keywords'];?>
">
      </fieldset>
      <fieldset style="width:48%; float:left;">
      
        <label>description</label>
        <input type="text" style="width:92%;" name="description" value="<?php echo $_smarty_tpl->tpl_vars['article']->value['description'];?>
">
      </fieldset>
      <fieldset style="width:48%; float:left;">
       
        <label>关联商品ID</label>
        <input type="text" style="width:92%;">
      </fieldset>
      <div class="clear"></div>
      <fieldset>
       
        <textarea rows="12" name="content"><?php echo $_smarty_tpl->tpl_vars['article']->value['content'];?>
</textarea>
        <script>
          Main.ckeditor('content');
        </script>
      </fieldset>
    
      <div class="clear"></div>
    </div>
    <footer>
      <div class="submit_link">
        <select name="is_open">
          <option value="1">是否显示?</option>
          <option value="1" <?php if ($_smarty_tpl->tpl_vars['article']->value['is_open']==1){?>selected="true"<?php }?>>是</option>
          <option value="0" <?php if ($_smarty_tpl->tpl_vars['article']->value['is_open']==0){?>selected="true"<?php }?>>否</option>
        </select>
        <input type="submit" value="发布" class="alt_btn" onclick="AD.articleEdit('#article_add',<?php echo $_smarty_tpl->tpl_vars['article']->value['article_id'];?>
)">
       </div>
    </footer>
  </article>
<?php }} ?>