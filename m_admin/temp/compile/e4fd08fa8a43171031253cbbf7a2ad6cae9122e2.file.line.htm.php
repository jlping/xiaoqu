<?php /* Smarty version Smarty-3.1.10, created on 2013-08-04 10:57:18
         compiled from "G:\web\htdocs\gothpunks\m_admin\templates\libs\tools\chart\line.htm" */ ?>
<?php /*%%SmartyHeaderCode:1602351fe338ea3b048-43193727%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e4fd08fa8a43171031253cbbf7a2ad6cae9122e2' => 
    array (
      0 => 'G:\\web\\htdocs\\gothpunks\\m_admin\\templates\\libs\\tools\\chart\\line.htm',
      1 => 1375154243,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1602351fe338ea3b048-43193727',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    's_path' => 0,
    'line_chart' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_51fe338ea896f7_75480982',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51fe338ea896f7_75480982')) {function content_51fe338ea896f7_75480982($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['s_path']->value;?>
/js/highcharts.js"></script>
<script type="text/javascript">

$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'chart2',
                type: 'column',
                marginRight: 130,
                marginBottom: 25
				
            },
            title: {
                text: '每天的订单数',
                x: -20 //center
            },
           
            xAxis: {
                categories: <?php echo $_smarty_tpl->tpl_vars['line_chart']->value['cats'];?>

            },
            yAxis: {
                title: {
                    text: '订单 (数)'
                }
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'个';
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
			
                x: -10,
                y: 100
                
            },
            series: <?php echo $_smarty_tpl->tpl_vars['line_chart']->value['series'];?>

        });
    });
    
});
		</script>
        <div id="chart2" style="min-width: <?php echo $_smarty_tpl->tpl_vars['line_chart']->value['count']*40;?>
px; height: 400px; margin: 0 auto; padding:10px 0;"></div>
    <?php }} ?>