<?php

//自定义系统设置
if ($act == 'config') {
    $config = $Main->loadConfig();
    $sm->assign('conf', $config);

}
elseif ($act == 'cus_config') {
    $config = $Main->loadConfig(false);
    $sm->assign('conf', $config);
}
//修改配置
elseif ($act == 'add_config') {
    $data = $Main->getPostData($_POST, array(
        'act'
    ));
    echo $GLOBALS['db']->autoExecute('config', $data);
    die();
}
//删除配置
elseif ($act == 'del_config') {
    $c_id = !empty($_POST['c_id']) ? intval($_POST['c_id']) : 0;
    $sql = "delete from config where c_id=$c_id";
    echo mysql_query($sql);
    die();
}
//修改配置
elseif ($act == 'edit_config') {
    $data = !empty($_POST['data']) ? $Main->json2Array(stripcslashes($_POST['data'])) : '';
    echo $Main->editConfig($data);
    die();
}
//修改一个配置
elseif ($act == 'edit_config1') {
    $c_id = !empty($_POST['c_id']) ? intval($_POST['c_id']) : 0;
    $data = $Main->getPostData($_POST, array(
        'act',
        'c_id'
    ));
    echo $GLOBALS['db']->autoExecute('config', $data, 'update', " c_id=" . $c_id);
    die();
}
//运费设置
elseif ($act == 'shipping') {
    $shipping_list = $Main->shippingList();
    $sm->assign('slist', $shipping_list);
}
//新增运费
elseif ($act == 'shipping_add') {
    $data = !empty($_POST['data']) ? $_POST['data'] : '';
    echo $Main->shippingAdd($data);
    die();
}
//删除运费
elseif ($act == 'shipping_del') {
    $shipping_id = !empty($_POST['shipping_id']) ? intval($_POST['shipping_id']) : 0;
    $sql = "delete from shipping where shipping_id=$shipping_id";
    echo mysql_query($sql);
    die();
}
//修改运费
elseif ($act == 'shipping_edit') {
    $shipping_id = !empty($_POST['shipping_id']) ? intval($_POST['shipping_id']) : 0;
    $data = !empty($_POST['data']) ? $_POST['data'] : '';
    echo $Main->shippingEdit($data, $shipping_id);
    die();
}
//支付设置
elseif ($act == 'payment') {
    $payment_list = $Main->paymentList();
    $sm->assign('plist', $payment_list);
}
//支付增加
elseif ($act == 'payment_add') {
    $data = !empty($_POST['data']) ? $_POST['data'] : '';
    echo $Main->paymentAdd($data);
    die();
}
//删除增加
elseif ($act == 'payment_del') {
    $pay_id = !empty($_POST['pay_id']) ? intval($_POST['pay_id']) : 0;
    $sql = "delete from  payment where pay_id=$pay_id";
    echo mysql_query($sql);
    die();
}
//删除修改
elseif ($act == 'payment_edit') {
    $pay_id = !empty($_POST['pay_id']) ? intval($_POST['pay_id']) : 0;
    $data = !empty($_POST['data']) ? $_POST['data'] : '';
    echo $Main->paymentEdit($data, $pay_id);
    die();
}

//邮件设置
elseif ($act == 'email_set') {
    $templates = $Main->mailTemplate();
    //print_r($templates);
    $sm->assign('templates', $templates);
}
//更爱邮件设置
elseif ($act == 'email_set_edit') {
    $data = !empty($_POST['data']) ? $_POST['data'] : '';
    echo $Main->emailSetEdit($data);
    die();
}
//更爱邮件设置
elseif ($act == 'log_on') {
	$cnzz = $GLOBALS['db']->getOne("select value from config where code='cnzz'");
	if($_POST)
	{
		unset($_POST['act']);
		$data['value'] = json_encode(($_POST));
		$GLOBALS['db']->autoExecute('config',$data,'update',"code='cnzz'");
		$Main->msg('','',true);

	}
	$cnzz  = $Main->json2Array($cnzz);

	$sm->assign('cnzz',$cnzz);
}
?>