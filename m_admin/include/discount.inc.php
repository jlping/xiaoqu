<?php
if ($act == "discount") {
    $where = '';
    $discount = $Main->discountList($start, $size, $where);
    $sm->assign('discount', $discount);
    $sql = "select count(*) from discount $where";
    $Main->setPage($sql, $size, 'discount', 'p');
}
//设置优惠码
elseif ($act == 'set_discount') {
    $value = !empty($_POST['value']) ? floatval($_POST['value']) : 0;
    $res = $Main->setDiscount($value);
    die('1');
}
//删除优惠码
elseif ($act == "del_discount") {
    $discount_id = !empty($_GET['discount_id']) ? intval($_GET['discount_id']) : 0;
    if ($Main->delDiscount($discount_id)) {
        $Main->msg("操作成功!", "admins.php?act=discount");
    }
}
//批量惠码
elseif ($act == "batch_discount") {
	$money = !empty($_POST['money']) ? floatval($_POST['money']) :  0.1 ; 
    $sta = !empty($_REQUEST['sta']) ? $_REQUEST['sta'] : '';
    $where = "";
    if ($sta) {
        $sp = preg_split("/\*/", $sta);
        if (count($sp) == 2) {
            $field = $sp[0];
            $value = $sp[1];
            $where = " where `$field`='$value' ";
        } else {
            $where = " where order_sn like '%$sta%' or address like '%$sta%'";
        }
    }
    $order_list = @$Order->orderList($start, $size, $where);
	//print_r($order_list);
	foreach($order_list as $key=>$val)
	{
		$v = round($val['goods_amount']*$money);
		
		$code = $GLOBALS['db']->getRow("select code,value from discount where order_id='".$val['order_id']."' and status=1 ");
		if($code)
		{
			$val['code'] = $code;
			
		}
		else
		{
			$val['code'] = $Main->setDiscount($v,$val['order_id']);
		}
		$order_list[$key] = $val;
		
	}
    $sm->assign('order_list', $order_list);
    $sm->assign('sta', $sta);
	$Main->setPage("select count(*) from order_info $where ", $size, $act);
}
?>