<?php

if ($act == 'goods_cat') {
  $cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
  $cat_name = !empty($_REQUEST['cat_name']) ? ($_REQUEST['cat_name']) : '';
  $where = "";
  if($cat_id)
  	$where = " where cat_id=$cat_id";
  if($cat_name)
  	$where = " where cat_name like '%$cat_name%'";
  $count = $Main->counts("select count(*) from category $where");
  $pager = $Main->pager($count,$size,'admins.php?act=goods_cat&cat_name='.$cat_name);
  $cat = $Cate->categoryAll($start,$size,$where);
 // print_r($cat);die();
  $sm->assign('cat',	$cat);
  $sm->assign('pagers',	$pager);
}
elseif ($act == 'goods_cat_export') {
	$count = $Main->counts("select count(*) from category ");

  	$cat = $Cate->categoryAll($start,$count,"where 1");

  	$table = "<table border=1><tr><th>ID</th><th>名称</th><th>管理员</th><th>店员</th><th>管理员ID</th><th>电话</th><th>点击量</th><th>赞</th><th>商品数</th><th>积分</th></tr>";
  	foreach($cat as $key=>$val)
  	{
  		$cat_id = $val['cat_id'];
  		$member = @$Main->json2Array($val['members']);
  		$m = array();
  		if(is_array($member))
  		{

  			foreach($member as $_k=>$_v)
  			{
				$m[]=$_v['user_id'].'='.$_v['name'];
  			}

  		}

  		$goods_num = $GLOBALS['db']->getOne("select count(*) from goods where cat_id=$cat_id;");
  		$zan = $GLOBALS['db']->getOne("select count(*) from zan where id=$cat_id and type=1");
  		$table .="<tr><th>".$val['cat_id']."</th><th>".$val['cat_name']."</th><th>".$val['mag']['info']['name']."</th><th>".join($m,'|')."</th><th>".$val['mag_id']."</th><th>".$val['phone']."</th><th>".$val['click_count']."</th><th>".$zan."</th><th>".$goods_num."</th><th>".$val['gold']."</th></tr>";
  	}
  	$table.="</table>";
  	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();

}
elseif ($act == 'goods_list_export') {
	$count = $Main->counts("select count(*) from goods ");

  	$goods = $Goods->goodsList($start,$count);

  	$table = "<table border=1><tr><th>ID</th><th>类型</th><th>名称</th><th>关键词</th><th>卖点</th><th>店铺ID</th><th>投产成本</th><th>售价 </th><th>点击量</th><th>销售量</th><th>热度</th><th>好评度 </th><th>上传时间</th></tr>";
  	foreach($goods as $key=>$val)
  	{

  		$goods_id = $val['goods_id'];
  		//$zan = $Main->counts("select count(*) from zan where id=".$goods_id." and type=2");
		$haop = @round($val['haoping']/($val['h']['h3']+$val['h']['h2']+$val['h']['h1']));
		$per_haop = $haop ? $haop.'%': '60%';
		$type = $val['cat_type']==1 ? '精益' : '小区';
  		$table .="<tr><td>".$goods_id."</td><td>".$type."</td><td>".$val['goods_name']."</td><td>".$val['keywords']."</td><td>".$val['descs']."</td><td>".$val['cat_id']."</td>".
  				"<td>".$val['market_price']."</td><td>".$val['shop_price']." </td><td>".$val['click_count']."</td><td>".$val['goods_number']."</td>" .
  						 "<td>".$val['download']."</td><td>".$per_haop." </td><td>".($val['add_time'])." </td></tr>";
  	}
  	$table.="</table>";
  	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();

}
elseif ($act == 'goods_list_export2') {
	$count = $Main->counts("select count(*) from goods");

  	$goods = $Goods->goodsList($start,$count);

    $k = array();
   $d = array();
   $kSp = array();
   $dSp = array();
  	foreach($goods as $key=>$val)
  	{

  		$kSp[] =  $kp= preg_split("/,/",$val['keywords']);
  		$dSp[] = $dp  = preg_split("/,/",$val['descs']);
  		$k[] = count($kp);
  		$d[] = count($dp);

  	}
  	//print_r($kSp);die();
  	$table = "<table border=1><tr><th>ID</th><th>类型</th><th>名称</th><th colspan=".(max($k)).">关键词</th><th colspan=".max($d).">卖点</th></tr>";
  	foreach($goods as $key=>$val)
  	{

  		$goods_id = $val['goods_id'];
  		//$zan = $Main->counts("select count(*) from zan where id=".$goods_id." and type=2");
		$haop = @round($val['haoping']/($val['h']['h3']+$val['h']['h2']+$val['h']['h1']));
		$per_haop = $haop ? $haop.'%': '60%';
		$type = $val['cat_type']==1 ? '精益' : '小区';
		$ks = "";
		for($i=0;$i<max($k);$i++)
		{
			$ks.="<td>&nbsp;".@$kSp[$key][$i]."</td>";
		}
		$ds = "";
		for($j=0;$j<max($d);$j++)
		{
			$ds.="<td>&nbsp;".@$dSp[$key][$j]."</td>";
		}

  		$table .="<tr><td>".$goods_id."</td><td>".$type."</td><td>".$val['goods_name']."</td>".$ks.$ds."</tr>";
  	}
  	$table.="</table>";


  	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();

}
elseif ($act == 'goods_cat_add') {
	if(!empty($_POST))
	{
		$data = $Main->getPostData($_POST, array(
		'act',
        'cat_id'
		));
		if($GLOBALS['db']->autoExecute('category',$data))
		{
			$Main->msg("操作成功!","goodsCat");
		}
	}
	$sm->assign('cat_list', $categories);
	$sm->assign('acts','goods_cat_add');
}
elseif ($act == 'goods_cat_edit') {
	$cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
	if(!empty($_POST))
	{
	  $data = $Main->getPostData($_POST, array(
		'act',
        'cat_id'
		));

		if($GLOBALS['db']->autoExecute('category',$data,'UPDAET'," cat_id=$cat_id"))
		{
			$Main->msg("操作成功!","goodsCat");
		}

	}
	else
	{
		$cat_info  = $Cate->CatInfo($cat_id);
		//print_r($cat_info);
		$sm->assign('cat',$cat_info);
	}
   $sm->assign('acts','goods_cat_edit');

}

elseif ($act == 'del_goods_cat') {
    $cat_id = !empty($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
    $res = mysql_query("delete from category where cat_id=$cat_id");
    if($res)
	{
		$Main->msg("操作成功!","goodsCat");
	}
}
//商品列表
elseif($act=='goods_list')
{
	 $cat_id = !empty($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
	 $cat_type = !empty($_GET['cat_type']) ? intval($_GET['cat_type']) : 0;
	 $goods_id = !empty($_GET['goods_id']) ? intval($_GET['goods_id']) : 0;
	 if($cat_id>0)
	{
	 $cat = $Cate->catInfo($cat_id);
	 $ids = $Goods->ids($cat_id);
	 $join = ' (cat_id='.join($ids,' or cat_id=').')';
	}
	if($goods_id)
	{
		$join = " goods_id=$goods_id ";
	}
	else
	{
		$join= "cat_type=$cat_type";
	}
	 $where = " where  $join   order by goods_id desc";
	 $count = $Main->counts("select count(*) from goods $where");
     $pager = $Main->pager($count,$size,'admins.php?act=goods_list&cat_id='.$cat_id);
	 $goods_list = $Goods->goodsList($start,$size,$where,', click_count');
	// print_r($goods_list);die();
	 $sm->assign('goods_list',	$goods_list);
	 $sm->assign('pagers',	$pager);
	// print_r($goods_list);
}
//新增商品
elseif($act=='goods_add')
{
	preg_match("/goodsList-(\d+)/",$_SERVER['HTTP_REFERER'],$arr);
	$cat_id = @$arr[1];
	$sm->assign('cat_id',	$cat_id);
	if(!empty($_POST))
	{
		if($Goods->goodsEdit($_POST))
		{
			$Main->msg("操作成功!","goodsList-".$cat_id);
		}
	}

}
//商品编辑
elseif($act=='goods_edit')
{
	$goods_id = isset($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	if(!empty($_POST))
	{
		if($Goods->goodsEdit($_POST))
		{
			$Main->msg("操作成功!","goodsEdit-".$_POST['goods_id']);
		}

	}
	else
	{
		/* 获得商品的信息 */
		$goods = $Goods->goodsInfo($goods_id);

		$or['img_original'] = $goods['img'];
		$or['img'] = $goods['img'];
		$goods['gallery'][] = $or;

		$sm->assign('goods',	$goods);
		$attrs    = $Goods->goodsAttrs($goods_id);
		$sm->assign('attrs',	$attrs);
	}
}
//商品删除
elseif($act=='goods_delete')
{
	$goods_id = isset($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	preg_match("/goodsList-(\d+)/",$_SERVER['HTTP_REFERER'],$arr);
	$cat_id = @$arr[1];
	if($Goods->goodsDelete($goods_id))
	{
		$Main->msg("操作成功!","admins.php?act=goods_list");
	}
}
//套餐组合
elseif($act=='goods_package')
{
   $package_lst =$Goods->goodsPackage($start,$size);
	$sm->assign('lists',  $package_lst);

}
elseif($act=='goods_package_add')
{
	 if(!empty($_POST))
	{
     $data = $Main->getPostData($_POST, array('act'));
     if($GLOBALS['db']->autoExecute('goods_package',$data))
     {
            $Main->msg('添加成功！','admins.php?act=goods_package');
     }
	}
}
elseif($act=='goods_package_edit')
{
	 $p_id = isset($_REQUEST['p_id']) ? intval($_REQUEST['p_id']) : 0;

	 if(!empty($_POST))
	{
     $data = $Main->getPostData($_POST, array('act','p_id'));

     if($GLOBALS['db']->autoExecute('goods_package',$data,'update'," p_id=$p_id"))
     {
            $Main->msg('修改成功！','admins.php?act=goods_package');
     }
	}
	else
	{
		 $package =$Goods->goodsPackage(0,1," where p_id=$p_id");
		 $sm->assign('package',@$package[0]);
	}
}
elseif($act=='goods_package_del')
{
    $p_id = isset($_REQUEST['p_id']) ? intval($_REQUEST['p_id']) : 0;
	$sql = "delete from goods_package where p_id=$p_id";
	if(mysql_query($sql))
	{
		$Main->msg("删除成功!",'admins.php?act=goods_package');
	}

}

elseif($act=='cat_sort')
{
	$cat_id = isset($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
	$data['sort_order'] = isset($_REQUEST['sort']) ? intval($_REQUEST['sort']) : 0;
	$data['sort_order'] = $data['sort_order']==0 ? 50 :$data['sort_order'];
	if($cat_id)
		$res = $GLOBALS['db']->autoExecute('category',$data,'update',"cat_id=$cat_id");
	die('1');
}

elseif($act=='goods_sort')
{
	$goods_id = isset($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	$data['sort_order'] = isset($_REQUEST['sort']) ? intval($_REQUEST['sort']) : 0;
	$data['sort_order'] = $data['sort_order']==0 ? 50 :$data['sort_order'];
	if($goods_id)
		$res = $GLOBALS['db']->autoExecute('goods',$data,'update',"goods_id=$goods_id");
	die('1');
}
elseif($act=='sort')
{
	$goods_id = isset($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	$data['sort'] = isset($_REQUEST['sort']) ? intval($_REQUEST['sort']) : 0;

	if($goods_id)
		$res = $GLOBALS['db']->autoExecute('goods',$data,'update',"goods_id=$goods_id");
	die('1');
}

elseif($act=='catPlusV')
{
	$cat_id = isset($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
	$is_show = isset($_REQUEST['is_show']) ? intval($_REQUEST['is_show']) : 0;
	$c['is_show'] = $is_show;
	if($cat_id && $is_show)
		$res = $GLOBALS['db']->autoExecute('category',$c,'update',"cat_id=$cat_id");
	die('1');
	print_r($c);die();
}
elseif($act=='goodsPlusV')
{
	$goods_id = isset($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	$goods_weight = isset($_REQUEST['goods_weight']) ? intval($_REQUEST['goods_weight']) : 0;
	$c['goods_weight'] = $goods_weight;

	if($goods_id )
		$res = $GLOBALS['db']->autoExecute('goods',$c,'update',"goods_id=$goods_id");
	die('1');

}
?>