<?php
//文章管理
if ($act == 'article_cat') {
    $cat_list = $Cate->artCatTree();
    $sm->assign('cat_list', $cat_list);

}
//新增分类
elseif ($act == 'article_cat_add') {

	if(!empty($_POST))
	{
	  $data = $Main->getPostData($_POST, array(
		'act'
		));

		if($GLOBALS['db']->autoExecute('article_cat',$data))
		{
			$Main->msg("操作成功!","admins.php?act=article_cat");
		}

	}
	else
	{
		 $cat_list = $Cate->artCatTree();
		 $sm->assign('categories', $cat_list);

	}
	$sm->assign('acts','article_cat_add');
}
//修改分类
elseif ($act == 'article_cat_edit') {
   $cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;

   if(!empty($_POST))
	{
	  $data = $Main->getPostData($_POST, array(
		'act',
        'cat_id'
		));

		if($GLOBALS['db']->autoExecute('article_cat',$data,'UPDAET'," cat_id=$cat_id"))
		{
			$Main->msg("操作成功!","admins.php?act=article_cat");
		}
	}
	else
	{
		$cat_info  = $Cate->artCatInfo($cat_id);
		$sm->assign('cat',$cat_info);
		$cat_list = $Cate->artCatTree();
		$sm->assign('cat_list', $cat_list);
	}

	$sm->assign('acts','article_cat_edit');
}
//删除分类
elseif ($act == 'del_article_cat') {
    $cat_id = !empty($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
    $res =  $GLOBALS['db']->query("delete from article_cat where cat_id=$cat_id");
   if($res)
   {
	 $Main->msg("操作成功!","articleCat");
   }

}
//文章列表
elseif ($act == 'article_list') {
    $cat_id = !empty($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
    $where = " where cat_id=$cat_id  ";
    $sql = "select count(*) from article $where";
    $cat_list = $Cate->catArticles($start, $size, $where);
    $Main->setPage($sql, $size, 'admins.php?act=article_list&cat_id=' . $cat_id);
    $sm->assign('cat_list', $cat_list);
	$sm->assign('cat_id', $cat_id);
}
//文章增加

elseif ($act == 'article_add') {
	$cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
	if(!empty($_POST))
	{
		$data = $Main->getPostData($_POST, array(
        'act',
        'a_id'
		 ));
		 $data['add_time'] = time();

       $res =  $GLOBALS['db']->autoExecute('article', $data);
	   if($res)
	  {
		$Main->msg("操作成功！","admins.php?act=article_list&cat_id=".$cat_id);
	   }
	}
	else
	{
		$cat_list = $Cate->artCatTree();
		$sm->assign('cat_list', $cat_list);
		$sm->assign('cat_id', $cat_id);
	}
}

//文章编辑

elseif ($act == 'article_edit') {
    $a_id = !empty($_REQUEST['a_id']) ? intval($_REQUEST['a_id']) : 0;
	if(!empty($_POST))
	{
		$data = $Main->getPostData($_POST, array(
        'act',
        'a_id'
		));
		$res = $GLOBALS['db']->autoExecute('article', $data, 'upadte', "article_id=" . $a_id);
		if($res)
		{
			$cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
			 $Main->msg("操作成功!","admins.php?act=article_list&cat_id=".$cat_id);
		}
	}
	else
	{
		$cat_list = $Cate->artCatTree();
		$sm->assign('cat_list', $cat_list);
		$article = $Cate->article($a_id);
		$sm->assign('article', $article);
	}
}
//删除文章
elseif($act=='del_article')
{
	$a_id = !empty($_REQUEST['a_id']) ? intval($_REQUEST['a_id']) : 0;
	$res = mysql_query("delete from article where article_id=$a_id");
	if($res)
	{
		$cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
		$Main->msg("操作成功!","articleList-".$cat_id);
	}
}
?>