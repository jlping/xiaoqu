<?php
//用户列表
if ($act == 'user_list') {
	//$size = 1000;
	$start = $page*$size;
	$user_name = !empty($_GET['user_name']) ? ($_GET['user_name']) : '';
	$email = !empty($_GET['email']) ? ($_GET['email']) : '';
	$where = '';
	if($user_name)
	{
		$where = "where user_name like '%$user_name%'";
	}
	if($email)
	{
		$where = "where email like '%$email%'";
	}

    $user_list = $User->userList($start, $size, $where);
    foreach ($user_list as $key => $val) {
        $user_id = $val['user_id'];
        $val['order'] = $GLOBALS['db']->getOne("select count(*) from order_info where user_id=$user_id");
        $val['pay_order'] = $GLOBALS['db']->getOne("select count(*) from order_info where user_id=$user_id and pay_status=2");
        $val['info'] = $Main->json2Array($val['info']);
        $user_list[$key] = $val;
    }

    $sql = "select count(*) from users $where";
    $Main->setPage($sql, $size, 'admins.php?act=user_list&user_name='.$user_name.'&email='.$email);
    $sm->assign('user_list', $user_list);
}
elseif($act=='user_export')
{
	$table = "<table border=1><tr><th>用户ID</th><th>积分</th><th>用户名</th><th>性别</th><th>生日</th><th>姓名</th><th>邮箱</th><th>电话</th><th>微信</th><th>签到次数</th><th>机构</th></tr>";
	$user_list = $User->userList($start, 10000);

	 foreach ($user_list as $key => $val) {

		$user_id = $val['user_id'];
		$info = @$Main->json2Array($val['info']);

	 	@$u['name'] = @$info['name'];

	 	//$GLOBALS['db']->autoExecute('users',$u,'upate',"user_id=$user_id");
	 	//continue;
		$val['info'] = $Main->json2Array($val['info']);
		$u = 'u_'.$val['user_id'];
		$typeids = $Other->typeid($val['type_id']);
		$sex = $val['sex']==1 ? '男' : '女';
		$qd = $Main->counts("select count(*) from gold_log where buyer='login' and geter='$u'");
		$table.="<tr><td>".$val['user_id']."</td><td>".$val['gold']."</td><td>".$val['user_name']."</td><td>"
		.$sex."</td><td>".$val['birthday']."</td><td>".$val['info']['name']."</td><td>".$val['email']."</td><td>".$val['info']['tel']
		."</td><td>".$val['info']['weixin']."</td><td>".$qd."</td><td>".$typeids."</td></tr>";
        $user_list[$key] = $val;
	 }
	 $table .="</table>";

	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();
}
//删除用户
if ($act == 'user_del') {

  $user_id = !empty($_GET['user_id']) ? intval($_GET['user_id']) : 0;
  $sql = "delete from users where user_id=$user_id";

  if(mysql_query($sql))
  {
   $Main->msg("删除成功","admins.php?act=user_list");
  }
}
//用户信息
elseif ($act == 'user_info') {
    $user_id = !empty($_GET['user_id']) ? intval($_GET['user_id']) : 0;
    $user_info = $User->userInfo($user_id);
    $sm->assign('user_info', $user_info);
    $addr_list = $User->getAddrList($user_id);
    //print_r($addr_list);
    $sm->assign('addr_list', $addr_list);
    $where = " where user_id=$user_id ";
    $sql = "select count(*) from order_info $where";
    $order_list = $Order->orderList($start, $size, $where);
    //print_r($order_list);
    $Main->setPage($sql, $size, 'user_order', 'p');
    $sm->assign('order_list', $order_list);
}
//用户信息
elseif ($act == 'user_edit') {
	$user_id = !empty($_POST['user_id']) ? intval($_POST['user_id']) : 0;
	if($user_id)
	{
		if(empty($_POST['passwd']))
		{
			unset($_POST['passwd']);
		}
		else
		{
			$_POST['passwd'] = md5($_POST['passwd']);
		}
		if($GLOBALS['db']->autoExecute('users',$_POST,'update',"user_id=$user_id"))
		{
			$Main->msg("修改成功!","admins.php?act=user_list");
		}

	}

  die();
}
//用户评论
elseif ($act == 'user_review') {
	$type = !empty($_GET['type']) ? intval($_GET['type']) : 0;
    $where = "where type=$type";
    $reviews = $User->reviewList($start, $size, $where,true);
    $sql = "select count(*) from review $where";
    $Main->setPage($sql, $size, 'admins.php?act=user_review&type='.$type);

    $sm->assign('reviews', $reviews);
}
elseif($act=='user_review_export')
{
	$table = "<table border=1><tr><th>用户ID</th><th>用户名</th><th>姓名</th><th>邮箱</th><th>电话</th><th>商品ID</th><th>好评</th><th>内容</th></tr>";
	$type = !empty($_GET['type']) ? intval($_GET['type']) : 0;
    $where = "where type=$type";
    $count = $GLOBALS['db']->getOne("select count(*) from review $where");
    $reviews = $User->reviewList($start, $count, $where,true);

	 foreach ($reviews as $key => $val) {
		$user_id = $val['user_id'];


		$val['info'] = $val['user_info']['info'];

		$haop = '';
		if($val['score']==3)
			$haop = '好评';
		elseif($val['score']==2)
			$haop = '中评';
		elseif($val['score']==1)
			$haop = '差评';


		$table.="<tr><td>".$val['user_id']."</td><td>".$val['user_name']."</td><td>"
		.@$val['info']['name']."</td><td>".@$val['user_info']['email']."</td><td>".@$val['info']['tel']
		."</td><td>".$val['id']."</td><td>".$haop."</td><td>".$val['content']."</td></tr>";
        $user_list[$key] = $val;
	 }
	 $table .="</table>";
	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();
}
//删除评论
elseif($act=='user_review_del')
{
	$r_id = !empty($_GET['r_id']) ? intval($_GET['r_id']) : 0;
	$sql = "delete from review where r_id=$r_id";
	if(mysql_query($sql))
	{
		$Main->msg("删除成功!","",true);
	}
}
elseif ($act == 'login') {
    $sm->display('login.htm');
    exit();
}
//积分记录
elseif($act=='gold')
{
	$type = !empty($_GET['type']) ? intval($_GET['type']) : 1;
	$where  = " where type=$type order by add_time desc";
	$gold_log = $User->goldLog($start,$size,$where);
	$count = $Main->counts("select count(*) from gold_log  $where");
	$pager = $Main->pager($count,$size,"admins.php?act=gold&type=".$type);

	$sm->assign('pagers',	$pager);
	$sm->assign('gold_log',	$gold_log);

}
//删除积分记录
elseif($act=='gold_log_del')
{
	$log_id = !empty($_GET['log_id']) ? intval($_GET['log_id']) : 0;
	$sql = "delete from gold_log where log_id=$log_id";
	if(mysql_query($sql))
	{
		$Main->msg("操作成功!","",true);
	}

}
//金币日志明细
elseif($act=='gold_detail')
{
	$log_id = !empty($_GET['log_id']) ? intval($_GET['log_id']) : 0;
	$log = $User->goldLogDetail($log_id);
	//print_r($log);die();
	$sm->assign('log',$log);

}
//发送金币
elseif($act=='send_gold')
{

	$data = !empty($_POST['data']) ? $Main->json2Array(stripcslashes($_POST['data'])) : '' ;
	if(isset($data['cat_id']))
	{
		die($User->goldLogReturnCat($data));
	}
	else
	{
		die($User->goldLogReturnUser($data));
	}



}

//权限
elseif($act=='change_level')
{

	$data = !empty($_POST['data']) ? $Main->json2Array(stripcslashes($_POST['data'])) : '' ;
	$u['level'] = $data['level'];
	if(!empty($data['user_id']))
	{
		$GLOBALS['db']->autoExecute('users',$u,'update',"user_id=".$data['user_id']);
		die('1');
	}


}


?>
