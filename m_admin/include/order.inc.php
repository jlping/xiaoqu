<?php
//订单列表
if ($act == 'order_list') {
    $sta = !empty($_REQUEST['sta']) ? $_REQUEST['sta'] : '';
    $where = "";
    if ($sta) {
        $sp = preg_split("/\*/", $sta);
        if (count($sp) == 2) {
            $field = $sp[0];
            $value = $sp[1];
            $where = " where `$field`='$value' ";
        } else {
            $where = " where order_sn like '%$sta%' or address like '%$sta%'";
        }
    }
    $order_list = @$Order->orderList($start, $size, $where);

    $sm->assign('order_list', $order_list);
    $sm->assign('sta', $sta);
    $Main->setPage("select count(*) from order_info $where ", $size, 'admins.php?act=order_list');
}
elseif($act=='order_export')
{
	 $count = $Main->counts("select count(*) from order_info ");
	 $order_list = @$Order->orderList($start, $count, " where 1 ");

	$table = "<table border=1><tr><th>订单ID</th><th>购买者ID</th><th>购买者姓名</th><th>商品ID</th><th>商品名 </th><th>售价</th><th>时间</th><th>评论ID</th><th>评论内容</th><th>好评</th></tr>";

	 foreach($order_list as $key=>$val)
	 {
	 	$order_goods = @$val['order_goods'][0];

	 	 $review = $val['order_review'];

		$table .= "<tr><td>".$val['order_id']."</td><td>".$val['user_id']."</td><td>".
		$val['user']['info']['name']."</td><td>".$order_goods['goods_id']."</td><td>".
		$order_goods['goods_name']." </td><td>".$val['order_amount']."</td><td>"
		.$val['add_time']."</td><td>".@$review['r_id']."</td><td>".@$review['content']."</td><td>".@$review['score']."</td></tr>";

	 }
	$table.="</table>";
	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();
}


//orderByCatId
elseif($act=='orderByCatId')
{
	$cat_id = !empty($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;
	$goods_ids = $GLOBALS['db']->getAll("select goods_id from goods where cat_id=$cat_id");
	if(!$goods_ids)
	{
		$Main->msg('未查到数据,请返回!',"",true);

	}
	$ids = array();
	foreach($goods_ids as $key=>$val)
	{
		$ids[] = $val['goods_id'];
	}
	$join = join(",",$ids);
	$order_id = $GLOBALS['db']->getAll("select order_id from order_goods where goods_id in($join)");
	$order_ids = array();
	foreach($order_id as $key=>$val)
	{
		$order_ids[] = $val['order_id'];
	}
	$order_join = join(',',$order_ids);
	$where = "where order_id in($order_join)";
	$order_list = @$Order->orderList($start, $size, $where);
	$sm->assign('order_list', $order_list);
	$Main->setPage("select count(*) from order_info $where ", $size, 'admins.php?act=orderByCatId&cat_id='.$cat_id);

}
//orderByGoodsId
elseif($act=='orderByGoodsId')
{
	$goods_id = !empty($_GET['goods_id']) ? intval($_GET['goods_id']) : 0;

	$order_id = $GLOBALS['db']->getAll("select order_id from order_goods where goods_id=$goods_id");
   	if(!$order_id)
	{
		$Main->msg('未查到数据,请返回!',"",true);

	}
	$order_ids = array();
	foreach($order_id as $key=>$val)
	{
		$order_ids[] = $val['order_id'];
	}
	$order_join = join(',',$order_ids);
	$where = "where order_id in($order_join)";
	$order_list = @$Order->orderList($start, $size, $where);
	$sm->assign('order_list', $order_list);
	$Main->setPage("select count(*) from order_info $where ", $size, 'admins.php?act=orderByGoodsId&goods_id='.$goods_id);

}

?>