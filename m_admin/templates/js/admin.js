/*
	admin class
*/

function Admin(){
	
}

//状态提示 type--info,warning,error,success 
Admin.prototype.statusTips = function(type,msg){
	if(msg && type.length)
	{
		var msg = msg.join(" , ")
		var obj = $(".alert_"+type).html(msg).fadeIn()
	}
	else
	{
		 $(".status_tips").hide()
	}
	
}

//后台通用res
Admin.prototype.res = function(res){
	if(res==1)
	{
		var obj = $(".alert_success").html("操作成功").fadeIn();
		setTimeout("Main.locationHref()",1000);
	}
	else
	{
		var obj = $(".alert_error").html("操作失败:"+res).fadeIn();
	}
	
		
}
//新增邮费
Admin.prototype.shippingAdd = function(){
	var data = new Object
	data.name = $("#name").val()
	data.desc = $("#desc").val()
	data.base_fee  = $("#base_fee").val()
	data.free_money = $("#free_money").val()
	var msg = []
	if(chk.isEmpty(data.name))
	{
		msg[msg.length] = '名称不能为空';
	}
	if(chk.isEmpty(data.desc))
	{
		msg[msg.length] = '描述不能为空';
	}
	if(!chk.isNumber(data.base_fee))
	{
		msg[msg.length] = '运费必须是数字';
	}
	if(!chk.isNumber(data.free_money))
	{
		msg[msg.length] = '免邮额度须是数字';
	}
	if(msg.length)
	{
		this.statusTips('error',msg)
	}
	else
	{
		this.statusTips()
		var da = 'act=shipping_add&data='+$.toJSON(data)
		Main.AJAX("admins.php",da,this.res)	
	}
}
//删除运费
Admin.prototype.shippingDel = function(shipping_id){
	var yes = confirm("您确定要删除吗?","");
	if(shipping_id && yes)
	{
		var data = "act=shipping_del&shipping_id="+shipping_id
		Main.AJAX("admins.php",data,this.res)	
	}
}
//运费修改
Admin.prototype.shippingEdit = function(shipping_id){
	var id = shipping_id
	var data = new Object
	data.name = $("#name_"+id).val()
	data.desc = $("#desc_"+id).val()
	data.base_fee  = $("#base_fee_"+id).val()
	data.free_money = $("#free_money_"+id).val()
	
	var msg = []
	if(chk.isEmpty(data.name))
	{
		msg[msg.length] = '名称不能为空';
	}
	if(chk.isEmpty(data.desc))
	{
		msg[msg.length] = '描述不能为空';
	}
	if(!chk.isNumber(data.base_fee))
	{
		msg[msg.length] = '运费必须是数字';
	}
	if(!chk.isNumber(data.free_money))
	{
		msg[msg.length] = '免邮额度须是数字';
	}
	if(msg.length)
	{
		this.statusTips('error',msg)
	}
	else
	{
		this.statusTips()
		var da = 'act=shipping_edit&data='+$.toJSON(data)+'&shipping_id='+shipping_id
		Main.AJAX("admins.php",da,this.res)	
	}
}
//修改系统配置

Admin.prototype.saveSysConf = function(){
	var data = new Object
	data.shop_name = $("#shop_name").val()
	data.shop_title = $("#shop_title").val()
	data.shop_keywords = $("#shop_keywords").val()
	data.shop_desc = $("#shop_desc").val()
	data.thumb_size = $("#thumb_size").val()
	data.cache_time = $("#cache_time").val()
	var msg = new Array()
	if(chk.isEmpty(data.shop_name))
	{
		msg[msg.length]='网站名称不能为空'
	}
	if(chk.isEmpty(data.shop_title))
	{
		msg[msg.length]='网站标题不能为空'
	}
	if(chk.isEmpty(data.shop_keywords))
	{
		msg[msg.length]='网站关键词不能为空'
	}
	if(chk.isEmpty(data.shop_desc))
	{
		msg[msg.length]='网站关描述不能为空'
	}
	if(chk.isEmpty(data.thumb_size))
	{
		msg[msg.length]='缩略图尺寸不能为空'
	}
	else
	{
		var m = data.thumb_size.match(/^\d+\*\d+$/)
		if(!m)
		{
			msg[msg.length] = '缩略图必须以"250*250"的格式'	
		}	
	}
	if(chk.isEmpty(data.cache_time))
	{
		msg[msg.length]='缓存时间不能为空'
	}
	else if(!chk.isInteger(data.cache_time))
	{
			msg[msg.length] = '缓存时间必须是整数'	
	}
	
	if(msg.length)
	{
		this.statusTips('error',msg)
		return false
	}
	else
	{
		this.statusTips()	
		var da = "act=edit_config&data="+$.toJSON(data)
		Main.AJAX("admins.php",da,this.res)
	}
}
//增加config
Admin.prototype.configAdd = function(obj){
	var names = ['name','code','value_type','value'];
	var data = Main.dataValue(obj,names);
	if(data.name && data.code && data.value_type && data.value)
	{
		var da = 'act=add_config&'+Main.json2String(data);
		Main.AJAX("admins.php",da,this.res)	
	}
}
Admin.prototype.configEdit = function(obj){
	var names = ['name','code','value_type','value','c_id'];
	var data = Main.dataValue(obj,names);
	if(data.name && data.code && data.value_type && data.value && data.c_id)
	{
		var da = 'act=edit_config1&'+Main.json2String(data);
		Main.AJAX("admins.php",da)	
	}
	
}
Admin.prototype.configDel = function(c_id){
	if(c_id)
	{
		var da = 'act=del_config&c_id='+c_id;
		Main.AJAX("admins.php",da,this.res)	
	}
}

//新增支付方式
Admin.prototype.paymentAdd = function(){
	var data = new Object
	data.pay_name = $("#pay_name").val()
	data.pay_desc = $("#pay_desc").val()
	data.pay_code = $("#pay_code").val()
	data.account  = $("#account").val()
	data.currency = $("#currency").val()
	var msg = []
	if(chk.isEmpty(data.pay_name))
	{
		msg[msg.length] = '名称不能为空';
	}
	if(chk.isEmpty(data.pay_desc))
	{
		msg[msg.length] = '描述不能为空';
	}
	if(chk.isEmpty(data.pay_code))
	{
		msg[msg.length] = 'Code不能为空';
	}
	if(chk.isEmpty(data.account))
	{
		msg[msg.length] = '账号不能为空';
	}
	if(chk.isEmpty(data.currency))
	{
		msg[msg.length] = '货币不能为空';
	}
	if(msg.length)
	{
		this.statusTips('error',msg)
	}
	else
	{
		this.statusTips()
		var da = 'act=payment_add&data='+$.toJSON(data)
		Main.AJAX("admins.php",da,this.res)	
	}
}
//修改支付方式
Admin.prototype.paymentEdit = function(pay_id){
	var data = new Object
	data.pay_name = $("#pay_name"+pay_id).val()
	data.pay_desc = $("#pay_desc"+pay_id).val()
	data.pay_code = $("#pay_code"+pay_id).val()
	data.account  = $("#account"+pay_id).val()
	data.currency = $("#currency"+pay_id).val()
	var msg = []
	if(chk.isEmpty(data.pay_name))
	{
		msg[msg.length] = '名称不能为空';
	}
	if(chk.isEmpty(data.pay_desc))
	{
		msg[msg.length] = '描述不能为空';
	}
	if(chk.isEmpty(data.pay_code))
	{
		msg[msg.length] = 'Code不能为空';
	}
	if(chk.isEmpty(data.account))
	{
		msg[msg.length] = '账号不能为空';
	}
	if(chk.isEmpty(data.currency))
	{
		msg[msg.length] = '货币不能为空';
	}
	if(msg.length)
	{
		this.statusTips('error',msg)
	}
	else
	{
		this.statusTips()
		var da = 'act=payment_edit&data='+$.toJSON(data)+'&pay_id='+pay_id
		Main.AJAX("admins.php",da,this.res)	
	}
}
//删除支付
Admin.prototype.paymentDel = function(pay_id){
	var yes = confirm("您确定要删除吗?","");
	if(pay_id && yes)
	{
		var data = "act=payment_del&pay_id="+pay_id
		Main.AJAX("admins.php",data,this.res)	
	}
}

//邮件设置
Admin.prototype.emailSet = function(){
	var data = new Object;
	data.service_type = ipt.radioValue($("input[name=mail_service]"));
	data.smtp_server  = $("#smtp_server").val();
	data.smtp_port    = $("#smtp_port").val();
	data.account	  = $("#account").val();
	data.passwd  	  = $("#passwd").val();
	data.smtp_ssl  	  = $("input[name=smtp_ssl]:checked").val() || '';//ipt.radioValue($("input[name=smtp_ssl]"));
	
	var msg = [];
	if(chk.isEmpty(data.service_type))
	{
		msg[msg.length] = '服务器类型必须选择';
	}
	if(!chk.isHost(data.smtp_server))
	{
		msg[msg.length] = 'SMTP Server 格式错误';
	}
	if(!chk.isPort(data.smtp_port))
	{
		msg[msg.length] = 'SMTP 端口格式错误';
	}
	if(!chk.isEmail(data.account))
	{
		msg[msg.length] = '邮箱格式错误';
	}
	if(chk.isEmpty(data.passwd))
	{
		msg[msg.length] = '密码不得为空';
	}
	
	if(msg.length)
	{
		this.statusTips('error',msg);
	}
	else
	{
		this.statusTips();
		var da = 'act=email_set_edit&data='+$.toJSON(data);
		Main.AJAX("admins.php",da,this.res)	;
	}
	
}
//测试邮件
Admin.prototype.testMail = function(){
	var data = new Object;
	data.to = $("#test_account").val();
	data.body = $("#test_content").val();
	data.file = Main.getFiles($("#file_res"));
	
	
	var msg = [];
	if(!chk.isEmail(data.to))
	{
		msg[msg.length] = '邮箱格式错误';
	}
	if(msg.length)
	{
		this.statusTips('error',msg);
	}
	else
	{
		this.statusTips();
	
		var da = 'act=send_mail&data='+$.toJSON(data);
		Main.AJAX(sep+"/ajax.php",da,this.res);
	}
	
}
//取得邮件模板
Admin.prototype.getEmilTemp = function(code){
	var data = "act=get_email_template&code="+code;
	Main.AJAX(sep+"/ajax.php",data,this.getEmilTempRes);
	
}
Admin.prototype.getEmilTempRes = function(res){
	var res = eval("("+res+")");
	
	$("#tmp_code").val(res.code);
	$("#tmp_subject").val(res.subject);
	//$("#tmp_content").val(res.content);
	$("#t_id").val(res.t_id);
	CKEDITOR.instances.tmp_content.setData(res.content);

}
//修改邮件模板
Admin.prototype.setEmilTemp = function(){
	var data = new Object;
	data.code    =   $("#tmp_code").val();
	data.subject = $("#tmp_subject").val();
	data.content = CKEDITOR.instances.tmp_content.getData();

	
	data.t_id    = $("#t_id").val();
	var msg =[];
	if(chk.isEmpty(data.code))
	{
		msg[msg.length] = 'Code不能为空';
	}
	if(chk.isEmpty(data.subject))
	{
		msg[msg.length] = '主题不能为空';
	}
	if(chk.isEmpty(data.content))
	{
		msg[msg.length] = '内容不能为空';
	}
	
	if(msg.length)
	{
		this.statusTips('error',msg);
	}
	else
	{
		this.statusTips();
		if(data.t_id=='0')
		{
			var da = "act=add_email_template&"+Main.json2String(data);
		}
		else
		{
			var da = "act=set_email_template&"+Main.json2String(data);
		}
		Main.AJAX(sep+"/ajax.php",da,this.res);
	}
	
}
//删除邮件模板
Admin.prototype.delEmilTemp = function(){
	var t_id = $("#t_id").val();
	if(t_id!='0')
	{
		this.statusTips();	
		var data = "act=del_email_template&t_id="+t_id;
		Main.AJAX(sep+"/ajax.php",data,this.res);
	}
	else
	{
		this.statusTips('error',['请选择一个邮件模板']);	
	}
}
//增加优惠码
Admin.prototype.addDiscount = function(){
	var val = $("input[name=discount_val]").val();
	
	if(chk.isNumber(val))
	{
		this.statusTips();	
		var data = "act=set_discount&value="+val;
		Main.AJAX("admins.php",data,this.res);
	}
	else
	{
		this.statusTips('error',['请输入数字类型']);	
	}
}
//用户登录
Admin.prototype.login = function(obj){
	var obj = $(obj);
	var data = new Object;
	data.user_name = obj.find("input[name=user_name]").val();
	data.passwd = obj.find("input[name=passwd]").val();
	//data.m_key = obj.find("input[name=m_key]").val();
	if($.trim(data.user_name) && !chk.isEmpty(data.passwd))
	{
		var da = "act=logins&data="+$.toJSON(data);
		Main.AJAX("logins.php",da,this.loginRes);
	}
	
	
}
Admin.prototype.loginRes = function(res){
	if($.trim(res)=='1')
	{
		location.href='./'	
	}
}



Admin.prototype.articleCatUpdate = function(obj,cat_id){
	var names = ['cat_name','keywords','cat_desc','title','is_show'];
	var data = Main.dataValue(obj,names);
	
	
	if(data.cat_name && cat_id)
	{
		var da = "act=article_cat_update&data="+$.toJSON(data)+"&cat_id="+cat_id;
		Main.AJAX("admins.php",da,this.res);	
	}
}

//修改文章
Admin.prototype.articleEdit = function(obj){
	var names = ['title','keywords','description','is_open','author','cat_id'];
	var data = Main.dataValue(obj,names);
	

	if(data.title && data.cat_id)
	{
		$("form[name=article_form]").submit();	
	}
	else
	{
		alert("标题，分类 不能为空！");	
		return false;
	}
}
//订单搜索
Admin.prototype.ordrSearch = function(act){
	var act = act ? act :'order_list';
	var kw = $("input[name=kw]").val();
	if(kw)
	{
		location.href='admins.php?act='+act+'&sta='+kw;	
	}
}

//时间段内的订单
Admin.prototype.durOrder = function(){
	
	var start = $(".time1").val();	
	var end   = $(".time2").val();
	
	if(start && end)
	{
		var url = 'admins.php?act=main&start='+escape(start)+"&end="+escape(end);
		window.parent.location.href = url;
	}
}

var AD = new Admin()