<?php
	define('MANS_', true);
	require('../includes/init.php');
	if(!$User->isAdmin(@$_SESSION['admin']))
	{
		header("location:admins.php?act=login");
		exit();
	}
	else
	{
		header("location:admins.php?act=main");
		exit();
	}
?>