<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */

// $Id:$

define("MANS_", true);
require_once ("./config.php");
$sm->setCaching(0);


if (!$User->isAdmin(@$_SESSION['admin'])) {
    if ($_SERVER['QUERY_STRING'] != 'act=login') {
		if (!isDebugHost()) {
			die('It\'s Work!');	
		}
	}
}
$page_title = !empty($page_title) ? $page_title : 'Man0sions';
$sm->assign('page_title', $page_title);
if ($act == 'main') {
    $sta = !empty($_GET['sta']) ? $_GET['sta'] : 'mon';
    $start = !empty($_GET['start']) ? strtotime($_GET['start']) : '';
    $end = !empty($_GET['end']) ? strtotime($_GET['end']) : '';
	if($start>$end)
	{
		$Main->msg("开始日期不能大于结束日期！",$GLOBALS['ref']);
	}
    $days = $Main->getDurDate($sta);
    if ($start && $end) {
        $days['start'] = $start;
        $days['end'] = $end;
    }
    $line_chart = $Order->durOrder($days);
    $sm->assign('line_chart', $line_chart);
    $chart_title = $Main->dateFormat($days['start'], 'Y-m-d') . ' 到 ' . $Main->dateFormat($days['end'], 'Y-m-d') . " 的订单 ";
    $sm->assign('chart_title', $chart_title);
}

//系统管理
include_once("include/set_config.inc.php");
//订单管理
include_once("include/order.inc.php");
//优惠码
include_once("include/discount.inc.php");
//用户管理
include_once("include/user.inc.php");
//文章分类
include_once("include/article.inc.php");

//商品分类
include_once("include/goods.inc.php");
 $sm->assign('act',$act);
if ($no_style) {
    $sm->display('no_style.htm');
} else {
    $sm->display('main.htm', $cache_id);
}
?>

