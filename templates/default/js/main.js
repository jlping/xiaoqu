//共用方法
 /*
	全局color
*/
color = ['#e400ff','#db1530','#218573','#6953bc','#b8f560','#ee7031','#ffa200','#fff600','#84ff00','#0072ff','#0012ff','#7200ff','#ea00ff','#009AD9']
color2 = color

var t_path = 'templets/default'
var opacity = 0.96
//is ie 成功返回版本号

function isIe(){
	var browse = window.navigator.userAgent
	
	
	
	if(browse.search("IE")>0)
	{
		ie_pos = browse.search("IE")
		ver    = browse.substr(ie_pos+3,3)
		ver    = new Number(ver)
		return ver	
	}	
	else
	{
		return false	
	}
}

//ajax
function AJAX(url,data,response,obj,str){
	obj = obj ? obj : ''
	str = str ? str : ''
	
	
	$.ajax({
		'url':url,
		'data':data,
		'type':'post',
		'success':function(msg){
			if(response)
			{
				response(msg,obj,str)
			}
		}		
	})
	
}
//close widnow
function closeWindow(){
	
	window.close()	
}


//check empty
function checkEmpty(obj){
	
	if(obj)
	{
		return false	
	}
	else
	{
		return true	
	}
}

//存在于数组的字符串中,存在返回索引，不存在返回null

function inArrayString(str,arr){
	if(arr.constructor!=Array || str.constructor!=String)
	{
		return null		
	}
	
	for(i=0;i<arr.length;i++)
	{
		
		if(arr[i].search(str)>=0)
		{
			return i	
		}	
	}
	
	return null
}

//系统类
function Sys(){
	this.status  = $(".status")
	this.success = $(".success")
	this.error   =   $(".error")
	this.closes()
	this.w_width = screen.width
	this.w_height = screen.height
	this.v_code = false;
	
	
}
Sys.prototype.ajaxImg = function(type,top){
	if(type=='show')
	{
		var top = this.w_height/2.5+top
		$("#ajax_img").css({"top":top}).fadeIn()
	}
	else
	{
		$("#ajax_img").hide()	
	}
}
//提示信息
Sys.prototype.msg = function(msg,err){
	
	if(err)
	{
		this.error.fadeIn()
		this.error.find("font").html(msg)	
	}
	else
	{
		this.success.fadeIn()
		this.success.find("font").html(msg)
	}
}
Sys.prototype.closes = function(obj){
	var jq_obj = $("."+obj)
	jq_obj.hide()
}
Sys.prototype.mask = function(){
	var jq_obj = $("#mask")
	var d_height = $(document).height()
	var d_width  = $(document).width()
	jq_obj.css({"width":d_width,"height":d_height,'opacity':0.8})
	return jq_obj
}
Sys.prototype.dialog = function(obj){
	var obj = $(obj)
	this.mask().show()
	obj.dialog({"width":500,"height":300,
			buttons: {
				Cancel: function() {
					
				 }
			}
	})
}
//取得随机颜色
Sys.prototype.randColor = function(){
	
	var len = color2.length-1
	var rand = Math.round(Math.random()*len)
	return color2[rand]
}
//设置弹出框的大小和位置
Sys.prototype.popBoxSize = function(width,height){
	if(isIe() && isIe()<9)
	{
			
	}
	var p = this.popObj()
	var sub_top = height/2
	var sub_left = width/2
	p.frame.css({'width':width,'height':height})
	p.tb_window.css({'width':width,'height':height+27,'margin-top':-sub_top-27,'margin-left':-sub_left})
}
//取得弹出框obj
Sys.prototype.popObj = function(){
	var obj = new Object
	var parent = $(window.parent.document)
	var frame = parent.find("#TB_iframeContent")
	var TB_window = parent.find("#TB_window")
	var title = TB_window.find("#TB_ajaxWindowTitle").html()
	obj.parent = parent
	obj.frame = frame
	obj.tb_window = TB_window
	obj.title = title
	return obj
	
}
//parent 刷新
Sys.prototype.parentHref = function(url){
	
	var href = parent.location.href
	var url = url ? url : href
	
	window.parent.location.href = url
}
//搜索
Sys.prototype.searchs = function(obj,type){
	var kw = $(obj).find("input[name=kw]").val();
	var type = type ? type : 'video';
	if(chk.isEmpty(kw))
	{
		alert("请输入关键词!");
		return false;	
	}
	else
	{
		var url = "index.php?act=search&type="+type+"&kw="+kw;	
		location.href = url;
	}
	
}
//检查验证码
Sys.prototype.checkVcode = function(obj){
	var v_code = $(obj).find("input[name=v_code]").val();
	if(!chk.isEmpty(v_code))
	{
		var data = "act=check_vcode&v_code="+v_code;
		AJAX("ajax.php",data,this.checkVcodeRes);
	}
	else
	{
		alert("请输入验证码");
	}
}
Sys.prototype.checkVcodeRes = function(res){
	if(res=='1')
	{
		this.v_code = true;	
	}	
}
//denyIp
Sys.prototype.denyIp = function(ua){
	var v_code = $("input[name=v_code]").val();
	if(ua.length==10 && chk.isUserName(ua) && v_code)
	{
		var data = "act=clear_deny&ua="+ua+"&v_code="+v_code;
		AJAX("ajax.php",data,this.denyIpRes);
	}
	else
	{
		alert("验证码或传递的数据错误!");	
	}	
}
Sys.prototype.denyIpRes = function(res){
    if(res=='1')
	{
		location.href="index.php";
	}
	else
	{
		alert("验证码或传递的数据错误!");	
	}
}
//ckeditor config
Sys.prototype.ckeditor = function(obj,type){
	var type = type ? type :'basic';
	var height =$("textarea[name="+obj+"]").height();
	
	if(type=='basic')
	{
		CKEDITOR.replace( obj, {
		toolbarGroups: [
			{ name: 'document',	   groups: [ 'mode', 'document' ] },			
			{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'tools' },	
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
			{ name: 'links' },
			{ name: 'insert' },
			{ name: 'styles' },
			{ name: 'colors' }
			
		]
		,height:height});
	}
	else if(type=='simple')
	{
		
		CKEDITOR.replace( obj, {
		toolbar: [
		[ 'Image',  'Smiley' ] 
		]
		,height:height});
		
	}
	else
	{
		CKEDITOR.replace( obj,{height:height});	
	}
	
	
}
Sys.prototype.setCnTime = function(){
	$.datepicker.regional['zh-CN'] = {  
      clearText: '清除',  
      clearStatus: '清除已选日期',  
      closeText: '关闭',  
      closeStatus: '不改变当前选择',  
      prevText: '<上月',  
      prevStatus: '显示上月',  
      prevBigText: '<<',  
      prevBigStatus: '显示上一年',  
      nextText: '下月>',  
      nextStatus: '显示下月',  
      nextBigText: '>>',  
      nextBigStatus: '显示下一年',  
      currentText: '今天',  
      currentStatus: '显示本月',  
      monthNames: ['一月','二月','三月','四月','五月','六月', '七月','八月','九月','十月','十一月','十二月'],  
      monthNamesShort: ['一','二','三','四','五','六', '七','八','九','十','十一','十二'],  
      monthStatus: '选择月份',  
      yearStatus: '选择年份',  
      weekHeader: '周',  
      weekStatus: '年内周次',  
      dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],  
      dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],  
      dayNamesMin: ['日','一','二','三','四','五','六'],  
      dayStatus: '设置 DD 为一周起始',  
      dateStatus: '选择 m月 d日, DD',  
      dateFormat: 'yy-mm-dd',  
      firstDay: 1,  
      initStatus: '请选择日期',  
      isRTL: false  
    };  
	$.datepicker.setDefaults($.datepicker.regional['zh-CN']);	
}
Sys.prototype.json2String = function(json) {
	var data = [];
	for(var i in json)
	{
		data[data.length] = i+'='+encodeURIComponent(json[i]);	
	}
	return data.join("&");

}
//取得指定对象下的值
Sys.prototype.dataValue = function(obj,names){
	var data = new Object;
	var obj = $(obj);
	
	
	for(var i=0;i<names.length;i++)
	{
		var name = names[i];
		data[name] =  obj.find("*[name="+name+"]").val();
			
	}
	return data;
}
Sys.prototype.dateTime = function(str) {
   var data = new Object;
   var reg_exp = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
   var p = str.match(reg_exp);
  
   if(p)
   {
	 data.year = p[1];
	 data.mon  = p[2];
	 data.day  = p[3];
	 data.hour = p[4];
	 data.min   = p[5];
	 data.sec   = p[6];
	}
  return data;
} 

//打开弹出框
Sys.prototype.popupBox = function(url, title,inline) {
	var inline = inline ? inline : false;
	if(inline)
	{
		$(url).show();	
	}
    $.colorbox({
        href: url,
        title: title,
		inline:inline,
		onClosed:function(){
			$(url).hide();	
		}
    })
}
//设置倒计时
function RTime(date,obj){
	this.t_time = 0	;
	this.year = date.year;
	this.mon  = date.mon;
	this.day  = date.day;
	this.hour = date.hour;
	this.min  = date.min;
	this.sec  = date.sec;
	this.obj = obj;
	this.count = 0;
	this.setTimes();
}
//设置时间
RTime.prototype.setTimes = function(){
	var self = this;
	this.t_time = setTimeout(function(){self.setTimes()},1000);
	
	var r_time = this.showTime();
	var a_link = this.obj.parent("span").next("a");
	if(r_time)
	{
		this.obj.html(r_time);
		a_link.removeClass("hover");
	}
	else
	{
		a_link.addClass("hover").attr({"onclick":""});
		this.obj.removeClass("left_time");
	}
	
}
RTime.prototype.getTimes = function(){
	var r_time = this.showTime()
}
//取得剩余时间
RTime.prototype.showTime = function() {
	
	Today = new Date();
	var NowHour = Today.getHours();
	var NowMinute = Today.getMinutes();
	var NowMonth = Today.getMonth();
	var NowDate = Today.getDate();
	var NowYear = Today.getYear();
	var NowSecond = Today.getSeconds();
	if (NowYear < 2000) NowYear = 1900 + NowYear;
	Today = null;
	Hourleft = this.hour - NowHour ;
	Minuteleft = this.min - NowMinute ;
	Secondleft = this.sec - NowSecond ;
	Yearleft = this.year - NowYear ;
	Monthleft = this.mon - NowMonth - 1 ;
	Dateleft = this.day - NowDate;
	
	if (Secondleft < 0) {
		Secondleft = 60 + Secondleft;
		Minuteleft = Minuteleft - 1;
		
	}
	if (Minuteleft < 0) {
		Minuteleft = 60 + Minuteleft;
		Hourleft = Hourleft - 1;
		
	}
	if (Hourleft < 0) {
		Hourleft = 24 + Hourleft;
		Dateleft = Dateleft - 1;
	}
	
	if (Dateleft < 0) {
		Dateleft = 31 + Dateleft;
		Monthleft = Monthleft - 1;
		//Monthleft = Monthleft ? Monthleft+' 个月 ' : ''
	}
	if (Monthleft < 0) {
		Monthleft = 12 + Monthleft;
		Yearleft = Yearleft - 1;
		//Yearleft = Yearleft ? Yearleft+' 年 ' : ''
	}
	else
	{
		Dateleft += this.getMonthDays(NowYear,NowMonth) * Monthleft;	
	}
	
	if(Yearleft<0) 
	{
		//Temp =   0+'<small>天</small>' +0 + '<small>时</small>' + 0 + '<small>分</small>' + 0 + '<small>秒</small>'
		Temp = '';
	}
	else
	{
	//Temp = Yearleft + '年, ' + Monthleft + '月, ' + Dateleft + '天, ' + Hourleft + '小时, ' + Minuteleft + '分, ' + Secondleft + '秒'
	Temp ='<small>剩余</small> '+ Dateleft+'<small>天</small>' +Hourleft + '<small>时</small>' + Minuteleft + '<small>分</small>' + Secondleft + '<small>秒</small>';
	//Temp = '<small>'+Dateleft+'</small> : <small>' +Hourleft + '</small> : <small>' + Minuteleft + '</small> : <small>' + Secondleft + '</small>';
	}
	
	return Temp
	
}

//取得某月有几天
RTime.prototype.getMonthDays = function(year,month){
	 month = parseInt(month,10)+1;  
     var temp = new Date(year+"/"+month+"/0");  
     return temp.getDate(); 
}





function toggleObj(obj){
	obj = $(obj)
	obj.toggle()
}
function closeObj(obj){
	
	obj = $(obj)
	obj.hide()
}
function showObj(obj,chk){
	if(chk)
	{
		obj.show()	
	}
	else
	{
		obj.hide()
	}
}

function showIpInput(checked)
{
	if(checked)
	{
		$(".ip").show()	
	}
	else
	{
		$(".ip").hide()	
	}	
}
//通用RES
function RES(msg){
	if(msg)
	{
		location.href= location.href	
	}
}
//翻页
function pager(page){
	location.href=page
}
function setCookie(c_name,value,expiredays)
{
	var exdate=new Date()
	exdate.setDate(exdate.getDate()+expiredays)
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toGMTString()) +";path=/"
}

function getCookie(c_name)
{
	if (document.cookie.length>0)
	{ 
	c_start=document.cookie.indexOf(c_name + "=")
	if (c_start!=-1)
	{ 
	c_start=c_start + c_name.length+1 
	c_end=document.cookie.indexOf(";",c_start)
	if (c_end==-1) c_end=document.cookie.length
	return unescape(document.cookie.substring(c_start,c_end))
	} 
	}
	return ""
}

//图片上传
function upLoadInit(obj,url,res,img_id_upload,idx,mul,auto,text,num,types,size,btn){
		
		var text = text ? text : '选择文件'  //按钮文字
		var mul  = (mul!=undefined) ? mul : false        //上传多张
		var auto = (auto!=undefined) ? auto : true       //自动上传
		var res = (res!=undefined) ? res : queueData
		var num   = (num!=undefined) ? num : 10          //上传数量
		var types = (types!=undefined) ? types : '*.gif; *.jpg; *.png; *.pdf; *.doc; *.docx; *.xls; *.xlsx; *.mov; *.wmv; *.mp4'
		var size   = (size!=undefined) ? size : 10 
		var btn    = (btn!=undefined) ? btn : {'width':80,'height':25} 
		obj.uploadify({
    	'auto'     : auto,//关闭自动上传
    	'removeTimeout' : 1,//文件队列上传完成1秒后删除
        'swf'      : HOST+'/templets/images/uploadify.swf',
        'uploader' : url,
        'method'   : 'post',//方法，服务端可以用$_POST数组获取数据
        'buttonText' : text,//设置按钮文本
        'multi'    : mul,//允许同时上传多张图片
        'uploadLimit' : num,//一次最多只允许上传10张图片
        'fileTypeDesc' : '文件格式:',//只允许上传图像
        'fileTypeExts' : types,//限制允许上传的图片后缀
        'fileSizeLimit' : size+'MB',//限制上传的图片不得超过200KB
		'width':		 btn.width,
		'height':		 btn.height, 
		
        'onUploadSuccess' : function(file, data, response) {//每次成功上传后执行的回调函数，从服务端返回数据到前端
			
               img_id_upload[idx]=data;
			   if(mul)
               idx++;
        },
        'onQueueComplete' : res
        
    });
}
	
function upLoadInitRes(res){
	alert(img_id_upload)
}

//取得文件及名称json
function getFiles(obj){
	var obj = obj.find(".pdf")
	var data = new Object
	for(var i=0;i<obj.length;i++)
	{
		var ele = obj.eq(i)
		var res = new Object
		res.url = ele.find("a img").attr("name")
		res.name = ele.find(".file_name").html()
		res.name = res.name ? res.name : ''
		data[i] = res
	}
	if($.toJSON(data).length<3)
	{
	 data = ''
	}
	return data
}
//删除文件
function delFile(type,idx,quest,res){
	var url = url ? url  : 'index.php'
	var res = res ? res : ''
	var obj = $("."+type).find(".pdf_"+idx)
	var url = obj.find("a img").attr("name")
	
	var data = "act=del_file&url="+url
	obj.remove()
	
	AJAX('ajax.php',data)
}
//按钮提示
function upLoadButton(type,limit,size){
	var str = '类型:'+type+', 单个< '+size+' M 最多'+limit+' 个文件'
	return str
}


//获取用户名
function getUserName(){
	
	if(!s_user)
	{
		var ao_box = $(".ao_box")
		var left  = (screen.width - 330)/2
		var top = (screen.height - 290)/2
		ao_box.css({"left":left,"top":top}).show()
		ao_box.find(".u_btn").click(function(){
		s_user = ao_box.find(".uname").val()
			
		})
		
	}
	else
	{
		return s_user	
	}
	
}


//随机颜色
function randColor(){
	return '#'+('00000'+(Math.random()*0x1000000<<0).toString(16)).slice(-6);
}
var sys = new Sys()



/*
	取得input的值
*/
function InputValue(){
	
}
//text value
InputValue.prototype.textValue = function(obj){
	var obj = $(obj)
	var val = obj.val()
	return val
}
//text value
InputValue.prototype.radioValue = function(obj){
	var obj = $(obj)
	for(var i=0;i<obj.length;i++)
	{
		var sel = obj.eq(i)
		if(sel.attr("checked")=='checked')	
		{
			var val = sel.val()
			return val
		}
	}
	return ''
}

var ipt = new InputValue()


//赞
function Zan(){
	
}
Zan.prototype.addZan = function(id,key,act){
	var data = "act="+act+"&"+key+"="+id
	AJAX("ajax.php",data,this.addZanRes)
}
Zan.prototype.addZanRes = function(res){
		if(res=='0')
		{
			alert('您已经赞过了!')
		}
		else
		{
			var obj = $(".zan_"+res)
			var c = obj.find("strong")
			val = new Number(c.html())
			val = val+1
			c.html(val)
		}
}
//支援
Zan.prototype.zhiyuan = function(type,id){
	var v_code  = $("input[name=v_code]").val();
	if(chk.isEmpty(v_code))
	{
		alert("请输入验证码");
		return false	
	}
	if(type && id && v_code)
	{
		var data = "act=add_zhiyuan&type="+type+"&id="+id+"&v_code="+v_code;
		AJAX("ajax.php",data,this.zhiyuanRes);
	}
	
}
Zan.prototype.zhiyuanRes = function(res){
	if(res =='1')
	{
		var href = window.parent.location.href;
		sys.parentHref(href);
	}
	else
	{
		alert(res);	
	}	
}

var zan =new  Zan()


//留言类
function Message(){
	
}
Message.prototype.addMessage = function(){
	
	var data = new Object
	data.u_name = $(".msg_name").val()
	data.content = $(".msg_con").val()
	var msg = ''
	if(data.u_name=='')
	{
		msg += "请输入OA用户名或手机号!\n"
	}
	if(data.content=='')	
	{
		msg += "请输入内容！\n"
	}
	
	if(msg)
	{
		alert(msg)
		return false	
	}
	else
	{
		var da = "act=add_msg&data="+$.toJSON(data)
		AJAX("ajax.php",da,this.addMessageRes)	
	}
}

Message.prototype.addMessageRes = function (res){
	if(res=='1')
	{
		alert("感谢您的反馈!")
		window.parent.location.href = './'	
	}
}
var msg = new Message()

/*
	encode
*/
function Code() {

    this.REGX_HTML_ENCODE = /"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g;

    this.REGX_HTML_DECODE = /&\w+;|&#(\d+);/g;

    this.REGX_TRIM = /(^\s*)|(\s*$)/g;

    this.HTML_DECODE = {
        "&lt;": "<",
        "&gt;": ">",
        "&amp;": "&",
        "&nbsp;": " ",
        "&quot;": "\"",
        "&copy;": ""

        // Add more
    };

};
Code.prototype.encodeHtml = function(s) {
    s = (s != undefined) ? s: this.toString();
    var res =  (typeof s != "string") ? s: s.replace(this.REGX_HTML_ENCODE,
    function($0) {
        var c = $0.charCodeAt(0),
        r = ["&#"];
        c = (c == 0x20) ? 0xA0: c;
        r.push(c);
        r.push(";");
        return r.join("");
    });
	return escape(res);
};

Code.prototype.decodeHtml = function(s) {
    var HTML_DECODE = this.HTML_DECODE;

    s = (s != undefined) ? s: this.toString();
    return (typeof s != "string") ? s: s.replace(this.REGX_HTML_DECODE,
    function($0, $1) {
        var c = HTML_DECODE[$0];
        if (c == undefined) {
            // Maybe is Entity Number
            if (!isNaN($1)) {
                c = String.fromCharCode(($1 == 160) ? 32 : $1);
            } else {
                c = $0;
            }
        }
        return c;
    });
};
Code.prototype.trim = function(s) {
    s = (s != undefined) ? s: this.toString();
    return (typeof s != "string") ? s: s.replace(this.REGX_TRIM, "");
};
Code.prototype.hashCode = function() {
    var hash = this.__hash__,
    _char;
    if (hash == undefined || hash == 0) {
        hash = 0;
        for (var i = 0, len = this.length; i < len; i++) {
            _char = this.charCodeAt(i);
            hash = 31 * hash + _char;
            hash = hash & hash; // Convert to 32bit integer
        }
        hash = hash & 0x7fffffff;
    }
    this.__hash__ = hash;

    return this.__hash__;
};
var code = new Code();



