/*
	用户类
*/

function Users(){
	
}

//检查用户
Users.prototype.checkUser = function(){
		var user = new Object
		var img = getFiles($(".user_img"))
		user.nick_name = ipt.textValue("input[name=nick_name]")
		user.user_name = ipt.textValue("input[name=user_name]") //姓名
		
		user.tel = ipt.textValue("input[name=user_tel]")
		passwd = ipt.textValue("input[name=user_passwd]")
		user.typeid = ipt.textValue("select[name=typeid]")
		user.passwd = passwd ? passwd : ''
		var msg = ''
		if(user.nick_name=='')
		{
			msg+="请输入昵称\n"	
		}
		if(img=='')
		{
			msg+="请上传头像\n"	
		}
		if(user.user_name=='')
		{
			msg+="请输入姓名\n"	
		}
		if(user.typeid=='')
		{
			msg+="请选择机构\n"	
		}
		
		
		if(user.tel=='')
		{
			msg+="请输入电话\n"	
		}
		if(!sess_user && user.passwd=='')
		{
			msg+="请输入密码\n"	
		}
		if(msg)
		{
			alert(msg)
			return false	
		}
		else
		{
			user.avater = img[0].url
			return user	
		}
}
//用户注册
Users.prototype.register = function(){
	var user = this.checkUser()
	
	if(user)
	{
		var data = "act=user_adds&user="+$.toJSON(user)	
		AJAX("ajax.php",data,this.registerRes)
	}
}
//用户修改
Users.prototype.userEdit = function(){
	var user = this.checkUser()
	
	if(user)
	{
		user.u_id = ipt.textValue("input[name=u_id]")
		var data = "act=user_update&user="+$.toJSON(user)	
		AJAX("ajax.php",data,this.userEditRes)
	}
}
Users.prototype.userEditRes = function(res){
	if(res='1')
	{
		alert("修改成功!")
		sys.parentHref()	
	}
}
Users.prototype.registerRes = function(res){
	if(res!='0')
	{
		var p = sys.popObj()
		var res = eval("("+res+")")
		alert("注册成功!请保存您的登录信息:\n用户名:"+res.oa_name+"\n密  码:"+res.passwd+"")
		location.href='index.php?act=step&no_style=1&steps=1'	
		return false;
		if(p.title=='请先完善信息')
		{
			location.href="index.php?act=dasai_add"
		}
		else if(p.title=='创建用户')
		{
			location.href='index.php?act=step&no_style=1&steps=1'	
		}
		else
		{
			location.href = ''	
		}
	}

}
//用户登录
Users.prototype.userLogin = function(){
	var user = new Object
	user.user_name = ipt.textValue("input[name=login_user_name]")
	user.passwd = ipt.textValue("input[name=login_user_pass]")
	var msg = ''
	if(user.user_name=='')
	{
		msg+="请输入用户名\n"	
	}
	if(user.passwd=='')
	{
		//msg+="请输入密码\n"	
	}
	if(msg)
	{
		alert(msg)	
	}
	else
	{
		var data = "act=user_login&user="+$.toJSON(user)
		AJAX("ajax.php",data,this.userLoginRes)
	}
}
Users.prototype.userLoginRes = function(res){
	if(res=='0')
	{
		alert('用户名或密码错误!')	
	}
	else
	{
		alert("登陆成功!")
		location.href = './'
	}
}
//申请加入小组
Users.prototype.joinGroup = function(p_id,u_id){
	
	var data = "act=join_group&u_id="+u_id+'&p_id='+p_id
	AJAX("ajax.php",data,this.joinGroupRes)
	
}
Users.prototype.joinGroupRes = function(res){
	if(res=='1')
	{
		alert("申请已提交，等待组长审核!")	
	}
	else if(res=='2')
	{
		alert("您已经申请或已经是该组员!")		
	}
}
//审核会员
Users.prototype.groupStatus = function(g_id,status){
	var data = "act=group_status&g_id="+g_id+"&status="+status
	AJAX("ajax.php",data,this.groupStatusRes)
}
Users.prototype.groupStatusRes = function(res){
	location.href = location.href
}
//上传视频
Users.prototype.videoAdd = function(v_id){
	var data = new Object
	var file = getFiles($(".video"))
	var img  = getFiles($(".fmt"))
	data.title = ipt.textValue("input[name=v_title]")
	data.description = ipt.textValue("textarea[name=v_description]")
	data.status = ipt.radioValue("input[name=v_status]")
	data.p_id = ipt.textValue("input[name=p_id]")
	data.t_id = ipt.textValue("select[name=t_id]")
	data.v_id = v_id
	var msg = ''
	if(file=='')
	{
		msg+="请上传视频\n"	
	}
	
	if(img=='')
	{
		msg+="请上封面图\n"	
	}
	else
	{
		data.img = img[0].url	
	}
	
	
	if(data.title=='')
	{
		msg+="请输入标题\n"	
	}
	if(data.t_id=='')
	{
		msg+="请选择分类\n"	
	}
	if(data.description=='')
	{
		msg+="请输入描述\n"	
	}
	
	if(msg)
	{
		alert(msg)	
		return  false
	}
	else
	{
		
		sys.ajaxImg('show')
		
		data.url = file[0].url
		if(data.v_id=='0')
		{
			var da = "act=video_add&data="+$.toJSON(data)
		}
		else
		{
			var da = "act=video_edit&data="+$.toJSON(data)
		}
		AJAX("ajax.php",da,this.videoAddres)	
	}
	
}

Users.prototype.videoAddres = function(res){
	var res = new Number(res)
	
	if(res>0)
	{
		//location.href = ""
		if(res==2) 
		{
			alert("该小组已存在参赛视频！")
		}
		else
		{
			window.parent.location.href = window.parent.location.href	
		}
	}
	else
	{
		alert("操作失败!")	
	}
	sys.ajaxImg("hide")
}

//收藏小组
Users.prototype.collection = function(id,type){
	var data = "act=collection&type="+type+"&id="+id;	
	AJAX("ajax.php",data,this.collectionRes);
	
}
Users.prototype.collectionRes = function(res){
	if(res==1)
	{
		alert("收藏成功!");	
	}
	else
	{
		alert("收藏失败！\n请确保以下问题:\n1:您是否已经登陆，并加入到小组中？\n2:你已经收藏过了.");	
	}
}
var user = new Users()