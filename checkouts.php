<?php
define('MANS_', true);
require (dirname(__FILE__) . '/includes/init.php');
$sm->setCaching(0);
if ($act == 'cart') {
	$cart_goods = $Order->cartGoods();
	$sm->assign('cart_goods', $cart_goods);
	$rand_goods = $Goods->randGoods($cart_goods['cat_ids'], 12);
	$rand_goods = array_chunk($rand_goods, 4);

	$sm->assign('rand_goods', $rand_goods);
}
//checkout
elseif ($act == 'checkout') {

	//*取得购物车商品,并设置cookie
	$cart_goods = $Order->cartGoods(); // 取得商品列表，计算合计

	//print_r($cart_goods);die();
	if (empty ($cart_goods['goods_list'])) {
		$Main->msg('购物车中没有商品，请返回!', '', true);
	}
	$user_id = $_SESSION['user_id'];
	$user_info = $User->userInfo($user_id);
	$smarty->assign('user_info', $user_info);
	$smarty->assign('cart_goods', $cart_goods);

	//取得用户地址列表
	$addr_list = @ $User->getAddrList($_SESSION['user_id']);
	$sm->assign('addr_list', $addr_list);
	if ($_SESSION['user_id']) {
		$addr = @ $addr_list[0]['addr'];
	} else {
		$addr = @ $User->getAddr();

	}
	$ua = @ $_SESSION[USER_AGENT];

	$sm->assign('message', @ $ua['message']);
	$sm->assign('addr_str', $addr['addr_str']);
	$sm->assign('address', $addr['address']);

	//print_r($addr);
	//die();
	$selected['payment'] = isset ($GLOBALS['ua']['payment']['pay_id']) ? $GLOBALS['ua']['payment']['pay_id'] : 1;
	$selected['shipping'] = isset ($GLOBALS['ua']['shipping']['shipping_id']) ? $GLOBALS['ua']['shipping']['shipping_id'] : 1;
	$sm->assign('selected', $selected);

	$shipping_list = $Main->shippingList();
	//print_r($shipping_list);
	$shipping_default = $_SESSION[USER_AGENT]['shipping'] = isset ($_SESSION[USER_AGENT]['shipping']) ? $_SESSION[USER_AGENT]['shipping'] : @ $shipping_list[0];

	$sm->assign('shipping_list', $shipping_list);
	$sm->assign('shipping_default', $shipping_default);
	//取得payment
	$payment_list = $Main->paymentList();
	//print_r($payment_list);
	$payment_default = $_SESSION[USER_AGENT]['payment'] = isset ($_SESSION[USER_AGENT]['payment']) ? $_SESSION[USER_AGENT]['payment'] : @ $payment_list[0];
	$sm->assign('payment_list', $payment_list);
	$sm->assign('payment_default', $payment_default);
	//计算默认费用
	$grand = $Order->grandTotal($cart_goods, $shipping_default);
	//print_r($grand);die();
	$sm->assign('grand', $grand);

	/* 取得国家列表、商店所在国家、商店所在国家的省列表 */
	$smarty->assign('country_list', $Main->getRegion(0));
	$smarty->assign('province_list', $Main->getRegion(1));
}
//支付跳转
elseif ($act == 'redirect') {
	$no_style = 1;

	/*
		判断余额支付的状态
	*/
	$user_id = $_SESSION['user_id'];
	$key = md5($user_id);
	$pay_code = @ $_REQUEST['order_code'];

	if (($pay_code == $_SESSION['order_code'][$key])) {
		unset ($_SESSION['order_code'][$key]);
	} else {
		$Main->msg('非法操作!', './');
	}

	$cart_goods = $Order->cartGoods(); // 取得商品列表，计算合计
	if (empty ($cart_goods['goods_list'])) {
		$Main->msg('购物车中没有商品，请返回!', './');
	}

	if (!empty ($_SESSION[USER_AGENT]['discount'])) //使用验证码
		{
		$Main->applyDiscount();
	}

	$order = @ $Order->getOrder();

	if (!$order) {
		$Main->msg('Invalid order.', './');
	}

	/* 插入订单表 如果是不同的订单，才插入*/
	if (true) {
		$res = $Order->addOrder($order);
		$s_order['order_id'] = $res['order_id'];
		$s_order['order_sn'] = $res['order_sn'];
		$_SESSION[USER_AGENT]['order'] = $s_order;
	} else {
		$order_id = @ $_SESSION[USER_AGENT]['order']['order_id'];
		$Order->updateOrder($order, $order_id);

	}

	//对用户积分操作

	$order['order_sn'] = $_SESSION[USER_AGENT]['order']['order_sn'];
	$order['order_id'] = $_SESSION[USER_AGENT]['order']['order_id'];
	$Order->orderComplete($order['order_id']);
	//$Order->clearCart();

	/* 取得支付信息，生成支付代码 */
	/*
	if ($order['order_amount'] > 0) {

		$payment = $_SESSION[USER_AGENT]['payment'];
	    $pay_online = $Order->payCode($order);
	    $order['pay_desc'] = $payment['pay_desc'];
	    $smarty->assign('pay_online', $pay_online);

	}*/
	$smarty->assign('order', $order);
	$ua = $_SESSION[USER_AGENT];
	$addr = @ $Main->json2Array($ua['address']);
	$sm->assign('addr_str', @ $addr['addr_str']);
}
//支付验证
elseif ($act == 'payres') {
	$pay_code = !empty ($_GET['cd']) ? trim($_GET['cd']) : 'paypal';
	$pay_status = false;
	if (!empty ($_POST['mc_gross'])) //POST 验证
		{
		$order_id = $_POST['invoice'];

		if ($Order->payCheck($pay_code)) {
			$order_info = $Order->orderInfo($order_id);
			$addr = $order_info['address'];
			$tmp = $Main->mailTemplateOne('order_confirm');

			$data['subject'] = $tmp['subject'];
			$data['to'] = array (
				$addr['address']['email']
			);
			$com = array (
				$addr['address']['user_name'],
				$order_info['order_sn'],
				$Main->priceFormat($order_info['goods_amount']
			), $Main->priceFormat($order_info['shipping_fee']), $Main->priceFormat($order_info['discount']), $Main->priceFormat($order_info['order_amount']), $addr['addr_str'], $Order->orderUrl($order_id, $addr['address']['email']));
			$data['body'] = sprintf($tmp['content'], $com[0], $com[1], $com[2], $com[3], $com[4], $com[5], $com[6], $com[7]);

			$Order->orderComplete($order_id);
			echo $Main->sendMail($data);
		}
		die();
	}
	elseif (!empty ($_GET['o_id'])) {

		$order_id = !empty ($_GET['o_id']) ? trim($_GET['o_id']) : 0;

		$order_info = $Order->orderInfo($order_id);
		if ($order_info['user_id'] != $_SESSION['user_id']) {
			$Main->msg("您无权访问，请登录\n确认你购买过次商品?", "", true);
		}

		//$Order->canBePayres($order_info);

		if ($order_info) {
			$order_goods = $Order->orderGoods($order_id);

			$amount = $order_info['goods_amount'] + $order_info['shipping_fee'] - $order_info['discount'];
			if (($amount == @ $_GET['amt']) && (@ $_GET['st'] == 'Completed')) {
				$pay_status = true;
			}
			if ($order_info['pay_status'] == 2) {
				$pay_status = true;
			}
			$sm->assign('order', $order_info);
			$sm->assign('order_goods', $order_goods);
			/*
			$pay_online = $Order->payCode($order_info);
			$smarty->assign('pay_online', $pay_online);
			*/
		}
		$goods = $order_goods['0'];
		$url = '<a href="http://dq.cmbc.com.cn/xiaoqu/goods.php?id=' . $goods['goods_id'] . '">' . $goods['goods_name'] . '</a>';
		$user = $User->goodsAdmin($goods['goods_id']);


		$tmp = $Main->mailTemplateOne('action');
		$data['subject'] = $tmp['subject'];
		$data['to'] = array (
			$user['email']
		);
		$com = array (
			"您有新的订单,订单金额为：" . round($order_info['order_amount']
		) . "<br/>" .
		"案例名:" . $url . "<br/>" .
		"请及时处理!<br/>" .
		"" . $Main->dateFormat(time()),);
		$data['body'] = sprintf($tmp['content'], $com[0]);

		($Main->sendMail($data));

		//print_r($order_info);die();
		$sm->assign('pay_status', $pay_status);

	}
}
//设置查看邮箱
elseif ($act == 'set_payres') {
	$email = !empty ($_POST['o_idemail']) ? trim($_POST['email']) : '';
	$order_id = !empty ($_GET['o_id']) ? trim($_GET['o_id']) : 0;
}

$position = $Main->position(array (), CHECKOUT);
$Main->assigns($position);
$free_gift = $Goods->freeGift();
$sm->assign('free_gift', $free_gift);
if ($no_style) {
	$sm->display('no_style.htm', $cache_id);
} else {
	$sm->display("checkouts.htm", $cache_id);
}
?>

