<?php

define('MANS_', true);

require (dirname(__FILE__) . '/includes/init.php');
$sm->setCaching(0);
$position = $Main->position(array(),$act);

$rand_goods = $Goods->randGoods(array(),12);
$rand_goods = array_chunk($rand_goods,4);
$sm->assign('rand_goods',	$rand_goods);
//print_r($GLOBALS['categories']);
$no_index = false;

if ($act == 'error')
{
	$smarty->assign('page_title',	'Page Not Found ');

}
elseif ($act == 'popular_searches')
{
	if(!$sm->isCached('main.htm',$cache_id))
	{
		$cat_list = $Main->hotKeywords($start,$size);
		$sm->assign('cat_list',	$cat_list);
		$count = $Main->counts("select count(*) from keywords");

		$pager = $Main->Pager($count,$size,'popular_searches','p');
		$sm->assign('pagers',	$pager);
	}

}


//邮件查询
elseif($act=='track_order')
{
	$no_index = true;
	$no_style=1;
}
//文章列表
elseif($act=='article_cat')
{
	$cat_id = isset($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
	$size = !empty($_REQUEST['size'])? intval($_REQUEST['size']) : 24;
	$start = $page*$size;
	if(!$sm->isCached('main.htm',$cache_id))
	{
		$cat =  $Cate->artCatInfo($cat_id);
		if($cat)
		{
			$cat_pg = ART_CAT.'-'.$cat_id;
			$where = " where cat_id=$cat_id and is_open=1 ";
			$sm->assign('cat',	$cat);
			$count = $Main->counts("select count(*) from article $where");

			$pager = $Main->Pager($count,$size,$cat_pg,$cat['cat_name']);
			if(!preg_match("#".$cat['match']."#",$req))
			{
				$Main->url301($cat['url']);
			}

			$cat_list = $Cate->catArticles($start,$size,$where);
			$sm->assign('pagers',	$pager);
			$sm->assign('cat',	$cat);
			$sm->assign('cat_list',	$cat_list);
			$position = $Main->position($cat,ART_CAT);
			$Main->assigns($position);
		}
	}


}
//文章内页
elseif($act=='article')
{

	$a_id = !empty($_REQUEST['a_id']) ? intval($_REQUEST['a_id']) : 0;
	if(!$sm->isCached('main.htm',$cache_id))
	{
		$article = $Cate->article($a_id);
		if(!$article)
		{
			$Main->msg(" Error !","./");
		}
		$this_url = $article['url'];
		//$Main->url301($this_url);

		$sm->assign('article',	$article);
		//print_r($article);die();
		//$Main->cronArticle();
		$position = $Main->position($article,ART);
	}

}

//用户登录
elseif($act=='login')
{
	$no_index = true;
}
//用户中心
elseif($act=='user')
{

	$no_index = true;
	$user_act = !empty($_REQUEST['user_act']) ? $_REQUEST['user_act'] : 'default';
	$user_id  =  $_SESSION['user_id'];

	$sm->setCaching(0);
		$u_id = !empty($_REQUEST['user_id']) ? intval($_REQUEST['user_id']) : 0;
		if($user_act=='register')
		{


			if(!empty($_POST['user_act']))
			{
					$data =	$Main->getPostData($_POST,array('user_act','passwd2','user_id'));

					$info['name'] = $_POST['name'];
					$info['nick_name'] = $_POST['nick_name'];

					$info['tel'] = $_POST['tel'];
					$info['weixin'] = $_POST['weixin'];
					$data['info'] = $info;
					$data['passwd'] = md5($data['passwd']);

					$data['info'] = $Main->array2Json($data['info']);

					if(!empty($_POST['user_id']))
					{
						if($GLOBALS['db']->autoExecute('users',$data,'update',"  user_id=".$_POST['user_id']))
						{
							$User->logout();
							if(@$_GET['fr'])
								$Main->msg("修改成功,请重新登录",@$_GET['fr']);
							$Main->msg("修改成功,请重新登录","./");
						}
					}
					else
					{
						$data['add_time'] = time();
						if($GLOBALS['db']->autoExecute('users',$data))
						{
							$user_id = mysql_insert_id();
							$user_info = $s = $User->userInfo($user_id);
							$User->userSession($s);

							$Main->msg("注册成功!","main.php?act=user_valid&no_style=$no_style&api=$api");

						}
					}
				}
			else
			{
					$u_info = @$User->userInfo($u_id);

					$sm->assign('user_info',	$u_info);
					$typeids = $Other->typeids();
					$sm->assign('typeids',	$typeids);
			}

		}




   if($user_id)
	{

		$user_info = $User->userInfo($user_id);

		if(!$user_info['status'])
			$Main->msg('您还未通过审核，请验证工作证的照片!','main.php?act=user_valid');

		$User->userSession($user_info);

		//创建小店
		$c_id = !empty($_REQUEST['c_id']) ? intval($_REQUEST['c_id']) : 0;
		if($user_act=='steps_a')
		{


			if(!empty($_POST))
			{

				$data = $Main->getPostData($_POST,array('cat_id'));

				$data['mag_id'] = $user_id;
				//修改小店
				if(!empty($_POST['c_id']))
				{
					unset($data['mag_id']);
					if($GLOBALS['db']->autoExecute('category',$data,'update',"cat_id=$c_id"))
					{
						$Main->msg("","main.php?act=user&user_act=steps_b&c_id=$c_id");
					}
				}
				//新增小店
				else
				{
					if($user_info['cat_id'])
						$Main->msg('您已经创建或加入了小店，请回到自己的小店!','category.php?cat_id='.$user_info['cat_id']);

					$data['add_time'] = time();
					if($GLOBALS['db']->autoExecute('category',$data))
					{
						$c_id = mysql_insert_id();
						$u['cat_id'] = $c_id;
						$u['mag'] =1;
						$GLOBALS['db']->autoExecute('users',$u,'update',"user_id=$user_id");
						$Main->msg("","main.php?act=user&user_act=steps_b&c_id=$c_id");
					}
				}
			}
			if(!empty($_GET['c_id']))
			{
				$c = $Cate->catInfo($c_id);
				$c['imgs'] = preg_split("/,/",$c['img']);

				$sm->assign('cat',	$c);
			}
		}

		elseif($user_act=='steps_b')
		{

			if(!empty($_POST))
			{

				$data = $Main->getPostData($_POST,array('user_act','member','mag','mag_name'));

				$Other->setUserMag($data,$c_id);

				if($GLOBALS['db']->autoExecute('category',$data,'update',"cat_id=$c_id"))
				{
					$u = @$_POST['update'];
					if($u)
					{
						$Main->msg("","category.php?cat_id=$c_id");
					}
					else
					{
						$Main->msg("","main.php?act=user&user_act=steps_c&c_id=$c_id");
					}
				}

			}
			else
			{
				$c = $Cate->catInfo($c_id);

				$c['memberss'] = @$Main->json2Array($c['members']);
				//$c = $Other->xqCatInfo($c);

				$mag = $User->userInfo($c['mag_id']);

				$sm->assign('cat',	$c);
				$sm->assign('mag',	$mag);
			}
		}
		elseif($user_act=='steps_c')
		{
			$c = $Cate->catInfo($c_id);
			$c = $Other->xqCatInfo($c);
			$sm->assign('cat',	$c);

			//店铺注册完成+分

			$return1['return'] = $members_return = $GLOBALS['gold']['gold']['category']['member']*count($c['members']);
			$return1['comment'] = "添加用户".count($c['members'])."个 系统分配 ".$members_return." 青币 操作人:system";
			$return1['cat_id'] = $c['cat_id'];
			$User->goldLogReturnCat($return1);
			$return2['return'] = $img_return = $GLOBALS['gold']['gold']['category']['img']*count($c['img']);

			$return2['comment'] = "添加图片".count($c['img'])."张 系统分配 ".$img_return." 青币 操作人:system";
			$return2['cat_id'] = $c['cat_id'];
			$User->goldLogReturnCat($return2);

			//店铺注册完成，记录动作


			$actions = $GLOBALS['gold']['actions'];
			$a = '<a href="'.$c['url'].'">'.$c['cat_name'].'</a>';
			$data['type']=2;
			$data['content'] = sprintf($actions['cat_add'],$GLOBALS['counts']['cat_count'],$a,$Main->dateFormat(time()));
			$data['add_time'] = time();
			$data['cat_id'] = $c['cat_id'];
			$data['user_id'] = $user_id;
			$GLOBALS['db']->autoExecute('cat_log',$data);

		}


		elseif($user_act=='user_order')
		{
			$where = " where user_id=$user_id ";
			$sql = "select count(*) from order_info $where";
			$order_list = $Order->orderList($start,$size,$where);

			$Main->setPage($sql,$size,'main.php?act=user&user_act=user_order');

			$sm->assign('order_list',	$order_list);
		}
		elseif($user_act=='user_address')
		{
			$addr_list = $User->getAddrList($user_id);
			$sm->assign('addr_list', $addr_list);
			$smarty->assign('country_list', $Main->getRegion(0));
			$smarty->assign('province_list', $Main->getRegion(1));
		}
		elseif($user_act=='user_review')
		{
			$where = "where user_id=$user_id";
			$reviews = $User->reviewList($start,$size,$where,false,true);
			$sql = "select count(*) from review $where";
			$Main->setPage($sql,$size,'user-review','p');

			$sm->assign('reviews',	$reviews);
		}
		//我的收藏
		elseif($user_act=="wishlist")
		{
			$type = !empty($_GET['type']) ? intval($_GET['type']) : 1;
			$where = "where user_id=$user_id  and type=$type";
			$wishlist = $User->wishlist($start,$size,$where);
			$sql = "select count(*) from wishlist $where";
			$Main->setPage($sql,$size,'main.php?act=user&user_act=wishlist');

			$sm->assign('wishlist',	$wishlist);
			$sm->assign('type',	$type);
			//print_r($wishlist);die();

		}
		//我的收藏
		elseif($user_act=="wishlistdel")
		{

			$id = intval($_GET['id']);
			$sql = "delete from wishlist where wishlist_id=$id";
			if(mysql_query($sql))
				$Main->msg("删除成功",'',true);
			//print_r($wishlist);die();

		}

		$sm->assign('user_act',	$user_act);
		$sm->assign('user_info',	$user_info);
		$sm->assign('c_id',	$c_id);
	}

	$sm->assign('user_act',	$user_act);
}
//增加收藏
elseif($act=='addwish')
{
	$user_id  =  $_SESSION['user_id'];
	$id = intval($_GET['id']);
	$type = intval($_GET['type']);
	if(!$user_id)
		$Main->msg("请先登录","main.php?act=user_login");
	$w['user_id'] = $user_id;
	$w['id'] = $id;
	$w['type'] = $type;
	$w['add_time'] = time();
	if($GLOBALS['db']->autoExecute('wishlist',$w))
	{
		$Main->msg("已关注!","",true);
	}

}
//user_valid
elseif($act=='user_valid_pic')
{
	$user_id  =  intval($_SESSION['user_id']);
	$_POST['status'] = 1;
	if($user_id)
	{
		if($GLOBALS['db']->autoExecute('users',$_POST,'update',"user_id=$user_id"))
		{
			$Main->msg("","main.php?act=user_valid_gold&no_style=$no_style&api=$api");
		}
	}

}
elseif($act=='user_valid_oa')
{
	$user_id = $_SESSION['user_id'];
	$oa_name = @$_POST['user_name'];
	$passwd = @$_POST['passwd'];
	$res = false;
	if($oa_name && $passwd)
	{
		$url = "http://cmbcepqas1.cmbc.com.cn:50000/ume/isUserLegal.action?userId=$oa_name&passWord=$passwd";
		$f = @file_get_contents($url);
		if(trim($f)=='true')
		{
			$data['status']=1;
			if($GLOBALS['db']->autoExecute('users',$data,'update',"user_id=$user_id"))
			{
				$res = true;
			}
		}

	}
	if($res)
	{
		$Main->msg("","main.php?act=user_valid_gold");
	}
	$Main->msg("验证失败，请重新输入","main.php?act=user_valid");


}
//验证完成加分
elseif($act=='user_valid_gold')
{

	$user_id = $_SESSION['user_id'];
	$user_info = $User->userInfo($user_id);
	$User->userSession($user_info);
	//注册完成+分
	$return['return'] = $user_return = $GLOBALS['gold']['gold']['register'];
	$return['comment'] = '注册完成，系统返回'.($user_return).' 青币 给 '.$user_info['user_name'].'  操作人:system';
	$return['user_id'] = $user_id;
	$User->goldLogReturnUser($return);
	if($api &&  !empty($_SESSION['from']))
		$Main->msg("",$_SESSION['from']);
	else
		$Main->msg("","main.php?act=user&user_act=step");
}

//sitemap
elseif($act=='sitemap')
{
	$Main->sitemap();
	die();
}
elseif($act=="rrs")
{

	$Main->rrs();
	die();
}
elseif($act=='links')
{
	$no_index = true;
}
elseif($act=='back_up')
{
	if(@strtolower($_GET['shop_name'])==strtolower($GLOBALS['_CFG']['shop_name']))
	{
		$Main->backupSql();
	}
	$Main->zip();
}
//清除缓存文件
elseif($act=='clear_cache')
{
	$file_name = array(
		$Main->cacheName('order10'),
		$Main->cacheName('goods10'),
		$Main->cacheName('news'),
		$Main->cacheName('recs'),
		$Main->cacheName('categories'),
		$Main->cacheName('keys')
	);
	foreach($file_name as $val)
	{
		if(file_exists($val))
		{
			unset($val);
		}
	}
	$Main->msg("操作成功!","index.php");
}
//新品
elseif($act=='new_arrival')
{
	$size = 8;
	$where = " where is_delete=0 order by goods_id desc ";
	$goods_list = $Goods->goodsList($page*$size,$size,$where);
	$sm->assign('goods_list',	$goods_list);

	$pager = $Main->Pager($size*3,$size,'New-Arrivals','p');
	$sm->assign("pagers",	$pager);
}
//注销
elseif($act=='log_out')
{
	if($User->logOut())
	{
		$Main->msg("安全注销！","index.php");
	}

}
elseif($act=='goods_edit')
{


	$cat_id = !empty($_REQUEST['c_id']) ? intval($_REQUEST['c_id']) : 0;
	$goods_id = !empty($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	$cat_type = !empty($_REQUEST['cat_type']) ? intval($_REQUEST['cat_type']) : 0;
	if($goods_id)
	{

		$g = $Goods->goodsInfo($goods_id);
		$cat_id = $g['cat_id'];
		if(!$User->inCat(@$g['cat_id']))
		{
			$Main->msg("您无权操作!","index.php");
		}
		if(!$cat_type)
			$cat_type = $g['cat_type'];
	}





	if(!empty($_POST['goods_name']))
	{


		if($cat_type==0)  //区分小区精益 关键词，买点
		{
			$_POST['keywords'] = $Other->goodsKeywordsMaiDian(1,'keywords');
			$_POST['descs'] = $Other->goodsKeywordsMaiDian(2,'descs');
		}
		else
		{
			$_POST['keywords'] = $Other->goodsKeywordsMaiDian(3,'keywords');
			$_POST['descs'] = $Other->goodsKeywordsMaiDian(4,'descs');
		}


		if(!empty($_POST['goods_id']))
		{

			$data = $Main->getPostData($_POST,array('goods_id','cat_id'));

			if($GLOBALS['db']->autoExecute('goods',$data,'update',' goods_id='.$goods_id))
			{
				$gallery = !empty($_POST['gallery']) ? preg_split("/,/",$_POST['gallery']) : array();
				$Goods->setGallery($goods_id,$gallery);
				$Main->msg("修改成功","goods.php?id=$goods_id");
			}
		}
		else
		{

			$_POST['add_time'] = time();

			if($GLOBALS['db']->autoExecute('goods',$_POST))
			{
				$goods_id = mysql_insert_id();
				$gallery = !empty($_POST['gallery']) ? preg_split("/,/",$_POST['gallery']) : array();
				$Goods->setGallery($goods_id,$gallery);

				//商品添加完成，记录动作
				$g_url = 'goods.php?id='.$goods_id;
				$c = $Cate->catInfo($_POST['cat_id']);

				$actions = $GLOBALS['gold']['actions'];
				$a = '<a href="'.$c['url'].'">'.$c['cat_name'].'</a>';
				$g = '<a href="'.$g_url.'">'.$_POST['goods_name'].'</a>';
				if($cat_id==JF_MALL_ID)
				{
					$str = $actions['jf_goods_add'];
					$g = $_POST['goods_name'];
				}
				else
				{
					$str = $actions['goods_add'];
					$g = '<a href="'.$g_url.'">'.$_POST['goods_name'].'</a>';
				}
				$data['type']=2;
				$data['content'] = sprintf($str,$a,$Main->dateFormat(time()),$g,$_POST['shop_price']);
				$data['add_time'] = time();
				$data['cat_id'] = $_POST['cat_id'];
				$data['user_id'] = $_SESSION['user_id'];
				$GLOBALS['db']->autoExecute('cat_log',$data);

				$Main->msg("添加成功","goods.php?id=$goods_id");
			}
		}
	}
	elseif($goods_id)
	{
		$goods = $Goods->goodsInfo($goods_id);
		$goods = $Other->xqGoods($goods);


		$sm->assign('goods',	$goods);

	}

	if($cat_type==0)
	{
		$engine = 'engine<=6 and engine>0';
	}
	else
	{
		$engine = 'engine>=100 and type=3';
	}

	$keywords = $Other->keywords(0,100,"where $engine order by engine,k_kd asc");
	$kwd = array();
	foreach($keywords as $key=>$val)
	{
			$engine = $val['engine'];
			$kwd[$engine][] = $val;
	}
	$url = preg_replace("#&cat_type=\d#","",$_SERVER['REQUEST_URI']);

	//print_r($kwd);die();
	$sm->assign('kwd',	$kwd);
	$sm->assign('c_id',$cat_id);
	$sm->assign('url',$url);
	$sm->assign('cat_type',$cat_type);
}
//文件下载
elseif($act=='download')
{

	$goods_id = !empty($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;
	$order_id = !empty($_REQUEST['o_id']) ? intval($_REQUEST['o_id']) : 0;
	$order_info = $Order->orderInfo($order_id);

	if(!(($order_info['user_id']==$_SESSION['user_id']) && ($order_info['pay_status']==2)) )
	{
		$Main->msg("您无权访问，请登录,确认你购买过此商品?","./");
	}
	$sql = "update goods set download=download+1 where goods_id=$goods_id";
	mysql_query($sql);

	$goods = $Goods->goodsInfo($goods_id);
	$goods = $Other->xqGoods($goods);
	$goods_briefs = $goods['goods_briefs'];
	$urls = array();

	foreach($goods_briefs as $key=>$val)
	{
		$urls[] = $val['url'];
	 // echo $val['url'].'=='.file_exists($val['url']);
		$info = pathinfo($val['url']);
		$names[] = $info['basename'];
		copy($val['url'],ROOT_PATH.'temp/cache/'.$info['basename']);
	}
	$new_name = 'temp/cache/'.date('Ymdhis').'.zip';
	$new_path = ROOT_PATH.$new_name;
    chdir(ROOT_PATH.'temp/cache/');



	if($Main->zip(join($names,','),$new_path))
	{
		//$Main->unlinks($urls);
		$Main->msg("",$new_name);
	}


	die();
}
//店铺日志
elseif($act=='cat_log')
{

	$cat_id = !empty($_REQUEST['cat_id']) ? intval($_REQUEST['cat_id']) : 0;
	$c = $cat_id ? "l.cat_id=$cat_id and" : '';
	$log = $Other->getCatLog($start,$size," where   $c l.r_id=0 order by l.add_time desc ");
	$sm->assign('logs',	$log);
	//print_r($log);die();
	$c1 = $cat_id ? "cat_id=$cat_id and" : '';
	$count = $Main->counts("select count(*) from cat_log where $c1 r_id=0");
	$pager = $Main->pager($count,$size,"main.php?act=cat_log&no_style=$no_style&cat_id=".$cat_id);
	$sm->assign('pagers',	$pager);
}
//积分管理
elseif($act=='gold')
{
	$type = !empty($_GET['type']) ? $_GET['type'] : 'user';
	//个人

	//店铺

	if($type=='cat')
	{
		$cat_id = !empty($_REQUEST['cat_id'])? intval($_REQUEST['cat_id']) :0;
		$cat = $Cate->catInfo($cat_id);
		$cat = $Other->xqCatInfo($cat);
		$g = 'c_'.$cat_id;
		//print_r($cat);die();
		$sm->assign('cat',	$cat);
		$c = '&cat_id='.$cat_id;
	}
	elseif($type=='user')
	{
		$user_id = $_SESSION['user_id'];
		$user_info = $User->userInfo($user_id);

		if(!$user_info['status'])
			$Main->msg('您还未通过审核，请验证工作证的照片!','main.php?act=user_valid');
		$g = 'u_'.$user_id;
		$sm->assign('user_info',	$user_info);
		$c = '';

	}

	$gold_log = $User->goldLog($start,$size," where buyer='$g' or geter='$g' order by add_time desc");

	$count = $Main->counts("select count(*) from gold_log  where buyer='$g' or geter='$g'");
	$pager = $Main->pager($count,$size,"main.php?act=gold&type=".$type.$c);
	$sm->assign('pagers',	$pager);
	$sm->assign('gold_log',	$gold_log);
	$sm->assign('g',	$g);
	$sm->assign('type',	$type);
}
//订单评价
elseif($act=='order_review')
{
	$order_id = !empty($_GET['o_id']) ? intval($_GET['o_id']) : 0;
	$goods_id = !empty($_GET['goods_id']) ? intval($_GET['goods_id']) : 0;
	$r_id = !empty($_GET['r_id']) ? intval($_GET['r_id']) : 0;
	if($r_id)
	{
		$review = $GLOBALS['User']->reviewList(0,1," where type=3 and r_id=$r_id");
		$review = $review[0];
	}
	$sm->assign('review',	@$review);
	$sm->assign('id',	$goods_id);
	$sm->assign('order_id',	$order_id);
}
elseif($act=='order_review_list')
{
	$size=5;
	$start = $page*$size;
	$no_style=true;
	$goods_id = !empty($_GET['goods_id']) ? intval($_GET['goods_id']) : 0;
	$score = !empty($_GET['score']) ? intval($_GET['score']) : 1;
	$goods = $Goods->goodsInfo($goods_id);
	//print_r($goods);die();
	$where = "where id=$goods_id and type=3 and rp_id=0 and score=$score order by add_time desc";
	//echo $where;die();
	$order_reviews = $User->reviewList($start,$size,$where,true,true,@$goods['relates']);
	//print_r($order_reviews);die();
	$sm->assign('order_reviews',	$order_reviews);
	//print_r($order_reviews);die();
	$count = $Main->counts("select count(*) from review  $where");
	$pager = $Main->pager($count,$size,"main.php?act=order_review_list&goods_id=".$goods_id."&score=$score");
	$sm->assign('score',	$score);
	$sm->assign('pagers',	$pager);
	$sm->assign('goods_id',	$goods_id);

}
//删除小店日志
elseif($act=='del_cat_log')
{
	$log_id = !empty($_GET['log_id']) ? intval($_GET['log_id']) : 0;
	if(mysql_query("delete from cat_log where log_id=$log_id"))
	{
		$Main->msg("操作成功!","",true);
	}
}
//gold_log
elseif($act=='gold_log')
{

	$type = !empty($_GET['type']) ? intval($_GET['type']) : 1;
	$where  = " where type=$type order by add_time desc";
	$gold_log = $User->goldLog($start,$size,$where);
	$count = $Main->counts("select count(*) from gold_log  $where");
	$pager = $Main->pager($count,$size,"main.php?act=gold_log&type=".$type);

	$sm->assign('pagers',	$pager);
	$sm->assign('gold_log',	$gold_log);
}
//chat

elseif($act=='chat')
{
	$user_id = @$_SESSION['user_id'];
	$q_id= @intval($_GET['q_id']);
	if(in_array($user_id,$GLOBALS['chat_manager']))
	{
		$type=2;
	}
	else
	{
		$type=1;
	}
	if($q_id)
		$type=1;
	$title = '在线问答';
	if($q_id==1)
	{
		$title = '本周话题：在开展小区业务过程中最让你困惑的是什么？<br/>希望团聚小区网站能够如何帮助你？';
	}
	elseif($q_id==2)
	{
		$title = '本周话题：在你心目中，社区支行的盈利和发展模式怎样的？';
	}
	elseif($q_id==3)
	{
		$title = '在不考虑考核的前提下，你认为小区便民店的主要功能是提升客户金融资产，<br/>还是拓展客户并增加客户黏性？为什么？';
	}
	elseif($q_id==4)
	{
		$title = '前段时间招商银行引入的“咖啡银行”模式让人眼前一亮。对于这种异业合作的模式，你认为是未来小区业务发展的方向吗？为什么？除了“咖啡”小区还能和哪些“异业”开展合作？';
	}
	$sm->assign('title',$title);
	$sm->assign('type',$type);
	$sm->assign('q_id',$q_id);
	$chat = $Chat->chatGet("where q_id=$q_id");

	$sm->assign('chat',$chat);


}
elseif($act=='chat_export')
{
	$q_id= @intval($_GET['q_id']);
	$chat = $Chat->chatGet("where q_id=$q_id");
	$table = "<table border=1><tr><th>用户名</th><th>用户ID</th><th>内容</th><th>时间</th></tr>";

	foreach($chat as $key=>$val)
	{
		$table.="<tr><th>".$val['user_name']."</th><th>".$val['user_id']."</th><th>".$val['content']."</th><th>".$val['add_time']."</th></tr>";
	}
	$table .="</table>";
	$name = date("Y-m-d-H-i-s");
	//header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	//header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();
}
elseif($act=='zan_export')
{

	$zan = $GLOBALS['db']->getAll("select * from zan where type=3");

	$table = "<table border=1><tr><th>项目ID</th><th>OA名</th><th>时间</th></tr>";

	foreach($zan as $key=>$val)
	{
		$table.="<tr><th>".$val['id']."</th><th>".$val['user_name']."</th><th>".$Main->dateFormat($val['add_time'])."</th></tr>";
	}
	$table .="</table>";
	$name = date("Y-m-d-H-i-s");
	header("Content-type:application/vnd.ms-excel ;charset=UTF-8");
	header("Content-Disposition:filename=".$name.".xls");
	echo $table;
	die();
}
elseif($act=="clear_cookie")
{

	foreach($_COOKIE as $key=>$val)
	{
		foreach($val as $_k=>$_v)
		{
			if(isset($_v['qiandao']))
			{
				$user_id=$_k;
				setCookie("user[$user_id][qiandao]",1,time()-1,'/ ');
			}

		}

	}

}
elseif($act=='trans_jy')
{
	$Other->jyData();
	die();

}
/*
 * 登录api
 * */
 elseif($act=='loginapi')
 {
	if(!empty($_SESSION['user_info']))
	{
		$web = $_GET['web'].'?code='.(base64_encode(json_encode($_SESSION['user_info'])));

		header("location:$web");
		die();
	}
	$no_style = 1;
	$sm->assign('get',$_GET);

 }
elseif($act=='loginapi2')
 {

	$_SESSION['from'] ='main.php?'.$_SERVER['QUERY_STRING'];

	if(!empty($_SESSION['user_info']))
	{
		$web = $_GET['web'].'?code='.(base64_encode(json_encode($_SESSION['user_info'])));

		header("location:$web");
		die();
	}
	$no_style = 1;
	$sm->assign('get',$_GET);

 }


$Main->assigns($position);

$sm->assign('no_index',	$no_index);
if($no_style)
{

	$sm->display('no_style.htm',$cache_id);
}
else
{
	$sm->display('main.htm',$cache_id);
}
?>
