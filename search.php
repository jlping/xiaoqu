<?php


define('MANS_', true);
require ('./includes/init.php');
require (ROOT_PATH . '/includes/class/search.class.php');
$size = 12;
$start = $page*$size;
$keywords = !empty($_GET['kw']) ? $_GET['kw'] : '';
$cat_id   = !empty($_GET['cat_id']) ? intval($_GET['cat_id']) : 0;




if (empty($keywords)) {

    header('location:index.php');

	exit();
}

//实例化keywords类
 $keys = new Keywords($keywords,$cat_id);
 $sm->setCaching(0,$cache_id);


  $kws = $Main->buildUri($keys->keywords,-1);

  $cat = $cat_id ? $cat_id.'-' : '0-';
  $url = SEARCH."-".$page."-".$cat. $kws . '.html';
  $match = SEARCH.'-\d+(-\d+)?-'.$kws;


  if(!preg_match("#". $match."#",$req,$arr))
  {
	//$Main->url301($url);
  }

$cat_name = '';
if($cat_id)
{
	$cat_name = $Main->counts("select cat_name from category where cat_id=$cat_id");
}

/* 排序、显示方式以及类型 */
$size = isset($_SESSION['c_sort']['size']) ? intval($_SESSION['c_sort']['size']) : 8;
$sort = !empty($_SESSION['c_sort']['sort']) ? $_SESSION['c_sort']['sort'] : 'goods_id';
$order = !empty($_SESSION['c_sort']['order']) ? $_SESSION['c_sort']['order'] : 'desc';
if ($order == 'ASC') {
    $c_order = 'DESC';
} else {
    $c_order = 'ASC';
}



if(!$sm->isCached('search.htm',$cache_id))
{
	$Other->leftData();
	$sm->assign('c_order', $c_order);


	if ($keys->setCount() > 0) {
		if($GLOBALS['page']==0)
		{
			$s['engine'] = 'SELF';
			$s['keywords'] = $keys->keywords;
			$Main->keywordsAdd($s);
		}

		//取得搜索列表
		$goods_list = $keys->getSearchList($page*$size, $size, $sort, $order);

		$sm->assign('goods_list', $goods_list);
		//相关关键词
		$relate = $keys->relatedSearch();

		$relate_words = array();
		foreach ($relate as $key => $val) {
			$v = $Main->buildUri($val,-1);
			$relate_words[] = '<a href="'.SEARCH.'-0-' . $v . '.html">' . $val . '</a> ';
		}
		//相关分类
		$relate_categories = $keys->relateCategories();
		$sm->assign('relate_categories', $relate_categories);
	}


	//分页

	$pagers = $Main->Pager($keys->counts,$size,SEARCH,$cat.$kws);

	$sm->assign('pagers', $pagers);

	//统计搜索条件
	$condition = @$_SESSION['search'];
	//print_r($condition);
	$sm->assign('condition', 'condition');
	$sm->assign('price_limit',@$condition['price_limit']);
	$sm->assign('price_limitd',@$condition['price_limitd']);
	$sm->assign('kw', $kws);

	$position = $Main->position($cat_name.' '.$kws, SEARCH);
	$Main->assigns($position);

	$sm->assign('ur_here', '<em class="red">Related Searches:</em> ' . @join($relate_words, ' , ')); // 当前位置
}
$sm->display('search.htm',$cache_id);
?>
